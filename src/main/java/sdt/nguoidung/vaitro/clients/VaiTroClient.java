package sdt.nguoidung.vaitro.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.nguoidung.configs.RestPageImpl;
import sdt.nguoidung.utils.Utils;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class VaiTroClient {

    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;

    @Autowired
    @Qualifier("liferayRest")
    public RestTemplate liferayRestTemplate;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final String path = "/api/v1/vaitro";

    public List<VaiTroDTO> getListVaiTro() {
        String restURL = path + Utils.SLASH + "listAllVaiTro";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<List<VaiTroDTO>> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<List<VaiTroDTO>>() {});

        return responseEntity.getBody();
    }

    public VaiTroDTO getVaiTro(long vaiTroId) {

        String restURL = path+"/"+vaiTroId;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<VaiTroDTO> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<VaiTroDTO>(){});

        return responseEntity.getBody();
    }

    public Page<VaiTroDTO> getVaiTros(String keyword, int pageIndex, int size){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<RestPageImpl<VaiTroDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<VaiTroDTO>>() {});

        return response.getBody();

    }

    public String getVaiTroJson(String keyword, int pageIndex, int size){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<String> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, String.class);

        return response.getBody();

    }

    public boolean addVaiTro (VaiTroDTO vaiTroDTO) {

        String restURL =path+"/create";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<VaiTroDTO> entity = new HttpEntity<>(vaiTroDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean updateVaiTro (VaiTroDTO vaiTroDTO) {

        String restURL =path+"/update";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<VaiTroDTO> entity = new HttpEntity<>(vaiTroDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean deleteVaiTro (long vaiTroId) {
        String restURL =path+"/delete/"+vaiTroId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<?> entity = new HttpEntity(headers);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public VaiTroDTO findByTen(String ten){

        String restURL = path+"/findByTen";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL).queryParam("ten", ten);
        ResponseEntity<VaiTroDTO> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET,null, new ParameterizedTypeReference<VaiTroDTO>(){});

        return response.getBody();

    }

    public VaiTroDTO findByMa(String ma){
        String restURL = path+"/findByMa";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL).queryParam("ma", ma);
        ResponseEntity<VaiTroDTO> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET,null, new ParameterizedTypeReference<VaiTroDTO>(){});

        return response.getBody();

    }

    public List<VaiTroDTO> getVaiTroIdIsNotIn(String vaiTroIds){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findByIdIsNotIn")
                .queryParam("vaiTroIds", vaiTroIds);
        ResponseEntity<List<VaiTroDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<VaiTroDTO>>() {});

        return response.getBody();

    }

    public List<VaiTroDTO> getVaiTroIdIn(String vaiTroIds){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findByIdIn")
                .queryParam("vaiTroIds", vaiTroIds);

        ResponseEntity<List<VaiTroDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<VaiTroDTO>>() {});

        return response.getBody();

    }
}
