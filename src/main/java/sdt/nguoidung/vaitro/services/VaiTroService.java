package sdt.nguoidung.vaitro.services;

import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.nguoidung.coquanquanly.services.CoQuanQuanLyService;
import sdt.nguoidung.utils.Utils;
import sdt.nguoidung.vaitro.clients.VaiTroClient;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VaiTroService {
    
    private final VaiTroClient vaiTroClient;
    private final CoQuanQuanLyService coQuanQuanLyService;
    
    public Page<VaiTroDTO> getVaiTroRest(String keyword, int pageIndex, int size){

        return vaiTroClient.getVaiTros(keyword, pageIndex, size);
    }

    public VaiTroDTO findByVaiTroId(long vaiTroId){

        return vaiTroClient.getVaiTro(vaiTroId);
    }

    public VaiTroDTO getVaiTro (long vaiTroId) {

        if (vaiTroId > 0) {
            return findByVaiTroId(vaiTroId);
        } else {
            VaiTroDTO vaiTroDTO = new VaiTroDTO();
            vaiTroDTO.setTrangThai(1);

            return vaiTroDTO;
        }
    }

    public List<VaiTroDTO> getListVaiTro () throws IOException {

        return vaiTroClient.getListVaiTro();
    }

    public boolean deleteVaiTro(long vaiTroId) {

        return vaiTroClient.deleteVaiTro(vaiTroId);
    }

    public VaiTroDTO findByMa(String ma){

        return vaiTroClient.findByMa(ma);
    }

    public VaiTroDTO findByTen(String ten){

        return vaiTroClient.findByTen(ten);
    }

    public String getVaiTroJson(String keyword, int pageIndex, int size) {

        return vaiTroClient.getVaiTroJson(keyword, pageIndex, size);
    }

    public boolean saveVaiTro (VaiTroDTO vaiTroDTO) {

        if (Validator.isNotNull(vaiTroDTO.getId())) {

            return vaiTroClient.updateVaiTro(vaiTroDTO);
        }

        return vaiTroClient.addVaiTro(vaiTroDTO);

    }

    public List<VaiTroDTO> findByVaiTroIsNotIn(List<VaiTroDTO> vaiTros) {

        String ids = getVaiTroIds(vaiTros);

        return vaiTroClient.getVaiTroIdIsNotIn(ids);
    }

    public List<VaiTroDTO> findByVaiTroIn(String vaiTroIds) {

        return vaiTroClient.getVaiTroIdIn(vaiTroIds);
    }

    public String getVaiTroIds (List<VaiTroDTO> vaiTros) {

        StringBuilder builder = new StringBuilder();

        if (Validator.isNotNull(vaiTros)) {
            for (VaiTroDTO vaiTroDTO:vaiTros) {
                builder.append(vaiTroDTO.getId()).append(",");
            }
        }

        return builder.toString().length() > 0 ? builder.toString().substring(0, builder.toString().length() - 1) : "";
    }

    public List<VaiTroDTO> getVaiTroByCoQuanIdAndChucVuId (long coQuanId, long chucVuId) {
        List<VaiTroDTO> vaiTroDTOS = new ArrayList<>();
        if (chucVuId > 0) {

            List<ChucVu2CoQuan2VaiTroDTO> chucVu2CoQuans = coQuanQuanLyService.getCoQuan2ChucVu(coQuanId, chucVuId);
            if (chucVu2CoQuans != null) {
                vaiTroDTOS.addAll(chucVu2CoQuans.stream().map(
                        ChucVu2CoQuan2VaiTroDTO -> ChucVu2CoQuan2VaiTroDTO.getVaiTro()).collect(Collectors.toList()));
            }
        }

        return vaiTroDTOS;
    }

    public String getRoleIdsByVaiTros (List<VaiTroDTO> vaiTros) {
        List<Long> roleIds = new ArrayList<>();

        if (vaiTros != null) {
            for (VaiTroDTO vaiTroDTO:vaiTros) {
                roleIds.addAll(vaiTroDTO.getTaiNguyens().stream().map(
                        TaiNguyenDTO ->TaiNguyenDTO.getRoleId()).collect(Collectors.toList()));
            }
        }

        return Utils.convertListToString(roleIds);
    }
}
