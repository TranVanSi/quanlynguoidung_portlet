package sdt.nguoidung.vaitro.controllers;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import sdt.nguoidung.vaitro.services.VaiTroService;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class VaiTroAjaxController {

    private final VaiTroService vaiTroService;
    private final VaiTroValidate vaiTroValidate;

    @ResourceMapping(value ="loadVaiTro")
    public void loadVaiTro(ResourceRequest request, ResourceResponse response,
                           @RequestParam(value = "keyword", defaultValue = "") String keyword,
                           @RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                           @RequestParam(value = "size", defaultValue = "20") int size) throws Exception{

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(vaiTroService.getVaiTroJson(keyword, pageIndex, size).toString());
    }

    @ResourceMapping(value ="validateForm")
    public void validateForm(ResourceRequest request, ResourceResponse response,
                             @RequestParam(value = "ma", defaultValue = "") String ma,
                             @RequestParam(value = "ten", defaultValue = "") String ten,
                             @RequestParam(value = "vaiTroId", defaultValue = "0") long vaiTroId) throws Exception{

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ma",  vaiTroValidate.validateMa(ma, vaiTroId));
        jsonObject.put("ten", true);

        response.getWriter().write(jsonObject.toString());
    }
}
