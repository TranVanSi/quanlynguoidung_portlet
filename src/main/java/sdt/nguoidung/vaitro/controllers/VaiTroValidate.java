package sdt.nguoidung.vaitro.controllers;

import com.liferay.portal.kernel.servlet.SessionErrors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;
import sdt.nguoidung.vaitro.services.VaiTroService;

import javax.portlet.ActionRequest;

@Service
@RequiredArgsConstructor
public class VaiTroValidate {

    private final VaiTroService vaiTroService;

	public boolean validateMa(ActionRequest request, String ma, Long vaiTroId) {

		if (ma.length() == 0) {
			return false;
		}

		vaiTroId = (vaiTroId == null ? 0 : vaiTroId);

		VaiTroDTO vaiTroByMa = vaiTroService.findByMa(ma);
		if ((vaiTroId == 0 && vaiTroByMa != null) ||
				(vaiTroId > 0 && vaiTroByMa != null && !vaiTroId.equals(vaiTroByMa.getId()))) {
			SessionErrors.add(request, "vaitro.validate.ma.trung");
			return false;
		}

		return true;
	}

	public boolean validateMa(String ma, long vaiTroId) {

		if (ma.length() == 0) {
			return false;
		}

		VaiTroDTO vaiTroByMa = vaiTroService.findByMa(ma);
		if ((vaiTroId == 0 && vaiTroByMa != null) ||
				(vaiTroId > 0 && vaiTroByMa != null && vaiTroId != vaiTroByMa.getId())) {
			return false;
		}

		return true;
	}

	public boolean validateTen(ActionRequest request, String ten, long vaiTroId) {

		if (ten.length() == 0) {
			return false;
		}

		VaiTroDTO vaiTroByTen = vaiTroService.findByTen(ten);

		if ((vaiTroId == 0 && vaiTroByTen != null) ||
				(vaiTroId > 0 && vaiTroByTen != null && vaiTroId != vaiTroByTen.getId())) {
			SessionErrors.add(request, "vaitro.validate.ten.trung");
			return false;
		}

		return true;
	}
}
