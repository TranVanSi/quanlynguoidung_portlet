package sdt.nguoidung.vaitro.controllers;


import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import sdt.nguoidung.tainguyen.dto.TaiNguyenDTO;
import sdt.nguoidung.tainguyen.services.TaiNguyenService;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;
import sdt.nguoidung.vaitro.services.VaiTroService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class VaiTroController {

    private final VaiTroService vaiTroService;
    private final TaiNguyenService taiNguyenService;
    private final VaiTroValidate vaiTroValidate;

    @RenderMapping
    public String view(Model model) throws Exception {

        List<VaiTroDTO> listView = vaiTroService.getListVaiTro();
        model.addAttribute("listView", listView);

        return "danhsach";
    }

    @RenderMapping(params = "action=themMoi")
    public String themMoi(RenderRequest request, RenderResponse response, Model model,
                          @RequestParam(required = false, defaultValue = "true") String validation) {
        long vaiTroId = ParamUtil.getLong(request,"id");
        VaiTroDTO vaiTroDTO = new VaiTroDTO();

            if (vaiTroId > 0) {
                vaiTroDTO = vaiTroService.getVaiTro(vaiTroId);
            }
        model.addAttribute("danhSachTaiNguyenHeThongDuocChon", vaiTroDTO.getTaiNguyens());
        model.addAttribute("danhSachTaiNguyenHeThong",taiNguyenService.findByTaiNguyenIsNotIn(vaiTroDTO));

        if (validation.equals("false")) {
            SessionErrors.add(request, "alert-error");
            return "/themmoi";
        }

        model.addAttribute("vaiTro", vaiTroDTO);

        return "themmoi";
    }

    @ActionMapping(params = "action=themMoi")
    public void themMoi(Model model, ActionRequest actionRequest, ActionResponse actionResponse,
                        @RequestParam(value="taiNguyenHeThongDuocChon", defaultValue="") String taiNguyenHeThongDuocChonString,
                        @ModelAttribute("vaiTro") VaiTroDTO vaiTroDTO,
                        BindingResult result, SessionStatus sessionStatus) {

        if (!vaiTroValidate.validateMa(actionRequest, vaiTroDTO.getMa(), vaiTroDTO.getId())) {
            actionResponse.setRenderParameter("validation", "false");
            actionResponse.setRenderParameter("action","themMoi");
            model.addAttribute("vaiTro", vaiTroDTO);

            return;
        }

        List<TaiNguyenDTO> taiNguyens = taiNguyenService.findByTaiNguyenIn(taiNguyenHeThongDuocChonString);
        vaiTroDTO.setTaiNguyens(taiNguyens);

        vaiTroService.saveVaiTro(vaiTroDTO);
        SessionMessages.add(actionRequest, "form-success");
        actionResponse.setRenderParameter("action","");
    }

    @RenderMapping(params = "action=chiTiet")
    public String chiTiet(RenderRequest request, RenderResponse response, Model model,
                          @RequestParam(value = "id", defaultValue = "0") long vaiTroId,
                          @RequestParam(required = false, defaultValue = "true") String validation) throws Exception {


        if (validation.equals("false")) {
            SessionErrors.add(request, "alert-error");
            return "/chitiet";
        }

        if (vaiTroId > 0) {
            model.addAttribute("item", vaiTroService.getVaiTro(vaiTroId));
        } else {
            model.addAttribute("item", new VaiTroDTO());
        }

        return "chitiet";
    }

    @ActionMapping(params = "action=xoa")
    public void xoa(ActionRequest request, ActionResponse response, SessionStatus sessionStatus,
                    @RequestParam(value = "id", defaultValue = "0") long id) throws PortalException, IOException {
        if (id > 0) {
            vaiTroService.deleteVaiTro(id);
        }

        sessionStatus.setComplete();
        SessionMessages.add(request, "form-success");
        response.setRenderParameter("action","");
    }
}
