package sdt.nguoidung.vaitro.dto;

import lombok.Data;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.nguoidung.tainguyen.dto.TaiNguyenDTO;

import java.io.Serializable;
import java.util.List;

@Data
public class VaiTroDTO extends Auditable<String> implements Serializable {
    private Long id;
    private String ma;
    private String ten;
    private Integer trangThai;
    private String moTa;
    private List<TaiNguyenDTO> taiNguyens;
    private List<ChucVu2CoQuan2VaiTroDTO> chucVu2CoQuan2VaiTros;
}
