package sdt.nguoidung.tainguyen.services;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import sdt.nguoidung.tainguyen.clients.TaiNguyenClient;
import sdt.nguoidung.tainguyen.dto.TaiNguyenDTO;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaiNguyenService {

    private final TaiNguyenClient taiNguyenClient;

    public Page<TaiNguyenDTO> getTaiNguyenRest(String keyword, int pageIndex, int size){

        return taiNguyenClient.getTaiNguyens(keyword, pageIndex, size);
    }

    public TaiNguyenDTO findByTaiNguyenId(long taiNguyenId){

        return taiNguyenClient.getTaiNguyen(taiNguyenId);
    }

    public TaiNguyenDTO getTaiNguyen (long taiNguyenId) {

        if (taiNguyenId > 0) {
            return findByTaiNguyenId(taiNguyenId);
        } else {
            TaiNguyenDTO taiNguyenDTO = new TaiNguyenDTO();
            taiNguyenDTO.setTrangThai(1);

            return taiNguyenDTO;
        }
    }

    public String getTaiNguyenJson(String keyword, int pageIndex, int size) {

        return taiNguyenClient.getTaiNguyenJson(keyword, pageIndex, size);
    }

    public TaiNguyenDTO findByTen(String ten){

        return taiNguyenClient.findByTen(ten);
    }

    public TaiNguyenDTO findByMa(String ma){

        return taiNguyenClient.findByMa(ma);
    }

    public boolean saveTaiNguyen (TaiNguyenDTO taiNguyenDTO, User user) {

        if (Validator.isNotNull(taiNguyenDTO.getId()) && Validator.isNotNull(taiNguyenDTO.getRoleId()) && Validator.isNotNull(taiNguyenDTO.getTen())) {
            taiNguyenClient.updateRole(taiNguyenDTO.getRoleId(), taiNguyenDTO.getTen());
            return taiNguyenClient.updateTaiNguyen(taiNguyenDTO);
        }

        long role = taiNguyenClient.addRole(taiNguyenDTO, user.getCompanyId(), user.getUserId());
        taiNguyenDTO.setRoleId(role);

        return taiNguyenClient.addTaiNguyen(taiNguyenDTO);

    }

    public boolean deleteTaiNguyen (long taiNguyenId) {
        TaiNguyenDTO taiNguyenDTO = taiNguyenClient.getTaiNguyen(taiNguyenId);
        if (Validator.isNotNull(taiNguyenDTO)) {
            taiNguyenClient.deleteRole(taiNguyenDTO.getRoleId());
        }

        return taiNguyenClient.deleteTaiNguyen(taiNguyenId);
    }

    public List<TaiNguyenDTO> findByTaiNguyenIsNotIn(VaiTroDTO vaiTroDTO) {

        String ids = getTaiNguyenIds(vaiTroDTO);

        return taiNguyenClient.getTaiNguyenIdIsNotIn(ids);
    }

    public List<TaiNguyenDTO> findByTaiNguyenIn(String taiNguyenIds) {

        return taiNguyenClient.getTaiNguyenIdIn(taiNguyenIds);
    }

    public String getTaiNguyenIds (VaiTroDTO vaiTroDTO) {

        StringBuilder builder = new StringBuilder();

        if (Validator.isNotNull(vaiTroDTO.getTaiNguyens())) {
            for (TaiNguyenDTO taiNguyen : vaiTroDTO.getTaiNguyens()) {
                builder.append(taiNguyen.getId()).append(",");
            }
        }

        return builder.toString().length() > 0 ? builder.toString().substring(0, builder.toString().length() - 1) : "";
    }

    public List<TaiNguyenDTO> getListTaiNguyen() throws Exception {
        return taiNguyenClient.getListTaiNguyen();
    }
}
