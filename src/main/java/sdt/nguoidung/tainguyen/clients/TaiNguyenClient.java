package sdt.nguoidung.tainguyen.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.nguoidung.configs.RestPageImpl;
import sdt.nguoidung.utils.Utils;
import sdt.nguoidung.tainguyen.dto.TaiNguyenDTO;

import java.util.List;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class TaiNguyenClient {

    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;

    @Autowired
    @Qualifier("liferayRest")
    public RestTemplate liferayRestTemplate;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final String path = "/api/v1/tainguyen";
    private final String pathServiceLiferay = "/api/v1/role";

    public TaiNguyenDTO getTaiNguyen(long taiNguyenId) {

        String restURL = path+"/"+taiNguyenId;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<TaiNguyenDTO> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<TaiNguyenDTO>(){});

        return responseEntity.getBody();
    }

    public List<TaiNguyenDTO> getListTaiNguyen() throws Exception {

        String restURL = path + Utils.SLASH + "listAllTaiNguyen";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<List<TaiNguyenDTO>> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<List<TaiNguyenDTO>>() {});
       return responseEntity.getBody();
    }

    public Page<TaiNguyenDTO> getTaiNguyens(String keyword, int pageIndex, int size){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<RestPageImpl<TaiNguyenDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<TaiNguyenDTO>>() {});

        return response.getBody();

    }

    public String getTaiNguyenJson(String keyword, int pageIndex, int size){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<String> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, String.class);

        return response.getBody();

    }

    public TaiNguyenDTO findByTen(String ten){

        String restURL = path+"/findByTen";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL).queryParam("ten", ten);
        ResponseEntity<TaiNguyenDTO> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET,null, new ParameterizedTypeReference<TaiNguyenDTO>(){});

        return response.getBody();

    }

    public TaiNguyenDTO findByMa(String ma){
        String restURL = path+"/findByMa";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL).queryParam("ma", ma);
        ResponseEntity<TaiNguyenDTO> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET,null, new ParameterizedTypeReference<TaiNguyenDTO>(){});

        return response.getBody();

    }

    public long addRole (TaiNguyenDTO taiNguyenDTO, long companyId, long userId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pathServiceLiferay+"/create")
                .queryParam("name", taiNguyenDTO.getTen())
                .queryParam("type", taiNguyenDTO.getLoai())
                .queryParam("companyId", companyId)
                .queryParam("userId", userId);
        ResponseEntity<Long> response = liferayRestTemplate.exchange(
                builder.toUriString(), HttpMethod.POST, null, Long.class);

        return response.getBody();
    }

    public long updateRole(long roleId, String roleName) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pathServiceLiferay+"/update")
                .queryParam("name", roleName)
                .queryParam("roleId", roleId);
        ResponseEntity<Long> response = liferayRestTemplate.exchange(
                builder.toUriString(), HttpMethod.PUT, null, Long.class);
        return response.getBody();
    }

    public boolean deleteRole(long roleId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pathServiceLiferay+"/delete")
                .queryParam("roleId", roleId);
        ResponseEntity<String> response = liferayRestTemplate.exchange(
                builder.toUriString(), HttpMethod.DELETE, null, String.class);

        return true;
    }

    public boolean addTaiNguyen (TaiNguyenDTO taiNguyenDTO) {

        String restURL =path+"/create";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<TaiNguyenDTO> entity = new HttpEntity<>(taiNguyenDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean updateTaiNguyen (TaiNguyenDTO taiNguyenDTO) {

        String restURL =path+"/update";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<TaiNguyenDTO> entity = new HttpEntity<>(taiNguyenDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean deleteTaiNguyen (long taiNguyenId) {
        String restURL =path+"/delete/"+taiNguyenId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<?> entity = new HttpEntity(headers);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public List<TaiNguyenDTO> getTaiNguyenIdIsNotIn(String taiNguyenIds){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findByIdIsNotIn")
                .queryParam("taiNguyenIds", taiNguyenIds);
        ResponseEntity<List<TaiNguyenDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<TaiNguyenDTO>>() {});

        return response.getBody();

    }

    public List<TaiNguyenDTO> getTaiNguyenIdIn(String taiNguyenIds){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findByIdIn")
                .queryParam("taiNguyenIds", taiNguyenIds);

        ResponseEntity<List<TaiNguyenDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<TaiNguyenDTO>>() {});

        return response.getBody();

    }
}
