package sdt.nguoidung.tainguyen.controllers;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import sdt.nguoidung.tainguyen.dto.TaiNguyenDTO;
import sdt.nguoidung.tainguyen.services.TaiNguyenService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.util.List;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class TaiNguyenController {

	private final TaiNguyenValidate taiNguyenValidate;
	private final TaiNguyenService taiNguyenService;

	@RenderMapping
	public String view(Model model) throws Exception {

		List<TaiNguyenDTO> listView = taiNguyenService.getListTaiNguyen();
		model.addAttribute("listView", listView);

		return "danhsach";
	}

	@RenderMapping(params = "action=themMoi")
	public String themMoi(RenderRequest request, RenderResponse response, Model model,
						  @RequestParam(required = false, defaultValue = "true") String validation) {
		long taiNguyenId = ParamUtil.getLong(request, "taiNguyenId");
		if (validation.equals("false")) {
			SessionErrors.add(request, "alert-error");
			return "/themmoi";
		}
		TaiNguyenDTO taiNguyenDTO = new TaiNguyenDTO();

			if (taiNguyenId > 0) {
				taiNguyenDTO = taiNguyenService.getTaiNguyen(taiNguyenId);
			}

		model.addAttribute("taiNguyen", taiNguyenDTO);

		return "themmoi";
	}

	@ActionMapping(params = "action=themMoi")
	public void themMoi(ActionRequest actionRequest, ActionResponse actionResponse, SessionStatus sessionStatus,
						@ModelAttribute("taiNguyen") TaiNguyenDTO taiNguyenDTO, BindingResult result, Model model) throws Exception {

		User user = PortalUtil.getUser(actionRequest);
		TaiNguyenDTO taiNguyenByMa = taiNguyenService.findByMa(taiNguyenDTO.getMa());
		TaiNguyenDTO taiNguyenByTen = taiNguyenService.findByTen(taiNguyenDTO.getTen());

		if (!taiNguyenValidate.validate(actionRequest, taiNguyenDTO, taiNguyenByTen, taiNguyenByMa)) {
			actionResponse.setRenderParameter("validation", "false");
			actionResponse.setRenderParameter("action","themMoi");
			model.addAttribute("taiNguyen", taiNguyenDTO);

            return;
        }

		taiNguyenService.saveTaiNguyen(taiNguyenDTO, user);
		sessionStatus.setComplete();
		SessionMessages.add(actionRequest, "form-success");
		actionResponse.setRenderParameter("action","");
	}

	@RenderMapping(params = "action=chiTiet")
	public String chiTiet(RenderRequest request, RenderResponse response, Model model,
						  @RequestParam(value = "taiNguyenId", defaultValue = "0") long taiNguyenId,
						  @RequestParam(required = false, defaultValue = "true") String validation) {


		if (validation.equals("false")) {
			SessionErrors.add(request, "alert-error");
			return "/chitiet";
		}

		if (taiNguyenId > 0) {
			model.addAttribute("item", taiNguyenService.getTaiNguyen(taiNguyenId));
		} else {
			model.addAttribute("item", new TaiNguyenDTO());
		}

		return "chitiet";
	}

	@ActionMapping(params = "action=xoa")
	public void xoa(ActionRequest request, ActionResponse response, SessionStatus sessionStatus,
						@RequestParam(value = "taiNguyenId", defaultValue = "0") long taiNguyenId) {
		if (taiNguyenId > 0) {

			taiNguyenService.deleteTaiNguyen(taiNguyenId);
		}

		sessionStatus.setComplete();
		SessionMessages.add(request, "form-success");
		response.setRenderParameter("action","");
	}
}