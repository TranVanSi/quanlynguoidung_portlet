package sdt.nguoidung.tainguyen.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import sdt.nguoidung.tainguyen.services.TaiNguyenService;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class TaiNguyenAjaxController {

    private final TaiNguyenService taiNguyenService;

    @ResourceMapping(value ="loadTaiNguyen")
    public void loadTaiNguyen(ResourceRequest request, ResourceResponse response,
                              @RequestParam(value = "keyword", defaultValue = "") String keyword,
                              @RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                              @RequestParam(value = "size", defaultValue = "20") int size) throws Exception{

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(taiNguyenService.getTaiNguyenJson(keyword, pageIndex, size).toString());
    }
}
