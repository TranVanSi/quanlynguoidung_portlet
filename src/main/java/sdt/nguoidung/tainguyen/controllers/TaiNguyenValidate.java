package sdt.nguoidung.tainguyen.controllers;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.nguoidung.tainguyen.dto.TaiNguyenDTO;

import javax.portlet.ActionRequest;

@Service
@RequiredArgsConstructor
public class TaiNguyenValidate {

	public boolean validate(ActionRequest request, TaiNguyenDTO taiNguyenDraft, TaiNguyenDTO taiNguyenByTen, TaiNguyenDTO taiNguyenByMa) throws SystemException {

		boolean valid = true;
		long taiNguyenId = taiNguyenDraft.getId() != null ? taiNguyenDraft.getId() : 0;

		String ten = taiNguyenDraft.getTen();
		if (Validator.isNull(ten) || ten.length() > 100) {
			SessionErrors.add(request, "tainguyen.validate.ten");
			valid = false;
		} else {
			if ((taiNguyenId == 0 && taiNguyenByTen != null) ||
					(taiNguyenId > 0 && taiNguyenByTen != null && taiNguyenId != taiNguyenByTen.getId())) {
				SessionErrors.add(request, "tainguyen.validate.ten.trung");
				valid = false;
			}
		}

		if ((taiNguyenId == 0 && taiNguyenByMa != null) ||
				(taiNguyenId > 0 && taiNguyenByMa != null && taiNguyenId != taiNguyenByMa.getId())) {
			SessionErrors.add(request, "tainguyen.validate.ma.trung");
			valid = false;
		}

		return valid;
	}
}
