package sdt.nguoidung.tainguyen.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class TaiNguyenDTO extends Auditable<String> implements Serializable {
    private Long id;
    private String ma;
    private String ten;
    private int loai;
    private Long roleId;
    private int trangThai;
    private String moTa;
    private long ungDungTichHopId;

}
