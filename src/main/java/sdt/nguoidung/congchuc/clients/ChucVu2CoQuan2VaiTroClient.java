package sdt.nguoidung.congchuc.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.nguoidung.congchuc.services.ChucVu2CoQuan2VaiTroService;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuanDTO;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class ChucVu2CoQuan2VaiTroClient {
    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final String path = "/api/v1/chucvu2coquan2vaitro";

    public List<ChucVu2CoQuan2VaiTroDTO> getChucVuByCoQuanQuanLy (long coQuanQuanLyId) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/findByCoQuanQuanLyId")
                    .queryParam("coQuanQuanLyId", coQuanQuanLyId);
            ResponseEntity<List<ChucVu2CoQuan2VaiTroDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<List<ChucVu2CoQuan2VaiTroDTO>>(){});

            return responseEntity.getBody();
        } catch (Exception e) {

            return Collections.emptyList();
        }
    }

    public List<ChucVu2CoQuanDTO> getChucVuByCoQuanQuanLyId (long coQuanQuanLyId) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/findByCoQuanQuanLyId")
                    .queryParam("coQuanQuanLyId", coQuanQuanLyId);
            ResponseEntity<List<ChucVu2CoQuanDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<List<ChucVu2CoQuanDTO>>(){});

            return responseEntity.getBody();
        } catch (Exception e) {

            return Collections.emptyList();
        }
    }

}
