package sdt.nguoidung.congchuc.clients;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.nguoidung.configs.RestPageImpl;
import sdt.nguoidung.congchuc.dto.TaiKhoanNguoiDungDTO;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class TaiKhoanNguoiDungClient {

    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;
    
    private final String path = "/api/v1/taikhoannguoidung";

    public TaiKhoanNguoiDungDTO getTaiKhoanNguoiDung(long taiKhoanNguoiDungId) {

        String restURL = path+"/"+taiKhoanNguoiDungId;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<TaiKhoanNguoiDungDTO> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<TaiKhoanNguoiDungDTO>(){});

        return responseEntity.getBody();
    }

    public Page<TaiKhoanNguoiDungDTO> getTaiKhoanNguoiDungs(String keyword, int pageIndex, int size){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<RestPageImpl<TaiKhoanNguoiDungDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<TaiKhoanNguoiDungDTO>>() {});

        return response.getBody();

    }

    public TaiKhoanNguoiDungDTO addTaiKhoanNguoiDung (TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO) {

        String restURL = path + "/create";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<TaiKhoanNguoiDungDTO> entity = new HttpEntity<>(taiKhoanNguoiDungDTO, headers);

        HttpEntity<TaiKhoanNguoiDungDTO> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, TaiKhoanNguoiDungDTO.class, new Object[0]);

        return response.getBody();
    }

    public TaiKhoanNguoiDungDTO updateTaiKhoanNguoiDung (TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO) {

        String restURL =path+"/update";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<TaiKhoanNguoiDungDTO> entity = new HttpEntity<>(taiKhoanNguoiDungDTO, headers);
        HttpEntity<TaiKhoanNguoiDungDTO> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT,
                entity, TaiKhoanNguoiDungDTO.class, new Object[0]);

        return  response.getBody();

    }

    public boolean deleteTaiKhoanNguoiDung (long taiKhoanNguoiDungId) {
        String restURL =path+"/delete/"+taiKhoanNguoiDungId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<?> entity = new HttpEntity(headers);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public TaiKhoanNguoiDungDTO findByEmail(String email) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findByEmail")
                .queryParam("email", email);
        ResponseEntity<TaiKhoanNguoiDungDTO> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<TaiKhoanNguoiDungDTO>(){});

        return responseEntity.getBody();
    }
}
