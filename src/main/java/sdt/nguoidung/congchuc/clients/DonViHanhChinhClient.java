package sdt.nguoidung.congchuc.clients;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.nguoidung.congchuc.dto.DonViHanhChinhDTO;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DonViHanhChinhClient {
    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;

    private final String path = "/dvhc";

    public List<DonViHanhChinhDTO> findDVHCByCapId(long capId) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/capCoQuanQuanLyId/" + capId);
            ResponseEntity<List<DonViHanhChinhDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<DonViHanhChinhDTO>>() {
                    });
            return responseEntity.getBody();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

}
