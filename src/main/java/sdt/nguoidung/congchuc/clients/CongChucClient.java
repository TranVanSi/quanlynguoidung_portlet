package sdt.nguoidung.congchuc.clients;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.nguoidung.chucvu.dto.ChucVu2TaiNguyenDTO;
import sdt.nguoidung.configs.RestPageImpl;
import sdt.nguoidung.congchuc.dto.CongChucDTO;
import sdt.nguoidung.congchuc.dto.CongChucViewDTO;
import sdt.nguoidung.congchuc.mapper.CongChucMapper;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class CongChucClient {

    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final String path = "/api/v1/congchuc";
    private final CongChucMapper congChucMapper;

    public List<CongChucDTO> getListCongChucByCoQuanQuanLyId (long coQuanQuanLyId) {
        try {

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/findByCoQuanQuanLyId")
                    .queryParam("coQuanQuanLyId", coQuanQuanLyId);
            ResponseEntity<List<CongChucDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<List<CongChucDTO>>(){});

         return responseEntity.getBody();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public List<CongChucDTO> getListCongChucByChucVuId (long chucVuId) {
        try {

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/findByChucVuId")
                    .queryParam("chucVuId", chucVuId);
            ResponseEntity<List<CongChucDTO>> responseEntity = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<List<CongChucDTO>>(){});

            return responseEntity.getBody();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public List<CongChucDTO> getListCongChuc() throws Exception {
        List<CongChucDTO> congChucDTOS = new ArrayList<>();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/getAllCongChuc");
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, String.class);
        try {
            congChucDTOS = objectMapper.readValue(responseEntity.getBody(), new TypeReference<List<CongChucDTO>>() {});

        } catch (Exception e) {
            congChucDTOS.add(objectMapper.readValue(responseEntity.getBody(), new TypeReference<CongChucDTO>() {}));
        }

        return congChucDTOS;
    }

    public CongChucDTO getCongChuc(long congChucId) {

        String restURL = path+"/"+congChucId;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<CongChucDTO> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<CongChucDTO>(){});

        return responseEntity.getBody();
    }

    public Page<CongChucDTO> getCongChucs(String keyword, int pageIndex, int size){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<RestPageImpl<CongChucDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<CongChucDTO>>() {});

        return response.getBody();

    }

    public List<CongChucDTO> findTop1000(){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findTop1000");
        ResponseEntity<List<CongChucDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<CongChucDTO>>() {});

        return response.getBody();

    }

    public List<CongChucDTO> findTop1000ByIdNotIn(String congChucIds){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findTop1000ByIdNotIn")
                .queryParam("congChucIds", congChucIds);
        ResponseEntity<List<CongChucDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<CongChucDTO>>() {});

        return response.getBody();

    }

    public String getCongChucRestDataTable(DataTablesInput input){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        HttpEntity<DataTablesInput> entity = new HttpEntity<>(input, headers);

       ResponseEntity<String> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.POST, entity, String.class);

        return response.getBody();

    }

    public String getCongChucJson(String keyword, int pageIndex, int size){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<String> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, String.class);

        return response.getBody();

    }

    public CongChucDTO findByMa(String ma){
        String restURL = path+"/findByMa";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL).queryParam("ma", ma);
        ResponseEntity<CongChucDTO> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET,null, new ParameterizedTypeReference<CongChucDTO>(){});

        return response.getBody();

    }

    public boolean addCongChuc (CongChucDTO congChucDTO) {
        System.out.println("congChucDTO123=:"+congChucDTO);
        CongChucViewDTO congChucViewDTO = congChucMapper.toCongChucViewDTO(congChucDTO);
        System.out.println("congChucViewDTO=:"+congChucViewDTO);
        String restURL = path + "/create";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<CongChucViewDTO> entity = new HttpEntity<>(congChucViewDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean updateCongChuc (CongChucDTO congChucDTO) {

        String restURL =path+"/update";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<CongChucDTO> entity = new HttpEntity<>(congChucDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean deleteCongChuc (long congChucId) {
        String restURL =path+"/delete/"+congChucId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<?> entity = new HttpEntity(headers);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public Page<CongChucDTO> searchCongChuc(long coQuanQuanLyId, String keyword, int pageIndex, int size) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findToPaging")
                .queryParam("coQuanQuanLyId", coQuanQuanLyId)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<RestPageImpl<CongChucDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<CongChucDTO>>() {});

        return response.getBody();
    }

    public List<VaiTroDTO> getPermission(long congChucId) {

        String restURL = path+"/getPermission/"+congChucId;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<List<VaiTroDTO>> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<List<VaiTroDTO>>(){});

        return responseEntity.getBody();
    }
}
