package sdt.nguoidung.congchuc.clients;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.nguoidung.congchuc.dto.UserDTO;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class UserClient {

    @Autowired
    @Qualifier("liferayRest")
    public RestTemplate liferayRestTemplate;

    private final String path = "/api/v1/user";
    private final String userGroupPath = "/api/v1/usergroup";

    public UserDTO getUser(long userId) {

        String restURL = path + "/" + userId;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<UserDTO> responseEntity = liferayRestTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<UserDTO>() {
                });

        return responseEntity.getBody();
    }

    public long addUser(UserDTO userDTO) {
        System.out.println("userDTO="+userDTO );
        String restURL = path + "/create";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<UserDTO> entity = new HttpEntity<>(userDTO, headers);

        HttpEntity<Long> response = liferayRestTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, Long.class, new Object[0]);

        return response.getBody();

    }

    public boolean updateUser(UserDTO userDTO) {

        String restURL = path + "/update";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<UserDTO> entity = new HttpEntity<>(userDTO, headers);

        HttpEntity<?> response = liferayRestTemplate.exchange(builder.toUriString(), HttpMethod.PUT,
                entity, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean deleteUser(long userId) {
        String restURL = path + "/delete/" + userId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<?> entity = new HttpEntity(headers);
        HttpEntity<?> response = liferayRestTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean ganChucVuToCanBo(long userId, String maChucVu, String maCoQuan) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(userGroupPath + "/addChucVuToCanBo")
                .queryParam("userId", userId)
                .queryParam("maChucVu", maChucVu)
                .queryParam("maCoQuan", maCoQuan);

        HttpEntity<?> response = liferayRestTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                null, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public long getUserIdByEmail(long companyId, String email) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/getUserIdByEmail")
                .queryParam("companyId", companyId)
                .queryParam("email", email);
        ResponseEntity<Long> responseEntity = liferayRestTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<Long>() {
                });

        return responseEntity.getBody();
    }

    public boolean checkOldPassword(String password, String email) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/checkOldPassword")
                .queryParam("password", password)
                .queryParam("email", email);
        ResponseEntity<Boolean> responseEntity = liferayRestTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<Boolean>() {
                });

        return responseEntity.getBody();
    }

    public boolean changePassword(String password, String email) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path + "/changePassword")
                .queryParam("password", password)
                .queryParam("email", email);

        HttpEntity<?> entity = new HttpEntity(headers);
        HttpEntity<?> response = liferayRestTemplate.exchange(builder.toUriString(),
                HttpMethod.PUT, entity, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();
    }
}
