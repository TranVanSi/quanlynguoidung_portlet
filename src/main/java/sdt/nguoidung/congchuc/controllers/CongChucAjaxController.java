package sdt.nguoidung.congchuc.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.util.ParamUtil;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import sdt.nguoidung.congchuc.dto.CongChucDTO;
import sdt.nguoidung.congchuc.mapper.CongChucMapper;
import sdt.nguoidung.congchuc.services.ChucVu2CoQuan2VaiTroService;
import sdt.nguoidung.congchuc.services.CongChucService;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuanDTO;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class CongChucAjaxController {

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final CongChucService congChucService;
    private final CongChucMapper congChucMapper;
    private final ChucVu2CoQuan2VaiTroService chucVu2CoQuan2VaiTroSerVice;

    @ResourceMapping(value ="loadCongChuc")
    public void loadCongChuc(ResourceRequest request, ResourceResponse response,
                              @RequestParam(value = "keyword", defaultValue = "") String keyword,
                              @RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                              @RequestParam(value = "size", defaultValue = "20") int size) throws Exception{

        JSONObject jsonObject = new JSONObject();
        Page<CongChucDTO> congChucDTOPage = congChucService.getCongChucRest(keyword, pageIndex, size);

        jsonObject.put("data", congChucDTOPage.getContent());
        jsonObject.put("draw", 1);
        jsonObject.put("recordsTotal", congChucDTOPage.getTotalElements());
        jsonObject.put("recordsFiltered", congChucDTOPage.getTotalElements());

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(jsonObject.toString());
    }

    @ResourceMapping(value ="loadCongChucDataTable")
    public void loadCongChucDataTable(ResourceResponse response, DataTablesInput input) throws Exception{

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(congChucService.getCongChucRestDataTable(input));
    }

    @ResourceMapping(value="search")
    public void search(ResourceRequest request, ResourceResponse response) throws Exception {
        int offset = ParamUtil.getInteger(request,"offset", 0);
        int limit = ParamUtil.getInteger(request,"limit", 10);
        long coQuanQuanLyId = ParamUtil.getLong(request, "coQuanQuanLyId");
        String keyword = ParamUtil.getString(request, "search", "");
        Page<CongChucDTO> congChucJson  = congChucService.searchCongChuc(coQuanQuanLyId, keyword, offset, limit);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("total", congChucJson.getTotalElements());
        jsonObject.put("rows", congChucJson.getContent());

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonObject.toString());
    }

    @ResourceMapping(value = "getChucVuByCQQL")
    public void getChucVuByCQQL(ResourceRequest request, ResourceResponse response,
                                @RequestParam(value = "coQuanId", defaultValue = "0") long coQuanQuanLyId) throws Exception {

        Set<ChucVu2CoQuanDTO> danhSachChucVu2CoQuan2VaiTroDTOSet = new HashSet<>(chucVu2CoQuan2VaiTroSerVice.getChucVuByCoQuanQuanLyId(coQuanQuanLyId));

        String stringJson = "";

        if (danhSachChucVu2CoQuan2VaiTroDTOSet.size() > 0) {
            stringJson = objectMapper.writeValueAsString(danhSachChucVu2CoQuan2VaiTroDTOSet);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(stringJson);
        } else {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(new JSONObject().toString());
        }

    }
}
