package sdt.nguoidung.congchuc.controllers;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import sdt.nguoidung.chucvu.services.ChucVuService;
import sdt.nguoidung.congchuc.dto.CongChucDTO;
import sdt.nguoidung.congchuc.mapper.CongChucMapper;
import sdt.nguoidung.congchuc.services.CongChucService;
import sdt.nguoidung.congchuc.services.DonViHanhChinhService;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.services.CoQuanQuanLyService;
import sdt.nguoidung.utils.Constants;
import sdt.nguoidung.utils.DateTimeUtils;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class CongChucController {

    private final CongChucValidate congChucValidate;
    private final CongChucService congChucService;
    private final CoQuanQuanLyService coQuanQuanLyService;
    private final ChucVuService chucVuService;
    private final CongChucMapper congChucMapper;
    private final DonViHanhChinhService donViHanhChinhService;

    @RenderMapping
    public String view(Model model, RenderRequest request) throws Exception {
        long coQuanQuanLyId = ParamUtil.getLong(request, "coQuanQuanLyId");
        model.addAttribute("coQuanQuanLyId", coQuanQuanLyId);
        model.addAttribute("tenCoQuanQuanLy", coQuanQuanLyService.getCoQuanQuanLy(coQuanQuanLyId).getTen());
        return "danhsach";
    }

    @RenderMapping(params = "action=themMoi")
    public String themMoi(RenderRequest request, RenderResponse response, Model model) throws Exception {
        String validation = ParamUtil.getString(request, "validation", "true");
        long congChucId = ParamUtil.getLong(request, "congChucId");
        long coQuanQuanLyId = ParamUtil.getLong(request, "coQuanQuanLyId");
        model.addAttribute("chucVus", chucVuService.getListChucVu());
        model.addAttribute("coQuanQuanLyId", coQuanQuanLyId);
        model.addAttribute("coQuanQuanLys", coQuanQuanLyService.getListCoQuanQuanLy().get(0));
        model.addAttribute("noiCaps", donViHanhChinhService.findByCapId(Constants.CapDonViHanhChinh.NOI_CAP_CMND));
        if (validation.equals("false")) {
            model.addAttribute("validateFail","validateFail");
            SessionErrors.add(request, "alert-error");
            return "themmoi";
        }else{
            model.addAttribute("validateFail","");
        }
        CongChucDTO congChucDTO = congChucService.getCongChucById(congChucId, coQuanQuanLyId);
        if (Validator.isNotNull(congChucDTO)){
            String ngaySinhDate = congChucDTO.getNgaySinh();
            if (Validator.isNotNull(ngaySinhDate)){
                model.addAttribute("ngaySinhDate", DateTimeUtils.convertPatternDate(ngaySinhDate));
            }
            String ngayCapCMND = congChucDTO.getNgayCapCMND();
            if (Validator.isNotNull(ngayCapCMND)){
                model.addAttribute("ngayCapDate", DateTimeUtils.convertPatternDate(ngayCapCMND));
            }
        }
        model.addAttribute("congChuc", congChucDTO);
        model.addAttribute("email", congChucDTO.getTaiKhoanNguoiDung() != null ? congChucDTO.getTaiKhoanNguoiDung().getEmail() : "");

        return "themmoi";
    }

    @ActionMapping(params = "action=themMoi")
    public void themMoi(ActionRequest actionRequest, ActionResponse actionResponse,
                        @RequestParam(value = "coQuanQuanLyId", defaultValue = "0") long coQuanQuanLyId,
                        SessionStatus sessionStatus,
                        @ModelAttribute("congChuc") CongChucDTO congChucDTO, BindingResult result, Model model,
                        @RequestParam(value = "email", defaultValue = "") String email) throws Exception {
        ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

        if (congChucValidate.validate(actionRequest, actionResponse, congChucDTO, email)) {
            congChucService.saveCongChuc(congChucDTO, email, themeDisplay, congChucDTO.getCoQuanQuanLy().getId());
            sessionStatus.setComplete();
            SessionMessages.add(actionRequest, "form-success");
            actionResponse.setRenderParameter("action", "");

            return;
        }
        model.addAttribute("congChuc", congChucDTO);
        model.addAttribute("email", email);
        actionResponse.setRenderParameter("validation", "false");
        actionResponse.setRenderParameter("action", "themMoi");
        actionResponse.setRenderParameter("coQuanQuanLyId", String.valueOf(coQuanQuanLyId));
    }

    @ActionMapping(params = "action=xoa")
    public void xoa(ActionRequest request, ActionResponse response, SessionStatus sessionStatus,
                    @RequestParam(value = "congChucId", defaultValue = "0") long congChucId) throws PortalException, IOException {
        if (congChucId > 0) {
            congChucService.deleteCongChuc(congChucId);
        }

        sessionStatus.setComplete();
        SessionMessages.add(request, "form-success");
        response.setRenderParameter("action", "");
    }

    @RenderMapping(params = "action=chiTiet")
    public String chiTiet(RenderRequest request, RenderResponse response, Model model) throws Exception {
        String validation = ParamUtil.getString(request, "validation", "true");
        long congChucId = ParamUtil.getLong(request, "congChucId");
        long coQuanQuanLyId = ParamUtil.getLong(request, "coQuanQuanLyId");
        List<CoQuanQuanLyDTO> listCoQuanQuanLy = coQuanQuanLyService.getListCoQuanQuanLy();
        model.addAttribute("noiCaps", donViHanhChinhService.findByCapId(Constants.CapDonViHanhChinh.NOI_CAP_CMND));
        if (validation.equals("false")) {
            SessionErrors.add(request, "alert-error");
            return "/chitiet";
        }

        CongChucDTO congChucDTO = new CongChucDTO();
        if (congChucId > 0) {
            congChucDTO = congChucService.getCongChuc(congChucId, coQuanQuanLyId);
        }

        model.addAttribute("item", congChucDTO);
        model.addAttribute("listCoQuanQuanLy", listCoQuanQuanLy);

        return "chitiet";
    }

    @RenderMapping(params = "action=quyenHan")
    public String quyenHan(Model model, RenderRequest request) {
        long congChucId = ParamUtil.getLong(request, "congChucId");
        CongChucDTO congChucDTO = congChucService.findByCongChucId(congChucId);
        List<VaiTroDTO> vaiTroDTOS = congChucService.getPermission(congChucId);

        model.addAttribute("vaiTroDTOS", vaiTroDTOS);
        model.addAttribute("tenCanBo", congChucDTO.getHoVaTen());
        model.addAttribute("tenChucVu", congChucDTO.getChucVu().getTen());

        return "quyenHan";
    }
}