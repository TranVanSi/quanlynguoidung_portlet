package sdt.nguoidung.congchuc.controllers;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.nguoidung.congchuc.dto.CongChucDTO;
import sdt.nguoidung.congchuc.dto.TaiKhoanNguoiDungDTO;
import sdt.nguoidung.congchuc.services.CongChucService;
import sdt.nguoidung.congchuc.services.TaiKhoanNguoiDungService;
import sdt.nguoidung.congchuc.services.UserService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

@Service
@RequiredArgsConstructor
public class CongChucValidate {

    private final CongChucService congChucService;
    private final UserService userService;
    private final TaiKhoanNguoiDungService taiKhoanNguoiDungService;

    public boolean validate(ActionRequest request, ActionResponse response, CongChucDTO congChucDraft, String email) throws SystemException {
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

        boolean valid = true;
        long congChucId = congChucDraft.getId();
        CongChucDTO congChucByMa = congChucService.findByMa(congChucDraft.getMa());
        if (Validator.isNotNull(congChucDraft.getMa()) && Validator.isNotNull(congChucByMa)) {
            if ((congChucId == 0 && congChucByMa != null) ||
                    (congChucId > 0 && congChucByMa != null && congChucId != congChucByMa.getId())) {
                SessionErrors.add(request, "congchuc.validate.ma.trung");
                valid = false;
            }
        }


        if (email.length() > 0) {
            long userIdByEmail = userService.getUserIdByEmail(themeDisplay.getCompanyId(), email);

            TaiKhoanNguoiDungDTO taiKhoanNguoiDungByEmail = taiKhoanNguoiDungService.findByEmail(email);

            TaiKhoanNguoiDungDTO taiKhoanNguoiDungById = new TaiKhoanNguoiDungDTO();

            if (congChucId > 0 && congChucDraft.getTaiKhoanNguoiDung().getId() != null) {
                taiKhoanNguoiDungById = taiKhoanNguoiDungService.findByTaiKhoanNguoiDungId(congChucDraft.getTaiKhoanNguoiDung().getId());
            }

            if ((congChucId == 0 && (taiKhoanNguoiDungByEmail != null || userIdByEmail > 0))
                    || (congChucId > 0 && ((taiKhoanNguoiDungByEmail != null &&
                    !taiKhoanNguoiDungById.getUserId().equals(taiKhoanNguoiDungByEmail.getUserId()))
                    || (userIdByEmail > 0 && !taiKhoanNguoiDungById.getUserId().equals(userIdByEmail))))) {
                SessionErrors.add(request, "congchuc.validate.email.trung");
                valid = false;
            }
        }

        if (Validator.isNull(congChucDraft.getMa())) {
            SessionErrors.add(request, "validate.ma.null");
            valid = false;
        }
        System.out.println("valid=:"+valid);
        return valid;

    }
}
