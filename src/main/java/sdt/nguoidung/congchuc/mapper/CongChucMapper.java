package sdt.nguoidung.congchuc.mapper;

import com.liferay.portal.kernel.util.Validator;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import sdt.nguoidung.congchuc.dto.CongChucDTO;
import sdt.nguoidung.congchuc.dto.CongChucViewDTO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Mapper(componentModel = "spring")
public interface CongChucMapper {
    @Named("stringToDate")
    public static Date stringToDate(String date) throws Exception {
        Date result = null;

        if (Validator.isNotNull(date)) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            result = df.parse(date);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            sdf.format(result);
        }
        System.out.println("result=:"+result);
        return result;
    }
    @Mapping(source = "ngaySinh", target = "ngaySinh", qualifiedByName = "stringToDate")
    @Mapping(source = "ngayCapCMND", target = "ngayCapCMND", qualifiedByName = "stringToDate")
    CongChucViewDTO toCongChucViewDTO (CongChucDTO congChucDTO);

    @Named("stringDefaultToDate")
    public static Date stringDefaultToDate(String date) throws Exception {

        if (Validator.isNotNull(date)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.parse(date);
        }

        return null;
    }
    @Mapping(source = "ngaySinh", target = "ngaySinh", qualifiedByName = "stringDefaultToDate")
    @Mapping(source = "ngayCapCMND", target = "ngayCapCMND", qualifiedByName = "stringDefaultToDate")
    CongChucViewDTO toCongChucViewDTOFailded (CongChucDTO congChucDTO);
}
