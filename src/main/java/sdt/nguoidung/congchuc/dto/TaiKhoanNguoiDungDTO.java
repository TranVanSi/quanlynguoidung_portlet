package sdt.nguoidung.congchuc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Builder
@Accessors(chain = true)
public class TaiKhoanNguoiDungDTO{
    private Long id;
    private String tenDangNhap;
    private String matKhau;
    private String tenNguoiDung;
    private String cauHoiMatKhau;
    private String cauTraLoiMatKhau;
    private String email;
    private Integer trangThai;
    private Long userId;
    private LoaiDoiTuongDTO loaiDoiTuong;
    private CongChucDTO congChuc;

    public TaiKhoanNguoiDungDTO() {
    }
}
