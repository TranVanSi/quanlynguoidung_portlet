package sdt.nguoidung.congchuc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyDTO;

@Data
@AllArgsConstructor
@Builder
@Accessors(chain = true)
public class CongChucDTO {
    private long id;
    private String ma;
    private String hoVaTen;
    private String ngaySinh;
    private String soCMND;
    private String ngayCapCMND;
    private long noiCapId;
    private TaiKhoanNguoiDungDTO taiKhoanNguoiDung;
    private ChucVuDTO chucVu;
    private Long gioiTinhId;
    private CoQuanQuanLyDTO coQuanQuanLy;
    private String anh;
    private String dienThoaiDiDong;
    private String email;

    private int daXoa;
    public CongChucDTO() {
    }
}
