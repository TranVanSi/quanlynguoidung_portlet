package sdt.nguoidung.congchuc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyDTO;

import java.util.Date;

@Data
@AllArgsConstructor
@Builder
@Accessors(chain = true)
public class CongChucViewDTO {
    private long id;
    private String ma;
    private String hoVaTen;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date ngaySinh;
    private String soCMND;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date ngayCapCMND;
    private long noiCapId;
    private TaiKhoanNguoiDungDTO taiKhoanNguoiDung;
    private ChucVuDTO chucVu;
    private Long gioiTinhId;
    private CoQuanQuanLyDTO coQuanQuanLy;
    private String anh;
    private String dienThoaiDiDong;
    private String email;

    private int daXoa;
    public CongChucViewDTO() {
    }
}
