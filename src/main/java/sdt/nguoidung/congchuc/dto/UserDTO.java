package sdt.nguoidung.congchuc.dto;

import lombok.Data;

import java.util.Locale;

@Data
public class UserDTO {
    private Long userId;
    private Long companyId;
    private boolean autoPassword;
    private String password;
    private String rePassword;
    private String autoScreenName;
    private String screenName;
    private String emailAddress;
    private Long facebookId;
    private String openId;
    private Locale locale;
    private  String firstName;
    private String middleName;
    private String lastName;
    private Long prefixId;
    private Long suffixId;
    private int birthdayMonth;
    private int birthdayDay;
    private int birthdayYear;
    private String jobTitle;
    private Long[] groupIds;
    private Long[] roleIds;
    private Long[] userGroupIds;
    private boolean sendEmail;
    private Long creatorUserId;
    private String organizationName;
}
