package sdt.nguoidung.congchuc.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class DonViHanhChinhDTO  {
    private long id;
    private String ma;
    private String ten;
    private long chaId;
    private long capDonViHanhChinhId;
    private String tenCapDonViHanhChinh;
    private String maBuuCuc;
    private String nguoiTao;
    private String nguoiSua;
    private String chaMa;
    public DonViHanhChinhDTO() {
    }
}
