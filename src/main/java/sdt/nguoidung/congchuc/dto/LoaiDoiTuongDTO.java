package sdt.nguoidung.congchuc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class LoaiDoiTuongDTO {
    private Long id;
    private String ma;
    private String ten;
    private String moTa;
    private Integer trangThai;
    private List<TaiKhoanNguoiDungDTO> taiKhoanNguoiDungDTOList;

    public LoaiDoiTuongDTO(Long id) {
        this.id = id;
    }

    public LoaiDoiTuongDTO() {
    }
}
