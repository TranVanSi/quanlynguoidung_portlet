package sdt.nguoidung.congchuc.services;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringPool;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.nguoidung.congchuc.clients.UserClient;
import sdt.nguoidung.congchuc.dto.UserDTO;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserClient userClient;

    public UserDTO findByUserId(long userId) {

        return userClient.getUser(userId);
    }

    public long getUserIdByEmail(long companyId, String email) {

        return userClient.getUserIdByEmail(companyId, email);
    }

    public long saveUser(String hoVaTen, String email, String password, long creatorUserId, long companyId) throws Exception {
        return userClient.addUser(convertToUserDTO(hoVaTen, email, password, creatorUserId, companyId));

    }

    public boolean deleteUser(long userId) {

        return userClient.deleteUser(userId);

    }

    public boolean updateUser(UserDTO userDTO) {

        return userClient.updateUser(userDTO);

    }

    public UserDTO convertToUserDTO(String hoVaTen, String email, String password, long creatorUserId, long companyId) throws Exception {

        UserDTO user = new UserDTO();
        Organization organization = OrganizationLocalServiceUtil.getOrganization(companyId, "CB");
        System.out.println("organization=:"+organization.getOrganizationId());
        user.setScreenName(getScreenNameByEmail(email));
        user.setOpenId(StringPool.BLANK);
        user.setLocale(LocaleUtil.getDefault());
        String hoVaTenArray[] = hoVaTen.trim().split(" ");
        user.setFirstName(hoVaTenArray[0]);
        String middleName = hoVaTenArray.length >= 3 ? hoVaTenArray[1] : "";
        user.setMiddleName(middleName);
        user.setLastName(hoVaTen.substring(hoVaTen.indexOf(middleName) + middleName.length()));
        user.setJobTitle(StringPool.BLANK);
        user.setOrganizationName("CB");
        user.setPassword(password);
        user.setCreatorUserId(creatorUserId);
        user.setCompanyId(companyId);
        user.setEmailAddress(email);

        return user;
    }

    private String getScreenNameByEmail(String email) {

        long id = CounterLocalServiceUtil.increment("CB_SCREENNAME");

        if (email != null) {
            return email.substring(0, email.indexOf("@")) + id;
        }

        return "CB" + id;
    }

    public boolean ganChucVuToCanBo(long userId, String maChucVu, String maCoQuan) {

        return userClient.ganChucVuToCanBo(userId, maChucVu, maCoQuan);

    }

    public boolean checkOldPassword(String password, String email) {

        return userClient.checkOldPassword(password, email);
    }

    public boolean changePassword(String password, String email) {

        return userClient.changePassword(password, email);
    }
}
