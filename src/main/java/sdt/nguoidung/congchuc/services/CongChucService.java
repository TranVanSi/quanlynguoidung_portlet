package sdt.nguoidung.congchuc.services;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.stereotype.Service;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.chucvu.services.ChucVuService;
import sdt.nguoidung.congchuc.clients.CongChucClient;
import sdt.nguoidung.congchuc.dto.*;
import sdt.nguoidung.congchuc.mapper.CongChucMapper;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.services.CoQuanQuanLyService;
import sdt.nguoidung.utils.DateTimeUtils;
import sdt.nguoidung.utils.StringPool;
import sdt.nguoidung.utils.Utils;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CongChucService {

    private final CongChucClient congChucClient;
    private final UserService userService;
    private final ChucVuService chucVuService;
    private final CoQuanQuanLyService coQuanQuanLyService;
    private final TaiKhoanNguoiDungService taiKhoanNguoiDungService;

    public List<CongChucDTO> getListCongChuc(long coQuanQuanLyId) throws Exception {
        if (coQuanQuanLyId > 0) {
            return congChucClient.getListCongChucByCoQuanQuanLyId(coQuanQuanLyId);
        } else {
            return congChucClient.getListCongChuc();
        }
    }

    public Page<CongChucDTO> getCongChucRest(String keyword, int pageIndex, int size) {

        return congChucClient.getCongChucs(keyword, pageIndex, size);
    }

    public String getCongChucRestDataTable(DataTablesInput input) {

        return congChucClient.getCongChucRestDataTable(input);
    }

    public CongChucDTO findByCongChucId(long congChucId) {

        return congChucClient.getCongChuc(congChucId);
    }

    public CongChucDTO getCongChuc(long congChucId, long groupId) throws Exception {

        if (congChucId > 0) {
            CongChucDTO congChucDTO = findByCongChucId(congChucId);
            if (Validator.isNotNull(congChucDTO.getNgaySinh())) {
                congChucDTO.setNgaySinh(congChucDTO.getNgaySinh());
            }
            if (Validator.isNotNull(congChucDTO.getNgayCapCMND())) {
                congChucDTO.setNgayCapCMND(congChucDTO.getNgayCapCMND());
            }
            return congChucDTO;
        }
        CoQuanQuanLyDTO coQuanQuanLyDTO = coQuanQuanLyService.getCoQuanQuanLy(groupId);
        return new CongChucDTO().setCoQuanQuanLy(coQuanQuanLyDTO);
    }

    public CongChucDTO findByMa(String ma) {

        return congChucClient.findByMa(ma);
    }

    public String getCongChucJson(String keyword, int pageIndex, int size) {

        return congChucClient.getCongChucJson(keyword, pageIndex, size);
    }

    public boolean saveCongChuc(CongChucDTO congChucDTO, String email, ThemeDisplay themeDisplay, long coQuanQuanLyId) throws Exception {

        long userId = 0;
        String maCoQuan = getMaCoQuanByCQQLId(coQuanQuanLyId);
        if (Validator.isNotNull(congChucDTO.getId())) {
            TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO = new TaiKhoanNguoiDungDTO();
            if (Validator.isNotNull(congChucDTO.getTaiKhoanNguoiDung().getId()) &&
                    congChucDTO.getTaiKhoanNguoiDung().getId() > 0) {
                taiKhoanNguoiDungDTO = taiKhoanNguoiDungService.findByTaiKhoanNguoiDungId(congChucDTO.getTaiKhoanNguoiDung().getId());
                if (taiKhoanNguoiDungDTO.getUserId() != null) {
                    UserDTO userDTO = userService.findByUserId(taiKhoanNguoiDungDTO.getUserId());
                    userService.updateUser(Utils.updateUserLiferay(congChucDTO.getHoVaTen(), userDTO));
                    userId = taiKhoanNguoiDungDTO.getUserId();
                } else {
                    userId = userService.saveUser(congChucDTO.getHoVaTen(), email, StringPool.PASSWORD_DEFAULT, themeDisplay.getUserId(), themeDisplay.getCompanyId());
                    taiKhoanNguoiDungDTO.setUserId(userId);
                    taiKhoanNguoiDungDTO.setMatKhau(Utils.md5(StringPool.PASSWORD_DEFAULT));
                    taiKhoanNguoiDungDTO = taiKhoanNguoiDungService.saveTaiKhoanNguoiDung(taiKhoanNguoiDungDTO);
                }

            } else {

                userId = userService.saveUser(congChucDTO.getHoVaTen(), email, StringPool.PASSWORD_DEFAULT, themeDisplay.getUserId(), themeDisplay.getCompanyId());
                taiKhoanNguoiDungDTO = taiKhoanNguoiDungService.convertCongChucToTaiKhoanNguoiDung(
                        congChucDTO.getHoVaTen(), userId, email, StringPool.PASSWORD_DEFAULT, new LoaiDoiTuongDTO(6L));

            }
            if (Validator.isNotNull(congChucDTO.getNgaySinh())){
                congChucDTO.setNgaySinh(DateTimeUtils.convertPatternDates(congChucDTO.getNgaySinh()));
            }
            if (Validator.isNotNull(congChucDTO.getNgayCapCMND())){
                congChucDTO.setNgayCapCMND(DateTimeUtils.convertPatternDates(congChucDTO.getNgayCapCMND()));
            }
            congChucDTO.setTaiKhoanNguoiDung(taiKhoanNguoiDungDTO);
            congChucClient.updateCongChuc(congChucDTO);

        } else {

            userId = userService.saveUser(congChucDTO.getHoVaTen(), email, StringPool.PASSWORD_DEFAULT, themeDisplay.getUserId(), themeDisplay.getCompanyId());
            TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO = taiKhoanNguoiDungService.convertCongChucToTaiKhoanNguoiDung(
                    congChucDTO.getHoVaTen(), userId, email, StringPool.PASSWORD_DEFAULT, new LoaiDoiTuongDTO(6L));

            congChucDTO.setTaiKhoanNguoiDung(taiKhoanNguoiDungDTO);
            if (Validator.isNotNull(congChucDTO.getNgaySinh())){
                congChucDTO.setNgaySinh(DateTimeUtils.convertPatternDates(congChucDTO.getNgaySinh()));
            }
            if (Validator.isNotNull(congChucDTO.getNgayCapCMND())){
                congChucDTO.setNgayCapCMND(DateTimeUtils.convertPatternDates(congChucDTO.getNgayCapCMND()));
            }
            congChucClient.addCongChuc(congChucDTO);
        }

        ChucVuDTO chucVuDTO = chucVuService.findByChucVuId(congChucDTO.getChucVu().getId());
        userService.ganChucVuToCanBo(userId, chucVuDTO.getMa(), maCoQuan);

        return true;

    }

    public boolean deleteCongChuc(long congChucId) {
        CongChucDTO congChucDTO = findByCongChucId(congChucId);
        TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO = taiKhoanNguoiDungService.findByTaiKhoanNguoiDungId(congChucDTO.getTaiKhoanNguoiDung().getId());
        if (taiKhoanNguoiDungDTO != null) {
            if (taiKhoanNguoiDungDTO.getUserId() != null) {
                UserDTO userDTO = userService.findByUserId(taiKhoanNguoiDungDTO.getUserId());
                if (userDTO != null) {
                    userService.deleteUser(userDTO.getUserId());
                }
            }
        }
        return congChucClient.deleteCongChuc(congChucId);
    }

    public List<CongChucDTO> findTop1000() {

        return congChucClient.findTop1000();
    }

    public List<CongChucDTO> findTop1000ByIdNotIn(String congChucIds) {

        return congChucClient.findTop1000ByIdNotIn(congChucIds);
    }

    public List<CongChucDTO> getCongChucsToCongChuc2CoQuan(List<Long> congChucByCoQuanId, long congChucId) {
        List<CongChucDTO> congChucs = findTop1000ByIdNotIn(Utils.convertListToString(congChucByCoQuanId));

        if (congChucId > 0) {
            congChucs.add(findByCongChucId(congChucId));
        }

        return congChucs;
    }

    public String getMaCoQuanByCQQLId(long coQuanQuanLyId) throws Exception {
        CoQuanQuanLyDTO coQuanQuanLyDTO = coQuanQuanLyService.getCoQuanQuanLy(coQuanQuanLyId);
        return Validator.isNotNull(coQuanQuanLyDTO) ? coQuanQuanLyDTO.getMa() : "";
    }

    public Page<CongChucDTO> searchCongChuc(long coQuanQuanLyId, String keyword, int offset, int limit) {

        int pageIndex = offset / limit;

        return congChucClient.searchCongChuc(coQuanQuanLyId, keyword, pageIndex, limit);
    }

    public List<VaiTroDTO> getPermission(long congChucId) {

        return congChucClient.getPermission(congChucId);
    }

    public CongChucDTO getCongChucById(long id, long coQuanQuanLyId) throws Exception {

        CongChucDTO congChucDTO = new CongChucDTO();

        if (coQuanQuanLyId > 0) {
            CoQuanQuanLyDTO coQuanQuanLyDTO = coQuanQuanLyService.getCoQuanQuanLy(coQuanQuanLyId);
            congChucDTO.setCoQuanQuanLy(coQuanQuanLyDTO);
        }
        if (id > 0) {
            congChucDTO = getCongChuc(id, coQuanQuanLyId);
        }
        return congChucDTO;
    }

}
