package sdt.nguoidung.congchuc.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.nguoidung.congchuc.clients.ChucVu2CoQuan2VaiTroClient;
import sdt.nguoidung.congchuc.clients.CongChucClient;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuanDTO;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ChucVu2CoQuan2VaiTroService {

    private final ChucVu2CoQuan2VaiTroClient chucVu2CoQuan2VaiTroClient;

    public List<ChucVu2CoQuan2VaiTroDTO> getChucVuByCoQuanQuanLy (long coQuanQuanLyId) {

        return chucVu2CoQuan2VaiTroClient.getChucVuByCoQuanQuanLy(coQuanQuanLyId);
    }

    public List<ChucVu2CoQuanDTO> getChucVuByCoQuanQuanLyId (long coQuanQuanLyId) {

        return chucVu2CoQuan2VaiTroClient.getChucVuByCoQuanQuanLyId(coQuanQuanLyId);
    }

}
