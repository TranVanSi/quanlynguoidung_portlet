package sdt.nguoidung.congchuc.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import sdt.nguoidung.congchuc.clients.TaiKhoanNguoiDungClient;
import sdt.nguoidung.congchuc.dto.LoaiDoiTuongDTO;
import sdt.nguoidung.congchuc.dto.TaiKhoanNguoiDungDTO;
import sdt.nguoidung.utils.Utils;

@Service
@RequiredArgsConstructor
public class TaiKhoanNguoiDungService {
    
    private final TaiKhoanNguoiDungClient taiKhoanNguoiDungClient;
    
    public Page<TaiKhoanNguoiDungDTO> getTaiKhoanNguoiDungRest(String keyword, int pageIndex, int size){

        return taiKhoanNguoiDungClient.getTaiKhoanNguoiDungs(keyword, pageIndex, size);
    }

    public TaiKhoanNguoiDungDTO findByTaiKhoanNguoiDungId(long taiKhoanNguoiDungId){

        return taiKhoanNguoiDungClient.getTaiKhoanNguoiDung(taiKhoanNguoiDungId);
    }

    public TaiKhoanNguoiDungDTO saveTaiKhoanNguoiDung (TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO) {

        if (taiKhoanNguoiDungDTO.getId()!= null && taiKhoanNguoiDungDTO.getId() > 0) {

            return taiKhoanNguoiDungClient.updateTaiKhoanNguoiDung(taiKhoanNguoiDungDTO);
        }

        return taiKhoanNguoiDungClient.addTaiKhoanNguoiDung(taiKhoanNguoiDungDTO);

    }

    public TaiKhoanNguoiDungDTO convertCongChucToTaiKhoanNguoiDung (String hoVaTen, long userId,
                                                                    String email, String password, LoaiDoiTuongDTO loaiDoiTuong) throws Exception {
        TaiKhoanNguoiDungDTO taiKhoanNguoiDungDTO = new TaiKhoanNguoiDungDTO();
        taiKhoanNguoiDungDTO.setTenNguoiDung(hoVaTen);
        taiKhoanNguoiDungDTO.setUserId(userId);
        taiKhoanNguoiDungDTO.setTenDangNhap(email);
        taiKhoanNguoiDungDTO.setLoaiDoiTuong(loaiDoiTuong);
        taiKhoanNguoiDungDTO.setEmail(email);
        taiKhoanNguoiDungDTO.setMatKhau(Utils.md5(password));
        taiKhoanNguoiDungDTO.setTrangThai(1);

        return taiKhoanNguoiDungDTO;

    }

    public boolean deleteTaiKhoanNguoiDung (long taiKhoanNguoiDungId) {

        return taiKhoanNguoiDungClient.deleteTaiKhoanNguoiDung(taiKhoanNguoiDungId);

    }

    public TaiKhoanNguoiDungDTO findByEmail(String email){

        return taiKhoanNguoiDungClient.findByEmail(email);
    }
}
