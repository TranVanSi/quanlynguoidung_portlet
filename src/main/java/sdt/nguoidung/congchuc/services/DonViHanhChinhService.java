package sdt.nguoidung.congchuc.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.nguoidung.congchuc.clients.DonViHanhChinhClient;
import sdt.nguoidung.congchuc.dto.DonViHanhChinhDTO;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DonViHanhChinhService {
    
    private final DonViHanhChinhClient donViHanhChinhClient;

    public List<DonViHanhChinhDTO> findByCapId(long capId) {
        return donViHanhChinhClient.findDVHCByCapId(capId);
    }

}
