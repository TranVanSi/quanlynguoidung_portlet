package sdt.nguoidung.configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriTemplateHandler;
import sdt.nguoidung.congchuc.mapper.CongChucMapper;
import sdt.nguoidung.coquanquanly.mapper.CapCoQuanQuanLyMapper;
import sdt.nguoidung.coquanquanly.mapper.CoQuanQuanLyMapper;

@Configuration
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class Config {

    private final Environment env;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean("quanLyNguoiDungRest")
    public RestTemplate taiNguyenRestTemplate() {
        String rootURL = env.getProperty("api.v1.url.quanlynguoidung");

        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }

    @Bean("danhMucDungChungRest")
    public RestTemplate danhMucDungChungRestTemplate() {
        String rootURL = env.getProperty("api.v1.url.danhmucdungchung");

        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }

    @Bean("liferayRest")
    public RestTemplate liferayRestTemplate() {
        String rootURL = env.getProperty("api.v1.url.liferay");
        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }

    @Bean("objectMapper")
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    @Bean("capCoQuanQuanLyMapper")
    public CapCoQuanQuanLyMapper getCapCoQuanQuanLyMapper() {
        return Mappers.getMapper(CapCoQuanQuanLyMapper.class);
    }

    @Bean("coQuanQuanLyMapper")
    public CoQuanQuanLyMapper getCoQuanQuanLyMapper() {
        return Mappers.getMapper(CoQuanQuanLyMapper.class);
    }

    @Bean("congChucMapper")
    public CongChucMapper getCongChucMapper() {
        return Mappers.getMapper(CongChucMapper.class);
    }

}
