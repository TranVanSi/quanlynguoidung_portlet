package sdt.nguoidung.chucvu.dto;

import lombok.Data;

@Data
public class ChucVuDeleteDTO {
    private long id;
    private String nguoiSua;
}
