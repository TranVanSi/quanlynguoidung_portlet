package sdt.nguoidung.chucvu.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ChucVuInsertDTO extends Auditable<String> implements Serializable {
    private String ma;
    private String ten;

}
