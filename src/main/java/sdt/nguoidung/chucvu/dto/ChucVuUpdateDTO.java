package sdt.nguoidung.chucvu.dto;

import lombok.Data;

@Data
public class ChucVuUpdateDTO {
    private Long id;
    private String ma;
    private String ten;

    private String nguoiSua;
}
