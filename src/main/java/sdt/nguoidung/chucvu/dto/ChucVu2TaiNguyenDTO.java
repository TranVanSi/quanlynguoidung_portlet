package sdt.nguoidung.chucvu.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class ChucVu2TaiNguyenDTO {
    private long id;
    private String ma;
    private String ten;
    private List<ChucVu2CoQuan2VaiTroDTO> chucVu2CoQuan2VaiTros;

    public ChucVu2TaiNguyenDTO() {
    }
}
