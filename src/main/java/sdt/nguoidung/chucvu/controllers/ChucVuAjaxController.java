package sdt.nguoidung.chucvu.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.chucvu.services.ChucVuService;
import sdt.nguoidung.congchuc.clients.CongChucClient;
import sdt.nguoidung.congchuc.dto.CongChucDTO;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class ChucVuAjaxController {

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final ChucVuService chucVuService;
    private final CongChucClient congChucClient;

    @ResourceMapping(value ="loadChucVu")
    public void loadChucVu(ResourceRequest request, ResourceResponse response,
                              @RequestParam(value = "keyword", defaultValue = "") String keyword,
                              @RequestParam(value = "pageIndex", defaultValue = "0") int pageIndex,
                              @RequestParam(value = "size", defaultValue = "20") int size) throws Exception{

        JSONObject jsonObject = new JSONObject();
        Page<ChucVuDTO> chucVuDTOPage = chucVuService.getChucVuRest(keyword, pageIndex, size);

        jsonObject.put("data", chucVuDTOPage.getContent());
        jsonObject.put("draw", 1);
        jsonObject.put("recordsTotal", chucVuDTOPage.getTotalElements());
        jsonObject.put("recordsFiltered", chucVuDTOPage.getTotalElements());

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(jsonObject.toString());
    }

    @ResourceMapping(value ="loadChucVuDataTable")
    public void loadChucVuDataTable(ResourceResponse response, DataTablesInput input) throws Exception{

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(chucVuService.getChucVuRestDataTable(input));
    }

    @ResourceMapping(value = "loadCongChucByChucVuId")
    public void loadCongChucByChucVuId(ResourceRequest request, ResourceResponse response,
                                             @RequestParam(value = "chucVuId", defaultValue = "0") long chucVuId) throws Exception {

        if (congChucClient.getListCongChucByChucVuId(chucVuId).size() > 0) {
            response.getWriter().write("true");

        } else {
            response.getWriter().write(new JSONObject().toString());
        }

    }
}
