package sdt.nguoidung.chucvu.controllers;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.chucvu.services.ChucVuService;
import sdt.nguoidung.utils.ResponseDTO;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class ChucVuController {

	private final ChucVuValidate chucVuValidate;
	private final ChucVuService chucVuService;
	private final Environment env;

	@RenderMapping
	public String view(Model model) throws Exception {

		List<ChucVuDTO> listView = chucVuService.getListChucVu();
		model.addAttribute("listView", listView);

		String fileTemp = env.getProperty("url.filemau.chucvu");
		model.addAttribute("fileTemp", fileTemp);

		return "danhsach";
	}

	@RenderMapping(params = "action=themMoi")
	public String themMoi(RenderRequest request, RenderResponse response, Model model,
						  @RequestParam(required = false, defaultValue = "true") String validation) throws Exception {
		long chucVuId = ParamUtil.getLong(request,"chucVuId");
		System.out.println("chucVuId=:"+chucVuId);
		if (validation.equals("false")) {
			SessionErrors.add(request, "alert-error");
			return "/themmoi";
		}
		String nguoiSua = PortalUtil.getUser(request).getEmailAddress();
		ChucVuDTO item = new ChucVuDTO();

			if (chucVuId > 0) {
				item = chucVuService.getChucVu(chucVuId);
			} else {
				item.setNguoiTao(nguoiSua);
			}

		item.setNguoiSua(nguoiSua);
		model.addAttribute("item", item);

		return "themmoi";
	}

	@ActionMapping(params = "action=themMoi")
	public void themMoi(ActionRequest actionRequest, ActionResponse actionResponse, SessionStatus sessionStatus,
						@ModelAttribute("item") ChucVuDTO chucVuDTO, BindingResult result, Model model) throws Exception {

		ChucVuDTO chucVuByMa = chucVuService.findByMa(chucVuDTO.getMa());

		if (!chucVuValidate.validate(actionRequest, chucVuDTO, chucVuByMa)) {
			actionResponse.setRenderParameter("validation", "false");
			actionResponse.setRenderParameter("action","themMoi");
			SessionErrors.add(actionRequest, "form-error");
			model.addAttribute("chucVu", chucVuDTO);

            return;
        }

		chucVuService.saveChucVu(chucVuDTO);
		sessionStatus.setComplete();
		SessionMessages.add(actionRequest, "form-success");
		actionResponse.setRenderParameter("action","");
	}

	@ActionMapping(params = "action=xoa")
	public void xoa(ActionRequest request, ActionResponse response, SessionStatus sessionStatus,
					@RequestParam(value = "chucVuId", defaultValue = "0") long chucVuId) throws PortalException, IOException {
		if (chucVuId > 0) {
			chucVuService.deleteChucVu(chucVuId);
		}

		sessionStatus.setComplete();
		SessionMessages.add(request, "form-success");
		response.setRenderParameter("action","");
	}

	@RenderMapping(params = "action=chiTiet")
	public String chiTiet(RenderRequest request, RenderResponse response, Model model,
						  @RequestParam(value = "chucVuId", defaultValue = "0") long chucVuId,
						  @RequestParam(required = false, defaultValue = "true") String validation) {


		if (validation.equals("false")) {
			SessionErrors.add(request, "alert-error");
			return "/chitiet";
		}

		if (chucVuId > 0) {
			model.addAttribute("item", chucVuService.getChucVu(chucVuId));
		} else {
			model.addAttribute("item", new ChucVuDTO());
		}

		return "chitiet";
	}

	@ActionMapping(params = "action=importFile")
	public void importFile(ActionRequest actionRequest, ActionResponse actionResponse,
						   SessionStatus sessionStatus, Model model) throws Exception {

		User user = PortalUtil.getUser(actionRequest);
		UploadPortletRequest request = PortalUtil.getUploadPortletRequest(actionRequest);
		File file = request.getFile("fileImport");

		if (Validator.isNotNull(user) && Validator.isNotNull(file) && file.length() > 0) {
			ResponseDTO responseDTO = chucVuService.uploadFile(file, user.getEmailAddress());

			if ((responseDTO.getStatusCode() == 200 && responseDTO.getMessage().length() > 0)
					|| responseDTO.getStatusCode() == 400) {
				model.addAttribute("thongBao", responseDTO.getMessage());
				SessionErrors.add(actionRequest, "form-error");
			} else {
				SessionMessages.add(actionRequest, "form-success");
			}
		}

	}
}