package sdt.nguoidung.chucvu.controllers;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;

import javax.portlet.ActionRequest;

@Service
@RequiredArgsConstructor
public class ChucVuValidate {

	public boolean validate(ActionRequest request, ChucVuDTO chucVuDraft, ChucVuDTO chucVuByMa) throws SystemException {

		boolean valid = true;
		long chucVuId = chucVuDraft.getId();

		if ((chucVuId == 0 && chucVuByMa != null) ||
				(chucVuId > 0 && chucVuByMa != null && chucVuId != chucVuByMa.getId())) {
			SessionErrors.add(request, "chucvu.validate.ma.trung");
			valid = false;
		}

		return valid;
	}
}
