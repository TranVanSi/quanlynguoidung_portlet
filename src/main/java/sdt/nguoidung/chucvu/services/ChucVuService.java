package sdt.nguoidung.chucvu.services;

import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.stereotype.Service;
import sdt.nguoidung.chucvu.clients.ChucVuClient;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.utils.ResponseDTO;
import sdt.nguoidung.utils.Utils;

import java.io.File;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChucVuService {
    
    private final ChucVuClient chucVuClient;
    
    public Page<ChucVuDTO> getChucVuRest(String keyword, int pageIndex, int size){

        return chucVuClient.getChucVus(keyword, pageIndex, size);
    }

    public String getChucVuRestDataTable(DataTablesInput input){

        return chucVuClient.getChucVuRestDataTable(input);
    }

    public List<ChucVuDTO> getListChucVu() throws Exception {
        return chucVuClient.getListChucVu();
    }

    public ChucVuDTO findByChucVuId(long chucVuId){

        return chucVuClient.getChucVu(chucVuId);
    }

    public ChucVuDTO getChucVu (long chucVuId) {

        if (chucVuId > 0) {
            return findByChucVuId(chucVuId);
        }

        return new ChucVuDTO();
    }

    public ChucVuDTO findByMa(String ma){

        return chucVuClient.findByMa(ma);
    }

    public String getChucVuJson(String keyword, int pageIndex, int size) {

        return chucVuClient.getChucVuJson(keyword, pageIndex, size);
    }

    public boolean saveChucVu (ChucVuDTO chucVuDTO) {

        if (Validator.isNotNull(chucVuDTO.getId())) {

            return chucVuClient.updateChucVu(chucVuDTO);
        }

        return chucVuClient.addChucVu(chucVuDTO);

    }

    public boolean deleteChucVu (long chucVuId) {

        return chucVuClient.deleteChucVu(chucVuId);


    }

    public List<ChucVuDTO> findTop1000(){

        return chucVuClient.findTop1000();
    }

    public List<ChucVuDTO> findTop1000ByIdNotIn(String chucVuIds){

        return chucVuClient.findTop1000ByIdNotIn(chucVuIds);
    }

    public List<ChucVuDTO> getChucVusToChucVu2CoQuan (List<Long> chucVuByCoQuanId, long chucVuId) {
        List<ChucVuDTO> chucVus = findTop1000ByIdNotIn(Utils.convertListToString(chucVuByCoQuanId));

        if (chucVuId > 0) {
            chucVus.add(findByChucVuId(chucVuId));
        }

        return chucVus;
    }

    public ResponseDTO uploadFile(File file, String nguoiTao) {
        return chucVuClient.uploadFileData(file, nguoiTao);
    }
}
