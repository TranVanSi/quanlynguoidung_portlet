package sdt.nguoidung.chucvu.clients;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.configs.RestPageImpl;
import sdt.nguoidung.utils.ResponseDTO;
import sdt.nguoidung.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class ChucVuClient {

    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;
    
    private final String path = "/api/v1/chucvu";
    private final String pathUpload = "/api/v1/upload";

    public ChucVuDTO getChucVu(long chucVuId) {

        String restURL = path+"/"+chucVuId;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<ChucVuDTO> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , new ParameterizedTypeReference<ChucVuDTO>(){});

        return responseEntity.getBody();
    }

    public List<ChucVuDTO> getListChucVu() throws IOException {

        List<ChucVuDTO> chucVuDTOS = new ArrayList<>();
        String restURL = path + Utils.SLASH + "/listAllChucVu";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , String.class);

        try {
            chucVuDTOS = objectMapper.readValue(responseEntity.getBody(), new TypeReference<List<ChucVuDTO>>() {});
        } catch (Exception e) {
            chucVuDTOS.add(objectMapper.readValue(responseEntity.getBody(), new TypeReference<ChucVuDTO>() {}));
        }
        return chucVuDTOS;
    }

    public Page<ChucVuDTO> getChucVus(String keyword, int pageIndex, int size){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<RestPageImpl<ChucVuDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<RestPageImpl<ChucVuDTO>>() {});

        return response.getBody();

    }

    public List<ChucVuDTO> findTop1000(){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findTop1000");
        ResponseEntity<List<ChucVuDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<ChucVuDTO>>() {});

        return response.getBody();

    }

    public List<ChucVuDTO> findTop1000ByIdNotIn(String chucVuIds){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findTop1000ByIdNotIn")
                .queryParam("chucVuIds", chucVuIds);
        ResponseEntity<List<ChucVuDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<ChucVuDTO>>() {});

        return response.getBody();

    }

    public String getChucVuRestDataTable(DataTablesInput input){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        HttpEntity<DataTablesInput> entity = new HttpEntity<>(input, headers);

       ResponseEntity<String> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.POST, entity, String.class);

        return response.getBody();

    }

    public String getChucVuJson(String keyword, int pageIndex, int size){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path)
                .queryParam("keyword", keyword)
                .queryParam("pageIndex", pageIndex)
                .queryParam("size", size);
        ResponseEntity<String> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, String.class);

        return response.getBody();

    }

    public ChucVuDTO findByMa(String ma){
        String restURL = path+"/findByMa";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL).queryParam("ma", ma);
        ResponseEntity<ChucVuDTO> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET,null, new ParameterizedTypeReference<ChucVuDTO>(){});

        return response.getBody();

    }

    public boolean addChucVu (ChucVuDTO chucVuDTO) {

        String restURL =path+"/create";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<ChucVuDTO> entity = new HttpEntity<>(chucVuDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean updateChucVu (ChucVuDTO chucVuDTO) {

        String restURL =path+"/update";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<ChucVuDTO> entity = new HttpEntity<>(chucVuDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean deleteChucVu (long chucVuId) {
        String restURL =path+"/delete/"+chucVuId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<?> entity = new HttpEntity(headers);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public ResponseDTO uploadFileData(File file, String nguoiTao) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file",  new FileSystemResource(file));
        map.add("nguoiTao", nguoiTao);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
        Utils.disableSslVerification();
        ResponseEntity<ResponseDTO> response = restTemplate.postForEntity(pathUpload + Utils.SLASH + "chucvu", requestEntity, ResponseDTO.class);

        return response.getBody();
    }
}
