package sdt.nguoidung.coquanquanly.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class ChucVu2CoQuan2VaiTroDTO {
    private Long id;
    private Long coQuanQuanLyId;
    private VaiTroDTO vaiTro;
    private ChucVuDTO chucVu;

    public ChucVu2CoQuan2VaiTroDTO() {
    }
}
