package sdt.nguoidung.coquanquanly.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CoQuanQuanLyInsertDTO extends Auditable<String> implements Serializable {

    private String ma;
    private String ten;
    private String diaChi;
    private CapCoQuanQuanLyDTO capCoQuanQuanLy;
    private Long chaId;
    private Integer daXoa;

    public CoQuanQuanLyInsertDTO() {

    }

}
