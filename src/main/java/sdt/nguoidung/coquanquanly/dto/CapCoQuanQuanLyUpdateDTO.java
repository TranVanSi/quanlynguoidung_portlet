package sdt.nguoidung.coquanquanly.dto;

import lombok.Data;

@Data
public class CapCoQuanQuanLyUpdateDTO {
    private Long id;
    private String ma;
    private String ten;
    private Long chaId;
    private Integer cap;

    private String nguoiSua;
}
