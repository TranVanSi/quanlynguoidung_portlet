package sdt.nguoidung.coquanquanly.dto;

import lombok.Data;

@Data
public class CapCoQuanQuanLyDeleteDTO {
    private Long id;
    private String nguoiSua;
}
