package sdt.nguoidung.coquanquanly.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CapCoQuanQuanLyInsertDTO extends Auditable<String> implements Serializable {
    private String ma;
    private String ten;
    private Long chaId;
    private Integer cap;
}
