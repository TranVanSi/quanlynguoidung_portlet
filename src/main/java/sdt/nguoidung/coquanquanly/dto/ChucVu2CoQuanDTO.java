package sdt.nguoidung.coquanquanly.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class ChucVu2CoQuanDTO {
    private Long id;
    private Long coQuanQuanLyId;
    private ChucVuDTO chucVu;

    public ChucVu2CoQuanDTO() {
    }
}
