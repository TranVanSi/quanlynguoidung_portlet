package sdt.nguoidung.coquanquanly.dto;


import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CapCoQuanQuanLyDTO extends Auditable<String> implements Serializable {
    private Long id;
    private String ma;
    private String ten;
    private Long chaId;
    private Integer cap;

    private List<CoQuanQuanLyDTO> coQuanQuanLys;
}
