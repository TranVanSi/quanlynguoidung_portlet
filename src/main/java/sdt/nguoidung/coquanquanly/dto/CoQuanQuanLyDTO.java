package sdt.nguoidung.coquanquanly.dto;

import lombok.Data;
import sdt.nguoidung.congchuc.dto.CongChucDTO;

import java.io.Serializable;
import java.util.List;

@Data
public class CoQuanQuanLyDTO extends Auditable<String> implements Serializable {

    private Long id;
    private String ma;
    private String ten;
    private String diaChi;
    private CapCoQuanQuanLyDTO capCoQuanQuanLy;
    private List<CongChucDTO> congChucs;
    private Long chaId;
    private Integer daXoa;

    private String nguoiTao;
    private String nguoiSua;
    public CoQuanQuanLyDTO () {

    }

}
