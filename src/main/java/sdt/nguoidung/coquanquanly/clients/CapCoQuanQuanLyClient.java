package sdt.nguoidung.coquanquanly.clients;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sdt.nguoidung.coquanquanly.dto.CapCoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.mapper.CapCoQuanQuanLyMapper;
import sdt.nguoidung.utils.DataClient;
import sdt.nguoidung.utils.Utils;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class CapCoQuanQuanLyClient {

    @Autowired
    @Qualifier("danhMucDungChungRest")
    public RestTemplate restTemplate;

    @Autowired
    public DataClient dataClient;

    @Autowired
    @Qualifier("capCoQuanQuanLyMapper")
    public CapCoQuanQuanLyMapper capCoQuanQuanLyMapper;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final String path = Utils.SLASH + "capcqql";

    public CapCoQuanQuanLyDTO getById(long id) throws IOException {

        String restURL = path + "/id?id=" + id ;
        String data = dataClient.getDataFromServer(restURL, HttpMethod.GET);

        if (data == null) {
            return null;
        }

        CapCoQuanQuanLyDTO item = objectMapper.readValue(data, CapCoQuanQuanLyDTO.class);

        return item;
    }

    public CapCoQuanQuanLyDTO getByMa(String ma) throws IOException {

        String restURL = path + "/code?ma=" + ma ;
        String data = dataClient.getDataFromServer(restURL, HttpMethod.GET);

        if (data == null) {
            return null;
        }

        CapCoQuanQuanLyDTO item = objectMapper.readValue(data, CapCoQuanQuanLyDTO.class);

        return item;
    }

    public List<CapCoQuanQuanLyDTO> getListCapCoQuanQuanLy() throws IOException {

        String data = dataClient.getDataFromServer(path, HttpMethod.GET);
        JSONObject jsonObject = new JSONObject(data);
        JSONArray result = jsonObject.getJSONArray("data");
        return objectMapper.readValue(result.toString(), new TypeReference<List<CapCoQuanQuanLyDTO>>() {});
    }

    public boolean addOrUpdate (CapCoQuanQuanLyDTO item) throws Exception {

        String restURL = path + Utils.SLASH + "insert";
        String data;

        CapCoQuanQuanLyDTO chaId = getById(item.getChaId());
        int cap = chaId.getCap() + 1;
        if (item.getId() > 0) {
            restURL = path + Utils.SLASH + "update";
            item.setCap(cap);
            data = objectMapper.writeValueAsString(capCoQuanQuanLyMapper.capCQQLDTOToCapCQQLUpdateDTO(item));
        } else {
            if (item.getChaId() == 0) {
                item.setCap(1);
            } else {

                item.setCap(cap);
            }
            data = objectMapper.writeValueAsString(capCoQuanQuanLyMapper.capCQQLDTOToCapCQQLInsertDTO(item));
        }

        JSONObject object = new JSONObject();
        object.put("data", new JSONObject(data));

        return dataClient.getDataFromServer(restURL, HttpMethod.POST, object.toString());
    }

    public boolean delete (CapCoQuanQuanLyDTO item) throws JsonProcessingException {

        String restURL = path + Utils.SLASH + "delete";
        String data = objectMapper.writeValueAsString(capCoQuanQuanLyMapper.capCQQLTOToCapCQQLDeleteDTO(item));

        JSONObject object = new JSONObject();
        object.put("data", new JSONObject(data));

        return dataClient.getDataFromServer(restURL, HttpMethod.POST, object.toString());
    }

}
