package sdt.nguoidung.coquanquanly.clients;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyDTO;
import sdt.nguoidung.utils.Utils;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CoQuanQuanLyClient {

    @Autowired
    @Qualifier("liferayRest")
    public RestTemplate liferayRestTemplate;

    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final String pathServiceLiferay = "/api/v1/usergroup";
    private final String path = "/api/v1/coquanquanly";
    private final String pathChucVu2CoQuan2VaiTro = "/api/v1/chucvu2coquan2vaitro";

    public List<ChucVu2CoQuan2VaiTroDTO> getListChucVu2CoQuan2VaiTro (long coQuanQuanLyId) throws Exception {

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pathChucVu2CoQuan2VaiTro + "/findByCoQuanQuanLyId")
                .queryParam("coQuanQuanLyId", coQuanQuanLyId);
        ResponseEntity<List<ChucVu2CoQuan2VaiTroDTO>> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<ChucVu2CoQuan2VaiTroDTO>>() {});

        return responseEntity.getBody();
    }

    public boolean addCoQuanQuanLy (CoQuanQuanLyDTO coQuanQuanLy) {

        String restURL =path+"/create";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<CoQuanQuanLyDTO> entity = new HttpEntity<>(coQuanQuanLy, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean updateCoQuanQuanLy (CoQuanQuanLyDTO coQuanQuanLy) {

        String restURL =path+"/update";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<CoQuanQuanLyDTO> entity = new HttpEntity<>(coQuanQuanLy, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public CoQuanQuanLyDTO getCoQuanQuanLy(long coQuanQuanLyId) throws Exception {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findById")
                .queryParam("coQuanQuanLyId", coQuanQuanLyId);
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , String.class);

        return objectMapper.readValue(responseEntity.getBody(), new TypeReference<CoQuanQuanLyDTO>() {});
    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLy() throws Exception {
        List<CoQuanQuanLyDTO> coQuanQuanLyDTOS = new ArrayList<>();
        String restURL = path + Utils.SLASH + "listAllCoQuanQuanLy";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , String.class);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            coQuanQuanLyDTOS = objectMapper.readValue(responseEntity.getBody(), new TypeReference<List<CoQuanQuanLyDTO>>() {});
        } catch (Exception e) {
            coQuanQuanLyDTOS = objectMapper.readValue(responseEntity.getBody(), new TypeReference<List<CoQuanQuanLyDTO>>() {});
        }
        return coQuanQuanLyDTOS;
    }

    public List<CoQuanQuanLyDTO> findByCapCoQuanQuanLyId(long capId) throws Exception {
        List<CoQuanQuanLyDTO> coQuanQuanLyDTOS = new ArrayList<>();
        String restURL = path + Utils.SLASH + "findByCap/"+capId;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , String.class);
        try {
            coQuanQuanLyDTOS = objectMapper.readValue(responseEntity.getBody(), new TypeReference<List<CoQuanQuanLyDTO>>() {});
        } catch (Exception e) {
            coQuanQuanLyDTOS.add(objectMapper.readValue(responseEntity.getBody(), new TypeReference<CoQuanQuanLyDTO>() {}));
        }
        return coQuanQuanLyDTOS;
    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLyByParentId(long parentId) throws Exception {
        List<CoQuanQuanLyDTO> coQuanQuanLyDTOS = new ArrayList<>();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/listCoQuanQuanLyByParentId")
                .queryParam("parentId", parentId);
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null , String.class);
        try {
            String data = responseEntity.getBody();
            coQuanQuanLyDTOS = objectMapper.readValue(responseEntity.getBody(), new TypeReference<List<CoQuanQuanLyDTO>>() {});
        } catch (Exception e) {
            coQuanQuanLyDTOS.add(objectMapper.readValue(responseEntity.getBody(), new TypeReference<CoQuanQuanLyDTO>() {}));
        }
        return coQuanQuanLyDTOS;
    }

    public String getGroups(long companyId, long parentGroupId, boolean site){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pathServiceLiferay+"/getGroups")
                .queryParam("companyId", companyId)
                .queryParam("parentGroupId", parentGroupId)
                .queryParam("site", site);

        ResponseEntity<String> response = liferayRestTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, String.class);

        return response.getBody();
    }

    public String getChucVus(DataTablesInput input, long coQuanId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path+"/findByCoQuanId/"+coQuanId);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        HttpEntity<DataTablesInput> entity = new HttpEntity<>(input, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.POST, entity, String.class);

        return response.getBody();
    }

    public List<ChucVu2CoQuan2VaiTroDTO> getCoQuan2ChucVu(long coQuanId, long chucVuId){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pathChucVu2CoQuan2VaiTro+"/findByCoQuanAndChucVu"+"/"+coQuanId+"/"+chucVuId);

        ResponseEntity<List<ChucVu2CoQuan2VaiTroDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<ChucVu2CoQuan2VaiTroDTO>>() {});

        return response.getBody();

    }

    public boolean deleteByCoQuanAndChucVu (long coQuanId, long chucVuId) {
        String restURL =pathChucVu2CoQuan2VaiTro+"/deleteByCoQuanAndChucVu/"+coQuanId+"/"+chucVuId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<?> entity = new HttpEntity(headers);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean createByCoQuanAndChucVu (long coQuanId, long chucVuId, List<VaiTroDTO> vaiTros) {

        String restURL =pathChucVu2CoQuan2VaiTro+"/createByCoQuanAndChucVu/"+coQuanId+"/"+chucVuId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<List<VaiTroDTO>> entity = new HttpEntity<>(vaiTros, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public List<Long> findChucVuByCoQuanId(long coQuanId){

        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pathChucVu2CoQuan2VaiTro+"/findChucVuByCoQuanId"+"/"+coQuanId);

            ResponseEntity<List<Long>> response = restTemplate.exchange(
                    builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<Long>>() {});

            return response.getBody();
        } catch (Exception e) {
            return Collections.emptyList();
        }



    }

    public boolean addUserGroup (long userId, String maCoQuan, String maChucVu, String roleIds) {

        String restURL =pathServiceLiferay+"/addUserGroup";

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL)
                .queryParam("userId", userId)
                .queryParam("maChucVu", maChucVu)
                .queryParam("maCoQuan", maCoQuan)
                .queryParam("roleIds", roleIds);

        HttpEntity<?> response = liferayRestTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                null, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public List<ChucVuDTO> getChucVusByCoQuanId(long coQuanId){

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pathChucVu2CoQuan2VaiTro + "/findChucVuByCoQuanQuanLyId/" + coQuanId);
        ResponseEntity<List<ChucVuDTO>> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<ChucVuDTO>>() {
                });

        return response.getBody();

    }

    public boolean deleteCoQuanQuanLy (long coQuanQuanLyId) {
        String restURL =path+"/delete/"+coQuanQuanLyId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<?> entity = new HttpEntity(headers);
        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, String.class, new Object[0]);

        return  ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public CoQuanQuanLyDTO findByMa(String ma){
        String restURL = path+"/findByMa";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL).queryParam("ma", ma);
        ResponseEntity<CoQuanQuanLyDTO> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET,null, new ParameterizedTypeReference<CoQuanQuanLyDTO>(){});

        return response.getBody();

    }
}
