package sdt.nguoidung.coquanquanly.services;

import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.stereotype.Service;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.coquanquanly.clients.CoQuanQuanLyClient;
import sdt.nguoidung.coquanquanly.dto.CapCoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.mapper.CoQuanQuanLyMapper;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CoQuanQuanLyService {

    @Autowired
    @Qualifier("coQuanQuanLyMapper")
    public CoQuanQuanLyMapper coQuanQuanLyMapper ;
    private final CoQuanQuanLyClient coQuanQuanLyClient;

    public List<ChucVu2CoQuan2VaiTroDTO> getListChucVu2CoQuan2VaiTro(long coQuanQuanLyId) throws Exception {
        return coQuanQuanLyClient.getListChucVu2CoQuan2VaiTro(coQuanQuanLyId);
    }

    public boolean saveCoQuanQuanLy(CoQuanQuanLyDTO coQuanQuanLy, long cqqlChaId) {
        if (Validator.isNotNull(coQuanQuanLy.getId())) {
            if (cqqlChaId >0) {
                coQuanQuanLy.setId(null);
                return coQuanQuanLyClient.addCoQuanQuanLy(coQuanQuanLy);
            } else {
                return coQuanQuanLyClient.updateCoQuanQuanLy(coQuanQuanLy);
            }
        }

        return coQuanQuanLyClient.addCoQuanQuanLy(coQuanQuanLy);

    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLy() throws Exception {
        return coQuanQuanLyClient.getListCoQuanQuanLy();
    }

    public List<CoQuanQuanLyDTO> getListCoQuanQuanLy(long parentId) throws Exception {
        if(parentId != -1) {
            return coQuanQuanLyClient.getListCoQuanQuanLyByParentId(parentId);
        } else {
            return coQuanQuanLyClient.getListCoQuanQuanLy();
        }
    }

    public CoQuanQuanLyDTO getCoQuanQuanLy(long coQuanQuanLyId) throws Exception {
        CoQuanQuanLyDTO coQuanQuanLyDTO = new CoQuanQuanLyDTO();
        if (coQuanQuanLyId > 0) {
            coQuanQuanLyDTO = coQuanQuanLyClient.getCoQuanQuanLy(coQuanQuanLyId);
        }
        return coQuanQuanLyDTO;
    }

    public String getGroups(long companyId, long parentGroupId, boolean site) {

        return coQuanQuanLyClient.getGroups(companyId, parentGroupId, site);
    }

    public String getChucVus(DataTablesInput input, long coQuanId) {
        return coQuanQuanLyClient.getChucVus(input, coQuanId);
    }

    public List<ChucVu2CoQuan2VaiTroDTO> getCoQuan2ChucVu(long coQuanId, long chucVuId) {
        return coQuanQuanLyClient.getCoQuan2ChucVu(coQuanId, chucVuId);
    }

    public List<Long> findChucVuByCoQuanId(long coQuanId) {
        return coQuanQuanLyClient.findChucVuByCoQuanId(coQuanId);
    }

    public boolean deleteByCoQuanAndChucVu(long coQuanId, long chucVuId) {
        return coQuanQuanLyClient.deleteByCoQuanAndChucVu(coQuanId, chucVuId);
    }

    public boolean createByCoQuanAndChucVu(long coQuanId, long chucVuId, List<VaiTroDTO> vaiTros) {
        return coQuanQuanLyClient.createByCoQuanAndChucVu(coQuanId, chucVuId, vaiTros);
    }

    public boolean addUserGroup(long userId, String maCoQuan, String maChucVu, String roleIds) {
        return coQuanQuanLyClient.addUserGroup(userId, maCoQuan, maChucVu, roleIds);
    }

    public List<ChucVuDTO> getChucVusByCoQuanId (long coQuanId) {
        return coQuanQuanLyClient.getChucVusByCoQuanId(coQuanId);
    }

    public String getTieuDe(long coQuanQuanLyId) {
        String tieuDe = "Thêm mới cơ quan quản lý";

        if (coQuanQuanLyId > 0) {
            tieuDe = "Chỉnh sửa cơ quan quản lý";
        }

        return tieuDe;
    }

    public CapCoQuanQuanLyDTO getCapCoQuanQuanLy (CoQuanQuanLyDTO coQuanQuanLyDTO) {
        return coQuanQuanLyDTO.getCapCoQuanQuanLy();
    }

    public CoQuanQuanLyDTO getCoQuanQuanLyToCreateChild(CoQuanQuanLyDTO coQuanQuanLyDTO) {
        coQuanQuanLyDTO.setChaId(0l);
        coQuanQuanLyDTO.setTen("");
        coQuanQuanLyDTO.setMa("");
        coQuanQuanLyDTO.setDiaChi("");
        return coQuanQuanLyDTO;
    }

    public String getMaById (long coQuanQuanLyId) throws Exception {
        CoQuanQuanLyDTO coQuanQuanLyDTO = getCoQuanQuanLy(coQuanQuanLyId);
        return Validator.isNotNull(coQuanQuanLyDTO) ? coQuanQuanLyDTO.getMa() : "";
    }

    public boolean deleteCoQuanQuanLy (long coQuanQuanLyId) {
        return coQuanQuanLyClient.deleteCoQuanQuanLy(coQuanQuanLyId);
    }

    public CoQuanQuanLyDTO findByMa(String ma){

        return coQuanQuanLyClient.findByMa(ma);
    }
}
