package sdt.nguoidung.coquanquanly.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.nguoidung.coquanquanly.clients.CapCoQuanQuanLyClient;
import sdt.nguoidung.coquanquanly.dto.CapCoQuanQuanLyDTO;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CapCoQuanQuanLyService {
    
    private final CapCoQuanQuanLyClient capCoQuanQuanLyClient;

    public List<CapCoQuanQuanLyDTO> getListCapCoQuanQuanLy() throws IOException {
        return capCoQuanQuanLyClient.getListCapCoQuanQuanLy();
    }

    public CapCoQuanQuanLyDTO getById(long id) throws IOException {
        return capCoQuanQuanLyClient.getById(id);
    }

    public CapCoQuanQuanLyDTO getByMa(String ma) throws IOException {
        return capCoQuanQuanLyClient.getByMa(ma);
    }

    public boolean addOrUpdate(CapCoQuanQuanLyDTO item) throws Exception  {
        return capCoQuanQuanLyClient.addOrUpdate(item);
    }

    public boolean delete(CapCoQuanQuanLyDTO item) throws JsonProcessingException {
        return capCoQuanQuanLyClient.delete(item);
    }

}
