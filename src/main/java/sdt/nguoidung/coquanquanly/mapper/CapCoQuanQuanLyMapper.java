package sdt.nguoidung.coquanquanly.mapper;

import org.mapstruct.Mapper;
import sdt.nguoidung.coquanquanly.dto.CapCoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.dto.CapCoQuanQuanLyDeleteDTO;
import sdt.nguoidung.coquanquanly.dto.CapCoQuanQuanLyInsertDTO;
import sdt.nguoidung.coquanquanly.dto.CapCoQuanQuanLyUpdateDTO;

@Mapper(componentModel = "spring")
public interface CapCoQuanQuanLyMapper {
    CapCoQuanQuanLyDeleteDTO capCQQLTOToCapCQQLDeleteDTO(CapCoQuanQuanLyDTO capCoQuanQuanLyDTO);
    CapCoQuanQuanLyUpdateDTO capCQQLDTOToCapCQQLUpdateDTO(CapCoQuanQuanLyDTO capCoQuanQuanLyDTO);
    CapCoQuanQuanLyInsertDTO capCQQLDTOToCapCQQLInsertDTO(CapCoQuanQuanLyDTO capCoQuanQuanLyDTO);
}
