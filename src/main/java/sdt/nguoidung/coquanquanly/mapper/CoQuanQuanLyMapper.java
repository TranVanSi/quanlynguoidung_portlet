package sdt.nguoidung.coquanquanly.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyInsertDTO;

@Mapper(componentModel = "spring")
public interface CoQuanQuanLyMapper {
    @Mapping(source = "id", target = "chaId")
    CoQuanQuanLyInsertDTO toCoQuanQuanLyInsertDTO(CoQuanQuanLyDTO coQuanQuanLyDTO);
}
