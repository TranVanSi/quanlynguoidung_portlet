package sdt.nguoidung.coquanquanly.controllers;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.services.CoQuanQuanLyService;

import javax.portlet.ActionRequest;

@Service
@RequiredArgsConstructor
public class CoQuanQuanLyValidate {
	private final CoQuanQuanLyService coQuanQuanLyService;

	public boolean validate(ActionRequest request, CoQuanQuanLyDTO cqqlDraft) throws SystemException {

		boolean valid = true;
		CoQuanQuanLyDTO coQuanQuanLyByMa = coQuanQuanLyService.findByMa(cqqlDraft.getMa());

		if (Validator.isNotNull(cqqlDraft.getMa()) && Validator.isNotNull(coQuanQuanLyByMa)){
			if ((cqqlDraft.getId() == null && coQuanQuanLyByMa.getId() != null) ||
					(cqqlDraft.getId() != null && coQuanQuanLyByMa.getId() != null && !cqqlDraft.getId().equals(coQuanQuanLyByMa.getId()))) {
				SessionErrors.add(request, "coquanquanly.validate.ma.trung");
				valid = false;
			}
		}

		if (Validator.isNull(cqqlDraft.getMa())) {
			SessionErrors.add(request, "validate.ma.null");
			valid = false;
		}

		return valid;
	}
}
