package sdt.nguoidung.coquanquanly.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.util.PortalUtil;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.chucvu.services.ChucVuService;
import sdt.nguoidung.congchuc.clients.ChucVu2CoQuan2VaiTroClient;
import sdt.nguoidung.congchuc.clients.CongChucClient;
import sdt.nguoidung.congchuc.dto.CongChucDTO;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.nguoidung.coquanquanly.services.CoQuanQuanLyService;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;
import sdt.nguoidung.vaitro.services.VaiTroService;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class CoQuanQuanLyAjaxController {

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final CoQuanQuanLyService coQuanQuanLyService;
    private final ChucVuService chucVuService;
    private final VaiTroService vaiTroService;
    private final CongChucClient congChucClient;
    private final ChucVu2CoQuan2VaiTroClient chucVu2CoQuan2VaiTroClient;

    @ResourceMapping(value = "loadCoQuanQuanLyDataTable")
    public void loadCoQuanQuanLyDataTable(ResourceResponse response, DataTablesInput input,
                                          @RequestParam(value = "coQuanId", defaultValue = "0") long coQuanId) throws Exception {

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(coQuanQuanLyService.getChucVus(input, coQuanId));
    }

    @ResourceMapping(value = "addCoQuan")
    public void addCoQuan(ResourceRequest request, @RequestParam(value = "vaiTroHeThongDuocChon", defaultValue = "") String vaiTroHeThongDuocChon,
                          @RequestParam(value = "chucVuId", defaultValue = "0") long chucVuId,
                          @RequestParam(value = "coQuanId", defaultValue = "0") long coQuanId) throws Exception {

        if (coQuanId > 0 && chucVuId > 0) {
            coQuanQuanLyService.deleteByCoQuanAndChucVu(coQuanId, chucVuId);
        }
        String maCoQuan = coQuanQuanLyService.getMaById(coQuanId);
        ChucVuDTO chucVuDTO = chucVuService.findByChucVuId(chucVuId);

        List<VaiTroDTO> vaiTros = vaiTroService.findByVaiTroIn(vaiTroHeThongDuocChon);

        String roleIdsByVaiTros = vaiTroService.getRoleIdsByVaiTros(vaiTroService.findByVaiTroIn(vaiTroHeThongDuocChon));

        coQuanQuanLyService.createByCoQuanAndChucVu(coQuanId, chucVuId, vaiTros);
        coQuanQuanLyService.addUserGroup(PortalUtil.getUserId(request), maCoQuan, chucVuDTO.getMa(), roleIdsByVaiTros);
    }


    @ResourceMapping(value = "loadCongChucByCoQuanQuanLyId")
    public void loadCongChucByCoQuanQuanLyId(ResourceRequest request, ResourceResponse response,
                                @RequestParam(value = "coQuanQuanLyId", defaultValue = "0") long coQuanQuanLyId) throws Exception {

        if (congChucClient.getListCongChucByCoQuanQuanLyId(coQuanQuanLyId).size() > 0 || chucVu2CoQuan2VaiTroClient.getChucVuByCoQuanQuanLy(coQuanQuanLyId).size() > 0) {
            response.getWriter().write("true");

        } else {
            response.getWriter().write(new JSONObject().toString());

        }
    }

}
