package sdt.nguoidung.coquanquanly.controllers;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import sdt.nguoidung.chucvu.dto.ChucVuDTO;
import sdt.nguoidung.chucvu.services.ChucVuService;
import sdt.nguoidung.coquanquanly.dto.CapCoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.dto.ChucVu2CoQuan2VaiTroDTO;
import sdt.nguoidung.coquanquanly.dto.CoQuanQuanLyDTO;
import sdt.nguoidung.coquanquanly.mapper.CoQuanQuanLyMapper;
import sdt.nguoidung.coquanquanly.services.CapCoQuanQuanLyService;
import sdt.nguoidung.coquanquanly.services.CoQuanQuanLyService;
import sdt.nguoidung.utils.LiferayURL;
import sdt.nguoidung.vaitro.dto.VaiTroDTO;
import sdt.nguoidung.vaitro.services.VaiTroService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class CoQuanQuanLyController {

    private final CoQuanQuanLyService coQuanQuanLyService;
    private final CapCoQuanQuanLyService capCoQuanQuanLyService;
    private final ChucVuService chucVuService;
    private final VaiTroService vaiTroService;
    private final CoQuanQuanLyValidate coQuanQuanLyValidate;

    @RenderMapping
    public String view(Model model, RenderRequest request) throws Exception {
        long parentId = ParamUtil.getLong(request, "parentId", -1);
        long cqqlChaId = ParamUtil.getLong(request, "cqqlChaId", -1);
        long coQuanQuanLyId = ParamUtil.getLong(request, "coQuanQuanLyId");
        List<CoQuanQuanLyDTO> listView = coQuanQuanLyService.getListCoQuanQuanLy(parentId);
        String cauHinhCongChucForm = LiferayURL
                .createRenderURL(request, "congchuc_WAR_sdt_quanlynguoidung_portlet", null)
                .setParameter("coQuanQuanLyId", coQuanQuanLyId)
                .setParameter("backUrl", ParamUtil.getString(request, "backUrl"))
                .toString();

        CoQuanQuanLyDTO coQuanQuanLyDTO = coQuanQuanLyService.getCoQuanQuanLy(cqqlChaId);

        model.addAttribute("jspPage", cauHinhCongChucForm);
        model.addAttribute("listView", listView);
        model.addAttribute("cqqlChaId", cqqlChaId);
        model.addAttribute("tieuDe", Validator.isNotNull(coQuanQuanLyDTO) ? coQuanQuanLyDTO.getTen() : "");

        return "danhsach";
    }

    @ActionMapping(params = "action=timKiem")
    public void timKiem(ActionResponse actionResponse, @RequestParam(value="parentId", defaultValue="-1") long parentId) {
        actionResponse.setRenderParameter("action","");
        actionResponse.setRenderParameter("parentId",String.valueOf(parentId));
        actionResponse.setRenderParameter("cqqlChaId",String.valueOf(parentId));
    }

    @RenderMapping(params = "action=themMoi")
    public String themMoi(RenderRequest request, RenderResponse response, Model model) throws Exception {
        String validation = ParamUtil.getString(request, "validation","true");
        long coQuanQuanLyId = ParamUtil.getLong(request, "coQuanQuanLyId");
        long cqqlChaId = ParamUtil.getLong(request, "cqqlChaId");
        /*List<CapCoQuanQuanLyDTO> capCoQuanQuanLyDTOList = capCoQuanQuanLyService.getListCapCoQuanQuanLy();*/
        /*List<CoQuanQuanLyDTO> listCoQuanQuanLy = coQuanQuanLyService.getListCoQuanQuanLy();

        model.addAttribute("listCoQuanQuanLy",listCoQuanQuanLy);
        model.addAttribute("capCoQuanQuanLys", capCoQuanQuanLyDTOList);*/

        if (validation.equals("false")) {
            SessionErrors.add(request, "alert-error");
            return "themmoi";
        }
        String tieuDe = coQuanQuanLyService.getTieuDe(coQuanQuanLyId);
        CoQuanQuanLyDTO coQuanQuanLy = new CoQuanQuanLyDTO();

        if (coQuanQuanLyId > 0) {
            coQuanQuanLy = coQuanQuanLyService.getCoQuanQuanLy(coQuanQuanLyId);

            if (cqqlChaId > 0 && coQuanQuanLy != null) {
                coQuanQuanLy.setChaId(cqqlChaId);
            }
        }

        model.addAttribute("coQuanQuanLy", coQuanQuanLy);
        model.addAttribute("tieuDe", tieuDe);

        return "themmoi";
    }

    @ActionMapping(params = "action=themMoi")
    public void themMoi(ActionRequest actionRequest, ActionResponse actionResponse,
                        @ModelAttribute("coQuanQuanLy") CoQuanQuanLyDTO coQuanQuanLyDTO,
                        BindingResult result, SessionStatus sessionStatus, Model model,
                        @RequestParam(value = "cqqlChaId", defaultValue = "0") long cqqlChaId) {
        System.out.println("coQuanQuanLyDTO=:"+coQuanQuanLyDTO);
        /*if (coQuanQuanLyValidate.validate(actionRequest, coQuanQuanLyDTO)) {*/
            coQuanQuanLyService.saveCoQuanQuanLy(coQuanQuanLyDTO, cqqlChaId);
            sessionStatus.setComplete();
            actionResponse.setRenderParameter("action", "");

          /*  return;
        }*/
        model.addAttribute("coQuanQuanLy", coQuanQuanLyDTO);
        actionResponse.setRenderParameter("validation", "false");
        SessionMessages.add(actionRequest, "form-success");
        actionResponse.setRenderParameter("action", "themMoi");
    }

    @RenderMapping(params = "action=xemChucVu")
    public String xemChucVu(Model model, RenderRequest request) throws Exception {
        long coQuanQuanLyId = ParamUtil.getLong(request, "coQuanQuanLyId");
        String tenCoQuan = ParamUtil.getString(request, "tenCoQuan");
        List<ChucVu2CoQuan2VaiTroDTO> chucVu2CoQuan2VaiTroDTOS = coQuanQuanLyService.getListChucVu2CoQuan2VaiTro(coQuanQuanLyId);

        Map<ChucVuDTO, List<ChucVu2CoQuan2VaiTroDTO>> chucVu2CoQuan2VaiTroDTOListMap = chucVu2CoQuan2VaiTroDTOS.stream()
                .collect(Collectors.groupingBy(ChucVu2CoQuan2VaiTroDTO::getChucVu));

        model.addAttribute("listView", chucVu2CoQuan2VaiTroDTOListMap);
        model.addAttribute("coQuanId", coQuanQuanLyId);
        model.addAttribute("tenCoQuan", tenCoQuan);

        return "xemchucvu";
    }

    @RenderMapping(params = "action=taoCapCon")
    public String taoCapCon(Model model, RenderRequest request) throws Exception {
        String validation = ParamUtil.getString(request, "validation","true");
        long chaId = ParamUtil.getLong(request, "parentId");
        if (validation.equals("false")) {
            SessionErrors.add(request, "alert-error");
            return "/themmoi";
        }
        CoQuanQuanLyDTO coQuanQuanLyDTO = new CoQuanQuanLyDTO();
        if (chaId >0) {
            coQuanQuanLyDTO.setChaId(chaId);
        }


        List<CapCoQuanQuanLyDTO> capCoQuanQuanLyDTOList = capCoQuanQuanLyService.getListCapCoQuanQuanLy();
        List<CoQuanQuanLyDTO> coQuanQuanLyDTOS = coQuanQuanLyService.getListCoQuanQuanLy();


        model.addAttribute("capCoQuanQuanLys", capCoQuanQuanLyDTOList);
        model.addAttribute("listCoQuanQuanLy", coQuanQuanLyDTOS);
        model.addAttribute("coQuanQuanLy", coQuanQuanLyDTO);
        model.addAttribute("cqqlChaId", chaId);

        return "themmoi";
    }

    @RenderMapping(params = "action=ganChucVu")
    public String ganChucVu(Model model, RenderRequest request) {
        long coQuanId = ParamUtil.getLong(request, "coQuanId");
        long chucVuId = ParamUtil.getLong(request, "chucVuId");
        String tenCoQuan = ParamUtil.getString(request, "tenCoQuan");
        List<Long> chucVuByCoQuanId = coQuanQuanLyService.findChucVuByCoQuanId(coQuanId);
        ChucVuDTO chucVu = new ChucVuDTO();
        if(chucVuId > 0) {
            model.addAttribute("isEdit", true);
            chucVu = chucVuService.getChucVu(chucVuId);
        } else {
            model.addAttribute("isEdit", false);
        }

        model.addAttribute("chucVuDTO", chucVu);
        model.addAttribute("chucVus", chucVuService.getChucVusToChucVu2CoQuan(chucVuByCoQuanId, chucVuId));
        model.addAttribute("coQuanId", coQuanId);
        model.addAttribute("chucVuId", chucVuId);
        model.addAttribute("tenCoQuan", tenCoQuan);

        List<VaiTroDTO> vaiTroDTOS = vaiTroService.getVaiTroByCoQuanIdAndChucVuId(coQuanId, chucVuId);

        model.addAttribute("danhSachVaiTroHeThongDuocChon", vaiTroDTOS);
        model.addAttribute("danhSachVaiTroHeThong", vaiTroService.findByVaiTroIsNotIn(vaiTroDTOS));

        return "ganchucvu";
    }

    @ActionMapping(params = "action=xoa")
    public void xoa(ActionRequest request, ActionResponse response, SessionStatus sessionStatus,
                    @RequestParam(value = "coQuanQuanLyId", defaultValue = "0") long coQuanQuanLyId) throws PortalException, IOException {
        if (coQuanQuanLyId > 0) {
            coQuanQuanLyService.deleteCoQuanQuanLy(coQuanQuanLyId);
        }

        sessionStatus.setComplete();
        SessionMessages.add(request, "form-success");
        response.setRenderParameter("action","");
    }

    @RenderMapping(params = "action=chiTiet")
    public String chiTiet(RenderRequest request, RenderResponse response, Model model) throws Exception {
        String validation = ParamUtil.getString(request, "validation","true");
        long coQuanQuanLyId = ParamUtil.getLong(request, "coQuanQuanLyId");
        List<CoQuanQuanLyDTO> listCoQuanQuanLy = coQuanQuanLyService.getListCoQuanQuanLy();
        if (validation.equals("false")) {
            SessionErrors.add(request, "alert-error");
            return "/chitiet";
        }

        if (coQuanQuanLyId > 0) {
            model.addAttribute("item", coQuanQuanLyService.getCoQuanQuanLy(coQuanQuanLyId));
        } else {
            model.addAttribute("item", new CoQuanQuanLyDTO());
        }

        model.addAttribute("listCoQuanQuanLy", listCoQuanQuanLy);

        return "chitiet";
    }

}