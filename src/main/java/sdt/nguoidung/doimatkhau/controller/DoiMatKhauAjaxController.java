package sdt.nguoidung.doimatkhau.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import sdt.nguoidung.congchuc.services.UserService;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class DoiMatKhauAjaxController {

    private final UserService userService;

    @ResourceMapping(value ="checkOldPassword")
    public void checkOldPassword(ResourceRequest request , ResourceResponse response,
                                      @RequestParam(value = "email", defaultValue = "") String email,
                                      @RequestParam(value = "passwordOld", defaultValue = "") String passwordOld) throws Exception{

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        response.getWriter().write(String.valueOf(userService.checkOldPassword(passwordOld, email)));
    }
}
