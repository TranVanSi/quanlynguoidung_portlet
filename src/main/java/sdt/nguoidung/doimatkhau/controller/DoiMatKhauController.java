package sdt.nguoidung.doimatkhau.controller;

import com.liferay.portal.kernel.util.PortalUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import sdt.nguoidung.congchuc.services.UserService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

@Controller
@RequestMapping("VIEW")
@RequiredArgsConstructor
public class DoiMatKhauController {

    private final UserService userService;

    @RequestMapping
    public String view () {
        return "doimatkhau";
    }

    @RequestMapping(params = "action=thanhcong")
    public String thanhCong () {
        return "thanhcong";
    }

    @ActionMapping(params = "action=doiMatKhau")
    public void doiMatKhau (@RequestParam(value = "matKhauMoi", defaultValue = "") String matKhauMoi,
                              ActionRequest actionRequest, ActionResponse actionResponse) throws Exception{
        String email = PortalUtil.getUser(actionRequest).getEmailAddress();
        userService.changePassword(matKhauMoi, email);

        actionResponse.setRenderParameter("action", "thanhcong");
    }
}
