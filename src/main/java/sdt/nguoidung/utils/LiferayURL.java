package sdt.nguoidung.utils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;

public class LiferayURL {
	
	public static final String AJAX_URL_MARK = "_is_ajax_request";
	public static final String DEFAULT_JSP = null;
	public static final String DEFAULT_ACTION = null;
	
	private LiferayPortletURL _url;
	
	public LiferayURL(LiferayPortletURL url) {
		_url = url;
	}
	
	public LiferayURL setParameter(String name, Object value) {
		if (_url != null && value != null) {
			_url.setParameter(name, value.toString());
		}
		return this;
	}
	
	public LiferayURL setWindowState(WindowState state) throws WindowStateException {
		if (_url != null) {
			_url.setWindowState(state);
		}
		return this;
	}
	
	public LiferayURL setPortletMode(PortletMode mode) throws PortletModeException {
		if (_url != null) {
			_url.setPortletMode(mode);
		}
		return this;
	}
	
	public String toString() {
		return (_url == null) ? Utils.SLASH : _url.toString();
	}
	
	public LiferayPortletURL getOriginalURL() {
		return _url;
	}
	
	/**
	 * @TODO Kiem tra xem portlet da duoc add vao page nao chua
	 * @param request
	 * @param portletName
	 * @return
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static final boolean isPortletAddedToPage(PortletRequest request, String portletName)
		throws PortalException, SystemException
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		return PortalUtil.getPlidFromPortletId(themeDisplay.getScopeGroupId(), portletName) != LayoutConstants.DEFAULT_PLID;
	}
	
	/**
	 * @TODO Kiem tra xem portlet da duoc add vao page nao chua
	 * @param request
	 * @param portletName
	 * @return
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static final boolean isPortletAddedToPage(HttpServletRequest request, String portletName)
		throws PortalException, SystemException
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		return PortalUtil.getPlidFromPortletId(themeDisplay.getScopeGroupId(), portletName) != LayoutConstants.DEFAULT_PLID; 
	}
	
	public final static LiferayURL createActionURL(
		HttpServletRequest request,
		String portletName,
		long plid,
		String actionName
	) {
		LiferayPortletURL actionURL = PortletURLFactoryUtil.create(request, portletName, plid, PortletRequest.ACTION_PHASE);
		try {
			actionURL.setWindowState(WindowState.NORMAL);
			actionURL.setPortletMode(PortletMode.VIEW);
			actionURL.setCopyCurrentRenderParameters(false);
		} catch (WindowStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PortletModeException e) {
			e.printStackTrace();
		}
		
		if (DEFAULT_ACTION != actionName) {
			actionURL.setParameter(ActionRequest.ACTION_NAME, actionName);
		}
		return new LiferayURL(actionURL);
	}
	
	public final static LiferayURL createActionURL(PortletRequest request, String portletName, String actionName)
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long plid = 0;
		try {
			plid = PortalUtil.getPlidFromPortletId(themeDisplay.getScopeGroupId(), portletName);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return createActionURL(PortalUtil.getHttpServletRequest(request), portletName, plid, actionName);
	}
	
	public final static LiferayURL createActionURL(HttpServletRequest request, String portletName, String actionName)
		throws Exception 
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long plid = PortalUtil.getPlidFromPortletId(themeDisplay.getScopeGroupId(), portletName); 
		return createActionURL(request, portletName, plid, actionName);
	}
	
	public final static LiferayURL createActionURL(PortletRequest request, String actionName)
		throws Exception
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String portletName = themeDisplay.getPortletDisplay().getId();
		long plid = themeDisplay.getPlid();
		return createActionURL(PortalUtil.getHttpServletRequest(request), portletName, plid, actionName);
	}
	
	public final static LiferayURL createActionURL(HttpServletRequest request, String actionName)
		throws Exception
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String portletName = themeDisplay.getPortletDisplay().getId();
		long plid = themeDisplay.getPlid();
		return createActionURL(request, portletName, plid, actionName);
	}
	
	public final static LiferayURL createRenderURL(
		HttpServletRequest request,
		String portletName,
		long plid,
		String jsp
	) throws WindowStateException, PortletModeException
	{
		LiferayPortletURL renderURL = PortletURLFactoryUtil.create(request, portletName, plid, PortletRequest.RENDER_PHASE);
		renderURL.setWindowState(WindowState.NORMAL);
		renderURL.setPortletMode(PortletMode.VIEW);
		renderURL.setCopyCurrentRenderParameters(false);
		if (DEFAULT_JSP != jsp) {
			renderURL.setParameter(StringPool.JSP_PAGE, jsp);
		}
		return new LiferayURL(renderURL);
	}
	
	public final static LiferayURL createRenderURL(PortletRequest request, String portletName, String jsp)
		throws Exception
	{
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		long plid = PortalUtil.getPlidFromPortletId(themeDisplay.getScopeGroupId(), portletName); 
		return createRenderURL(PortalUtil.getHttpServletRequest(request), portletName, plid, jsp);
	}
	
	public final static LiferayURL createRenderURL(HttpServletRequest request, String portletName, String jsp)
		throws Exception
	{
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		long plid = PortalUtil.getPlidFromPortletId(themeDisplay.getScopeGroupId(), portletName); 
		return createRenderURL(request, portletName, plid, jsp);
	}

	public final static LiferayURL createRenderURL(PortletRequest request, String jsp)
		throws Exception
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String portletName = themeDisplay.getPortletDisplay().getId();
		long plid = themeDisplay.getPlid();
		return createRenderURL(PortalUtil.getHttpServletRequest(request), portletName, plid, jsp);
	}
	
	public final static LiferayURL createRenderURL(HttpServletRequest request, String jsp)
		throws Exception
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String portletName = themeDisplay.getPortletDisplay().getId();
		long plid = themeDisplay.getPlid();
		return createRenderURL(request, portletName, plid, jsp);
	}
	
	public final static LiferayURL createRenderURL(PortletRequest request)
		throws Exception
	{
		return createRenderURL(request, DEFAULT_JSP);
	}
	
	public final static LiferayURL createRenderURL(HttpServletRequest request)
		throws Exception
	{
		return createRenderURL(request, DEFAULT_JSP);
	}
	
	public final static LiferayURL createResourceURL(HttpServletRequest request, String resourceId)
		throws Exception
	{
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		return createResourceURL(request, themeDisplay.getPortletDisplay().getId(), resourceId);
	}
	
	public final static LiferayURL createResourceURL(PortletRequest request, String resourceId)
		throws Exception
	{
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		return createResourceURL(request, themeDisplay.getPortletDisplay().getId(), resourceId);
	}
	
	public final static LiferayURL createResourceURL(HttpServletRequest request, String portletName, String resourceId)
		throws PortalException, SystemException
	{
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		long plid = PortalUtil.getPlidFromPortletId(themeDisplay.getScopeGroupId(), portletName);
		if (plid == LayoutConstants.DEFAULT_PLID) {
			plid = themeDisplay.getPlid();
		}
		LiferayPortletURL url = PortletURLFactoryUtil.create(request, portletName, plid, PortletRequest.RESOURCE_PHASE);
		url.setResourceID(resourceId);
		url.setCopyCurrentRenderParameters(false);
		return new LiferayURL(url);
	}
	
	public final static LiferayURL createResourceURL(PortletRequest request, String portletName, String resourceId)
		throws PortalException, SystemException
	{
		return createResourceURL(PortalUtil.getHttpServletRequest(request), portletName, resourceId);
	}

	public static LiferayURL createAjaxURL(HttpServletRequest request, String portletName, String methodName)
		throws PortalException, SystemException
	{
		return createResourceURL(request, portletName, methodName).setParameter(AJAX_URL_MARK, 1);
	}
	
	public static LiferayURL createAjaxURL(PortletRequest request, String portletName, String methodName)
		throws PortalException, SystemException
	{
		return createResourceURL(PortalUtil.getHttpServletRequest(request), portletName, methodName).setParameter(AJAX_URL_MARK, 1);
	}
	
	public static LiferayURL createAjaxURL(HttpServletRequest request, String methodName)
		throws PortalException, SystemException
	{
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		return createResourceURL(request, themeDisplay.getPortletDisplay().getId(), methodName).setParameter(AJAX_URL_MARK, 1);
	}
	
	public static LiferayURL createAjaxURL(PortletRequest request, String methodName)
		throws PortalException, SystemException
	{
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		return createResourceURL(PortalUtil.getHttpServletRequest(request), themeDisplay.getPortletDisplay().getId(), methodName).setParameter(AJAX_URL_MARK, 1);
	}
	
	public static boolean isAjaxRequest(PortletRequest request) {
		return ParamUtil.getInteger(request, AJAX_URL_MARK) != 0;
	}
	
	public static boolean isAjaxRequest(UploadPortletRequest request) {
		return ParamUtil.getInteger(request, AJAX_URL_MARK) != 0;
	}
}
