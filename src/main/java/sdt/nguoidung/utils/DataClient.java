package sdt.nguoidung.utils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import lombok.RequiredArgsConstructor;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:config.properties")
public class DataClient {

    @Value("${token.url}")
    private String tokenUrl;

    @Value("${token.clientId}")
    private String clientId;

    @Value("${token.clientSecret}")
    private String clientSecret;

    @Autowired
    @Qualifier("danhMucDungChungRest")
    public RestTemplate restTemplate;

    public static final Log _log = LogFactoryUtil.getLog(DataClient.class);

    public String getAccessToken() {

        return "";
    }

    public String getDataFromServer(String path, HttpMethod httpMethod) {

        String accessToken = "";
        Utils.disableSslVerification();

        if (accessToken == null) {
            return null;
        }

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);
        String data = null;

        _log.info("Get data from Server:" + path + " - Method: " + httpMethod);

        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(
                    builder.toUriString(), httpMethod, Utils.getHeaderToken(accessToken), String.class);
            data = responseEntity.getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public boolean getDataFromServer(String path, HttpMethod httpMethod, String body) {

        String accessToken = "";
        Utils.disableSslVerification();

        if (accessToken == null) {
            return false;
        }

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);

        _log.info("Get data from Server:" + path + " - Method: " + httpMethod+ " - Body: " + body);

        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(
                    builder.toUriString(), httpMethod, Utils.getHeaderTokenBody(accessToken, body), String.class);

            String response = responseEntity.getBody();

            if (response != null) {
                JSONObject object = new JSONObject(response);

                if (object.has("REQUEST_STATUS") && object.getString("REQUEST_STATUS").equalsIgnoreCase("SUCCESSFUL")) {
                    return true;
                }
            } else {
                return true;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
