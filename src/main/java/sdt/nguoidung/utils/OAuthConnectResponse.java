package sdt.nguoidung.utils;

import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;

public class OAuthConnectResponse extends OAuthJSONAccessTokenResponse {

    @Override
    protected void init(String body, String contentType, int responseCode)
            throws OAuthProblemException {
        super.init(body, contentType, responseCode);
    }


}
