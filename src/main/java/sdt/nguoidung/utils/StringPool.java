package sdt.nguoidung.utils;

public class StringPool extends com.liferay.portal.kernel.util.StringPool {
	public static final String CHECKED = "checked";
	public static final String SELECTED = "selected";
	public static final String DISABLED = "disabled";
	public static final String JSP_PAGE = "jspPage";
	public static final String NUMBER0 = "0";
	public static final String NUMBER1 = "1";
	public static final String NUMBER2 = "2";

	public static final int DA_XOA = 1;

	public static final String PASSWORD_DEFAULT = "sdt@123";
}
