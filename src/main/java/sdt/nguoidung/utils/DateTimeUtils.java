package sdt.nguoidung.utils;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUtils {

    public static final String HMF = "HH:mm";
    public static final String DF = "dd/MM/yyyy";
    private static SimpleDateFormat _hmFormater = new SimpleDateFormat(HMF);
    private static SimpleDateFormat _formater = new SimpleDateFormat(DF);
    public static final String HM_SEP = StringPool.COLON;

    public static String getHourMinute(Date date) {
        return (date == null) ? StringPool.BLANK : _hmFormater.format(date);
    }

    public static String getDayMonthYear(Date date) {
        return (date == null) ? StringPool.BLANK : _formater.format(date);
    }

    public static Date parseDate(String ddMMyyyy) {
        Date date;
        try {
            date = _formater.parse(ddMMyyyy);
        } catch (ParseException e) {
            return null;
        }
        if (!_formater.format(date).equals(ddMMyyyy)) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static int[] parseHourMitute(String HHmm) {
        if (HHmm == null) {
            return null;
        }
        String[] tmp = HHmm.trim().split(HM_SEP);
        if (tmp.length != 2) {
            return null;
        }
        try {
            int h = Integer.parseInt(tmp[0]);
            int m = Integer.parseInt(tmp[1]);
            if (h < 0 || h > 23) {
                return null;
            }
            if (m < 0 || h > 59) {
                return null;
            }
            return new int[] {h, m};
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static Date parseDate(String ddMMyyyy, String HHmm) {
        Date date = parseDate(ddMMyyyy.trim());
        if (date == null) {
            return null;
        }
        int[] hm = parseHourMitute(HHmm);
        if (hm == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hm[0]);
        cal.set(Calendar.MINUTE, hm[1]);
        return cal.getTime();
    }

    public static Date getRequestDate(HttpServletRequest request, String ddMMyyyyParam, String HHmmParam) {
        return ddMMyyyyParam == null || HHmmParam == null
                ? null : parseDate( ParamUtil.getString(request, ddMMyyyyParam), ParamUtil.getString(request, HHmmParam)
        );
    }

    public static Date getRequestDate(PortletRequest request, String ddMMyyyyParam, String HHmmParam) {
        return ddMMyyyyParam == null || HHmmParam == null
                ? null
                : parseDate(
                ParamUtil.getString(request, ddMMyyyyParam),
                ParamUtil.getString(request, HHmmParam)
        );
    }

    public static String  convertPatternDate (String date) throws Exception {
        Date result = null;
        String resultDate = "";
        if (Validator.isNotNull(date)) {

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            result = df.parse(date);

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            resultDate = sdf.format(result);

        }
        return resultDate;
    }

    public static String  convertPatternDates (String date) throws Exception {
        Date result = null;
        String resultDate = "";
        if (Validator.isNotNull(date)) {

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            result = df.parse(date);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            resultDate = sdf.format(result);

        }
        return resultDate;
    }
}
