package sdt.nguoidung.utils;

import com.liferay.portal.kernel.util.Validator;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import sdt.nguoidung.congchuc.dto.UserDTO;

import javax.net.ssl.*;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;

public class Utils {
    public static  final String SLASH = "/";
    public static HttpEntity<?> getHeaderToken(String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization","Bearer " + accessToken);

        HttpEntity<?> entity = new HttpEntity(headers);

        return entity;
    }


    public static HttpEntity<String> getHeaderTokenBody(String accessToken, String body) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization","Bearer " + accessToken);

        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        HttpEntity<String> entity = new HttpEntity(body, headers);

        return entity;
    }

    public static String getListDataFromResponseService(String json) throws IOException {
        JSONObject object = new JSONObject(json);
        try {
            object = object.getJSONObject("objects");
        } catch (Exception e) {
            return null;
        }

        String objectData = null;
        try {
            objectData = object.getJSONArray("object").toString();
        } catch (Exception e) {
            objectData = object.getJSONObject("object").toString();
        }

        return objectData;
    }

    public static void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }
    public static String convertListToString(List<Long> longs) {

        if (longs != null && longs.size() > 0) {
            return StringUtils.join(longs, ",");
        }

        return "";
    }

    public static HashMap<String, String> convertListGroupToMap(String listGroupInSiteStr) {
        HashMap<String, String> hashMap = new HashMap<>();

        if (Validator.isNotNull(listGroupInSiteStr)) {

            JSONArray jsonArray = new JSONArray(listGroupInSiteStr);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjectChild = jsonArray.getJSONObject(i);

                if (Validator.isNotNull(jsonObjectChild)) {
                    hashMap.put(String.valueOf(jsonObjectChild.getLong("groupId")),
                            jsonObjectChild.getString("groupName"));
                }
            }
        }

        return hashMap;
    }

    public static String md5(String input) throws Exception {

        String md5;

        if (null == input)
            return null;

        // Create MessageDigest object for MD5
        MessageDigest digest = MessageDigest.getInstance("MD5");

        // Update input string in message digest
        digest.update(input.getBytes(), 0, input.length());

        // Converts message digest value in base 16 (hex)
        return new BigInteger(1, digest.digest()).toString(16);
    }

    public static UserDTO updateUserLiferay(String tenCanBo, UserDTO userDTO) {
        String firstName = "";
        String middleName = "";
        String lastName = "";
        if (tenCanBo.indexOf(" ") > 0) {
            firstName = tenCanBo.substring(0, tenCanBo.indexOf(" "));
        }
        if (tenCanBo.lastIndexOf(" ") > 0) {
            lastName = tenCanBo.substring(tenCanBo.lastIndexOf(" "), tenCanBo.length());
        }
        if (tenCanBo.indexOf(" ") > 0 && tenCanBo.lastIndexOf(" ") > 0) {
            middleName = tenCanBo.substring(tenCanBo.indexOf(" "), tenCanBo.lastIndexOf(" "));
        }
        if (firstName.length() == 0) {
            firstName = userDTO.getFirstName();
        }
        if (middleName.length() == 0) {
            middleName = "-";
        }
        if (lastName.length() == 0) {
            lastName = userDTO.getLastName();
        }
        userDTO.setFirstName(firstName);
        userDTO.setMiddleName(middleName);
        userDTO.setLastName(lastName);

        return userDTO;
    }

    public static HashMap<Integer, String> getHinhThucs () {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Không phân loại");
        hashMap.put(2, "Họp trực tiếp");
        hashMap.put(3, "Họp trực tuyến");

        return hashMap;
    }

    public static HashMap<Integer, String> getLoaiCuocHops () {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Không phân loại");
        hashMap.put(2, "Tiếp và làm việc với Chính phủ, Bộ, Ngành Trung ương (bao gồm cuộc họp, hội nghị trực tuyến tại UBND Tỉnh )");
        hashMap.put(3, "Tiếp và làm việc với khách nước ngoài tại UBND Tỉnh");
        hashMap.put(4, "Chủ trì họp với Sở, ban ngành Tỉnh, UBND cấp Huyện");
        hashMap.put(5, "Chủ trì làm việc với Sở, ban ngành Tỉnh, UBND cấp Huyện");
        hashMap.put(6, "Dự cuộc họp, hội nghị do Tỉnh ủy, HĐND Tỉnh hoặc sở, ban ngành, tỉnh, UBND cấp Huyện mời");
        hashMap.put(7, "Tiếp công dân dịnh kỳ, đối thoại với công dân");
        hashMap.put(8, "Tham dự các lễ ký kết, công bố, trao đổi quyết định các bộ.. tại UBND Tỉnh");
        hashMap.put(9, "Đi cơ sở, khảo sát thực địa");
        hashMap.put(10, "Đi công tác");
        hashMap.put(11, "Khác");

        return hashMap;
    }
}
