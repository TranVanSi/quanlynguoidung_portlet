<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
</head>
<portlet:actionURL var="themMoiURL">
    <portlet:param name="action" value="themMoi" />
</portlet:actionURL>
<portlet:renderURL var="homeUrl">
    <portlet:param name="coQuanId" value="${coQuanId}" />
    <portlet:param name="tenCoQuan" value="${tenCoQuan}" />
    <portlet:param name="action" value="xemChucVu" />
</portlet:renderURL>
<liferay-portlet:resourceURL var="addCoQuan" id="addCoQuan" ></liferay-portlet:resourceURL>
<portlet:defineObjects />

<portlet:actionURL var="ganChucVuURL">
    <portlet:param name="coQuanId" value="${coQuanId}"/>
    <portlet:param name="action" value="ganChucVu" />
</portlet:actionURL>
<liferay-ui:error key="form-error"
                  message="Yêu cầu của bạn thực hiện không thành công !." />
<input type="hidden" name="addCoQuan" value="${addCoQuan}" id="addCoQuan" />
<input type="hidden" name="coQuanId" value="${coQuanId}"/>
<div class="page-title">
    <h3 class="tieuDe text-center">${tenCoQuan}</h3>
</div>

<div class="container-fluid">
    <form action="${ganChucVuURL}" method="post" name="submitForm" id="formThemMoi" class="needs-validation " novalidate="true">

        <input type="hidden" name="id" value="${id}" />
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <c:if test="${isEdit eq false}">
                        <label>Chức vụ</label><br>
                        <select name="chucVuId" id="chucVuId"  class="form-control" required="true">
                            <c:forEach items="${chucVus }" var="chucVu">
                                <option value="${chucVu.id }" <c:if test="${chucVuId == chucVu.id}">selected</c:if> >${chucVu.ten }</option>
                            </c:forEach>
                        </select>
                    </c:if>
                    <c:if test="${isEdit eq true}">
                        <input type="hidden" name="chucVuId" value="${chucVuDTO.id}"/>
                        <label>Chức vụ: </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span>${chucVuDTO.ten}</span>
                    </c:if>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <label>Vai trò hệ thống</label><br>
                    <select name="vaiTroHeThong" id="vaiTroHeThong"  multiple="multiple" size="10"  class="form-control">
                        <c:forEach items="${danhSachVaiTroHeThong }" var="vaiTroHeThong">
                            <option value="${vaiTroHeThong.id }">${vaiTroHeThong.ten }</option>
                        </c:forEach>
                    </select>
                    <input type="text" value="" onkeyup="searchOptions(this.value);" class="form-control"/>
                </div>
                <div class="col-md-2 col-sm-2" style="text-align: center;margin-top: 5% !important">
                    <button type="button" class="btn btn-primary" id="right_to_left" style="margin-bottom: .3em !important;">>></button><br>
                    <button type="button" class="btn btn-primary" id="left_to_right"><<</button>
                </div>
                <div class="col-md-5 col-sm-5">
                    <label>Vai trò được chọn</label><br>
                    <select name="vaiTroHeThongDuocChon" id="vaiTroHeThongDuocChon" multiple="multiple"  size="10" class="form-control" required="true">
                        <c:forEach items="${danhSachVaiTroHeThongDuocChon }" var="vaiTroHeThongDuocChon" >
                            <option value="${vaiTroHeThongDuocChon.id }">${vaiTroHeThongDuocChon.ten }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>


    </form>
    <div class="form-group text-center">
        <button type="button" class="btn btn-sm btn-primary login-submit-cs text-center" onclick="dongPopup();"><spring:message code="vn.sdt.label.thoat" /></button>
    </div>
</div>
<script>
    function dongPopup() {
        window.opener.location.reload();
        window.close();
    }
</script>
<script src="${pageContext.request.contextPath}/js/themmoicoquan.js"></script>