<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="../init.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<portlet:renderURL var="timKiemURL">
    <portlet:param name="action" value="" />
</portlet:renderURL>
<portlet:renderURL var="ganCoQuanQuanLyURL">
    <portlet:param name="action" value="ganCoQuanQuanLy" />
</portlet:renderURL>
<portlet:renderURL var="ganChucVuURL" windowState="pop_up">
    <portlet:param name="coQuanId" value="${coQuanId}"/>
    <portlet:param name="tenCoQuan" value="${tenCoQuan}"/>
    <portlet:param name="action" value="ganChucVu" />
</portlet:renderURL>
<portlet:actionURL var="xoaURL">
    <portlet:param name="action" value="xoa" />
</portlet:actionURL>

<liferay-portlet:resourceURL var="loadCoQuanQuanLyDataTableURL" id="loadCoQuanQuanLyDataTable" >
    <liferay-portlet:param name="coQuanId" value="${coQuanId}"/>
</liferay-portlet:resourceURL>
<!-- Data table area Start-->
<div class="admin-dashone-data-table-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline8-list shadow-reset">
                    <div class="sparkline8-hd">
                        <div class="main-sparkline8-hd text-center">
                            <h1><spring:message code="vn.sdt.label.tieude.danhsachchucvucoquan"/>: </h1><br>
                            <h1>${tenCoQuan}</h1>
                            <div class="sparkline8-outline-icon" style="margin-bottom: 1%; float: right">
                                <button class="btn btn-sm btn-primary login-submit-cs" onclick="openWindow('${ganChucVuURL}', 800, 400)"><spring:message code="vn.sdt.button.themmoi" /></button>
                            </div>
                        </div>
                    </div>

                    <div class="sparkline8-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <table id="table" class="table-fit" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                <thead>
                                <tr>
                                    <th data-field="stt" class="text-center"><spring:message code="vn.sdt.label.stt"/></th>
                                    <th data-field="chucvu" class="text-center"><spring:message code="vn.sdt.label.tenchucvu"/></th>
                                    <th data-field="vaitro" class="text-center"><spring:message code="vn.sdt.label.vaitro"/></th>
                                    <th data-field="action" class="text-center"><spring:message code="vn.sdt.label.thaotac"/></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="item" items="${listView }" varStatus="num">
                                    <tr>
                                        <td class="align-middle text-center">${num.index +1}</td>
                                        <td class="align-middle text-left">
                                                ${item.key.ten}
                                        </td>
                                        <td class="align-middle text-left">
                                        <c:forEach var="obj" items="${item.value}" >
                                            ${obj.vaiTro.ten}<br>
                                        </c:forEach>
                                        </td>
                                        <td class="align-middle text-center">
                                            <a href="#" onclick="openWindow('${ganChucVuURL}&coQuanId=${item.value.get(0).coQuanQuanLyId}&chucVuId=${item.key.id}', 800, 400)"><i class="fa fa-edit" title="<spring:message code="vn.sdt.edit"/>"></i></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function openWindow(url, w, h) {
        const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
        const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

        const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        const systemZoom = width / window.screen.availWidth;
        const left = (width - w) / 2 / systemZoom + dualScreenLeft;
        const top = (height - h) / 2 / systemZoom + dualScreenTop;

        let strWindowFeatures = "location=yes,scrollbars=yes,status=yes,width=${w / systemZoom}, height=${h / systemZoom},top=${top}, left=${left}";
        let win = window.open(url, "_blank", strWindowFeatures);
        if (window.focus) win.focus();
    }
</script>