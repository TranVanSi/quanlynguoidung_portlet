<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>

<portlet:renderURL var="homeUrl">
    <portlet:param name="action" value=""/>
</portlet:renderURL>

<div class="project-details-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="project-details-wrap shadow-reset">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="project-details-title text-center">
                                <h2 class="no-margin tieude"><spring:message code="vn.sdt.chitiet"/> <spring:message
                                        code="vn.sdt.ten.coquanquanly"/></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="project-details-st">
                                            <span><strong><spring:message code="vn.sdt.label.coquanquanly.ma"/>:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div>
                                            <span>${item.ma}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="project-details-st">
                                            <span><strong><spring:message code="vn.sdt.lable.coquanquanly.ten"/>:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div>
                                            <span>${item.ten}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="project-details-st">
                                            <span><strong><spring:message code="vn.sdt.label.coquanquanly.capcha"/>:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div>
                                            <c:forEach items="${listCoQuanQuanLy}" var="itemCQQL" varStatus="num">
                                                <c:if test="${itemCQQL.id eq item.chaId}"><span>${itemCQQL.ten}</span></c:if>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="project-details-st">
                                            <span><strong><spring:message code="vn.sdt.label.coquanquanly.capcoquanquanly"/>:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div>
                                            <span>${item.capCoQuanQuanLy.ten}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="project-details-mg">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="project-details-st">
                                            <span><strong><spring:message code="vn.sdt.label.coquanquanly.diachi"/>:</strong></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-10">
                                        <div>
                                            <span>${item.diaChi}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group-inner">
                        <div class="login-btn-inner">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <div class="login-horizental cancel-wp pull-center">
                                        <button class="btn btn-sm btn-primary login-submit-cs" type="button"
                                                onclick="location.href='${homeUrl}'">
                                            <spring:message code="vn.sdt.button.quaylai"/></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

