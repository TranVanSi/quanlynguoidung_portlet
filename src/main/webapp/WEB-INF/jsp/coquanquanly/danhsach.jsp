<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="../init.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<portlet:renderURL var="themMoiURL">
    <portlet:param name="action" value="themMoi" />
</portlet:renderURL>
<portlet:renderURL var="taoCapConURL">
    <portlet:param name="action" value="taoCapCon"></portlet:param>
</portlet:renderURL>
<portlet:actionURL var="xoaURL">
    <portlet:param name="action" value="xoa" />
</portlet:actionURL>
<portlet:actionURL var="timKiemURL">
    <portlet:param name="action" value="timKiem" />
</portlet:actionURL>
<portlet:renderURL var="chiTietURL">
    <portlet:param name="action" value="chiTiet"></portlet:param>
</portlet:renderURL>
<portlet:renderURL var="xemChucVuURL">
    <portlet:param name="action" value="xemChucVu" />
</portlet:renderURL>
<portlet:renderURL var="khoiPhucCQQLURL">
    <portlet:param name="action" value="khoiPhucCQQL"></portlet:param>
</portlet:renderURL>

<liferay-portlet:resourceURL var="loadCongChucByCoQuanQuanLyId" id="loadCongChucByCoQuanQuanLyId">
</liferay-portlet:resourceURL>

<liferay-ui:success key="form-success"
                    message="Yêu cầu của bạn đã được thực hiện thành công!." />
<div class="admin-dashone-data-table-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline8-list shadow-reset">
                    <div class="sparkline8-hd">
                        <div class="main-sparkline8-hd text-center">
                            <h1 class="tieude"><spring:message code="vn.sdt.tieude.coquanquanly"/></h1>
                            <c:if test="${not empty tieuDe}">
                                <h1 class="tieude" style="text-transform:uppercase">thuộc ${tieuDe}</h1>
                            </c:if>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <button style="float: right; margin-bottom: 1%" class="btn btn-primary" type="button" title="Thêm mới" onclick="location.href='${themMoiURL}'">Thêm mới</button>
                            <table id="table" class="table-fit" data-toggle="table"
                                   data-pagination="true"
                                   data-search="true"
                                   data-show-columns="false"
                                   data-show-pagination-switch="false"
                                   data-show-refresh="false"
                                   data-key-events="false"
                                   data-show-toggle="true"
                                   data-cookie="false"
                                   data-cookie-id-table="saveId"
                                   data-show-export="false"
                                   data-click-to-select="false"
                                   data-mobile-responsive="true"
                                   data-toolbar="#toolbar">
                                <thead>
                                <tr>
                                    <th data-field="stt" class="text-center width5"><spring:message code="vn.sdt.label.stt"/></th>
                                    <th data-field="ma" class="text-center width10"><spring:message code="vn.sdt.label.ma"/></th>
                                    <th data-field="ten" class="text-center width20"><spring:message code="vn.sdt.label.ten"/></th>
                                    <th data-field="cap" class="text-center width15"><spring:message code="vn.sdt.label.cap"/></th>
                                    <th data-field="diachi" class="text-center width25"><spring:message code="vn.sdt.label.coquanquanly.diachi"/></th>
                                    <th data-field="action" class="text-center width20"><spring:message code="vn.sdt.label.thaotac"/></th>

                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="item" items="${listView }" varStatus="num">
                                    <tr>
                                        <td class="align-middle text-center">${num.index +1}</td>
                                        <td class="align-middle text-center">${item.ma}</td>
                                        <td class="align-middle text-left">${item.ten}</td>
                                        <td class="align-middle text-left">
                                                ${item.capCoQuanQuanLy.ten}
                                        </td>
                                        <td class="align-middle text-left">${item.diaChi}</td>
                                        <td class="align-middle text-center">
                                            <a class="btn-feature-info" href="#" onclick="location.href='${timKiemURL}&parentId=${item.id}'"><i class="fa fa-folder-open" aria-hidden="true" title="Xem cấp con"></i></a>
                                            <a class="btn-feature-info" href="#" onclick="location.href='${taoCapConURL}&parentId=${item.id}&cqqlChaId=${cqqlChaId }'"><i class="fa fa-plus-square-o" title="<spring:message code="vn.sdt.taocapcon"/>"> </i></a>
                                            <a class="btn-feature-info" href="#" onclick="location.href='${xemChucVuURL}&coQuanQuanLyId=${item.id}&tenCoQuan=${item.ten}'"><i class="fa fa-list" aria-hidden="true" title="Xem chức vụ"></i></a>
                                            <a class="btn-feature-info" href="#" onclick="location.href='${jspPage}&coQuanQuanLyId=${item.id}'"><i class="fa fa-user" aria-hidden="true" title="Công chức"></i></a>
                                            <a class="btn-feature-info" href="#" onclick="location.href='${themMoiURL}&coQuanQuanLyId=${item.id}&test=1'"><i class="fa fa-edit color-0281B8" title="<spring:message code="vn.sdt.edit"/>"></i></a>
                                            <a class="btn-feature-danger" onclick="xoa('${item.id}');"  ><i class="fa fa-trash color-FF0000" title="<spring:message code="vn.sdt.remove"/>"></i></a>

                                        </td>

                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('#table').bootstrapTable({
            exportDataType: $(this).val(),
            exportOptions:{
                ignoreColumn: [0,6]
            }
        });

    });

    function xoa(coQuanId) {
        if (confirm('Bạn có thật sự muốn xóa??')) {
            $.ajax({
                type: 'GET'
                ,url: '${loadCongChucByCoQuanQuanLyId}'+'&coQuanQuanLyId='+coQuanId
                ,success: function(data) {
                if (data == true || data == 'true') {
                    alert('Không được xóa vì cơ quan quản lý đang được sử dụng!');

                } else {
                    window.location.href="${xoaURL}&coQuanQuanLyId="+coQuanId;
                }

                },
            });
        }
    };
</script>
