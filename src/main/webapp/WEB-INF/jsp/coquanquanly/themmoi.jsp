<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
		.red{
			color: red;
		}
		.center{
			text-align: center;
		}
		.mgtb{
			margin-top: 5px;
			margin-bottom: 5px;
		}
		.mg45{
			margin-left: 45%;
		}
	</style>
</head>
<portlet:actionURL var="themMoiURL">
	<portlet:param name="action" value="themMoi" />
</portlet:actionURL>
<portlet:renderURL var="homeUrl">
	<portlet:param name="action" value="" />
</portlet:renderURL>

<portlet:defineObjects />
<c:if test="${empty coQuanQuanLy.id or coQuanQuanLy.id == 0}">
	<c:set var="tieuDe">
		<spring:message code="vn.sdt.tieude.themmoi.coquanquanly"/>
	</c:set>
</c:if>
<c:if test="${coQuanQuanLy.id > 0}">
	<c:set var="tieuDe">
		<spring:message code="vn.sdt.tieude.capnhat.coquanquanly"/>
	</c:set>

</c:if>
<liferay-ui:error key="alert-error"
				  message="Yêu cầu của bạn thực hiện không thành công!." />

<div class="basic-form-area mg-b-15">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="sparkline12-list shadow-reset">
					<div class="sparkline12-hd">
						<div class="main-sparkline12-hd text-center">
							<h1 class="tieude">${tieuDe}</h1>
						</div>
					</div>
					<div class="sparkline12-graph">
						<div class="basic-login-form-ad">
							<div class="row">
								<div class="col-lg-12">
									<div class="all-form-element-inner">
										<form:form action="${themMoiURL}" method="post" name="submitForm" class="needs-validation " novalidate="true"
												   modelAttribute="coQuanQuanLy">

											<div class="form-group-inner" style="display: none;" >
												<div class="row">
													<div class="col-lg-3">
														<select style="display: none;" id="egov-form-errors">

															<option value="ma">
																<liferay-ui:error key="coquanquanly.validate.ma.trung">
																	<spring:message code="vn.sdt.validate.trung.ma" />
																</liferay-ui:error>
															</option>
                                                            <option value="ma">
                                                                <liferay-ui:error key="validate.ma.null">
                                                                    <spring:message code="vn.sdt.validate.null.ma" />
                                                                </liferay-ui:error>
                                                            </option>
															<option value="ten">
																<liferay-ui:error key="coquanquanly.validate.ten.trung">
																	<spring:message code="vn.sdt.validate.trung.ten" />
																</liferay-ui:error>
															</option>
														</select>

														<form:hidden path="id" />
														<form:hidden path="nguoiTao" />
														<form:hidden path="nguoiSua" />
													</div>
												</div>
											</div>

											<div class="form-group-inner">
												<div class="row mgtb">
													<div class="col-lg-3 center">
														<form:label path="ma"><spring:message code="vn.sdt.tainguyen.ma" /><span class="red">*</span>:</form:label>
													</div>
													<div class="col-lg-9">
														<form:input path="ma" id="ma" class="form-control" required="true" maxlength="10"/>
													</div>
												</div>
											</div>

											<div class="form-group-inner">
												<div class="row mgtb">
													<div class="col-lg-3 center">
														<form:label path="ten"><spring:message code="vn.sdt.tainguyen.ten" /><span class="red">*</span>:</form:label>
													</div>
													<div class="col-lg-9">
														<form:input path="ten" id="ten" class="form-control" required="true"/>
													</div>
												</div>
											</div>

											<div class="form-group-inner">
												<div class="row mgtb">
													<div class="col-lg-3 center">
														<form:label path="diaChi"><spring:message code="vn.sdt.coquanquanly.diachi" /><span class="red">*</span>:</form:label>
													</div>
													<div class="col-lg-9">
														<form:input path="diaChi" class="form-control" type="text" required="true" />
													</div>
												</div>
											</div>

											<div class="form-group-inner">
												<div class="row mgtb">
													<div class="col-lg-3 center">
														<form:label path="chaId"><spring:message code="vn.sdt.coquanquanly.capcha" />:</form:label>
													</div>
													<div class="col-lg-9">
														<form:select path="chaId"  class="form-control">
															<form:option value="0"><spring:message code="vn.sdt.coquanquanl.select.default"/></form:option>
															<c:forEach items="${listCoQuanQuanLy}" var="itemCoQuanQuanLy" varStatus="num">
																<option value="${itemCoQuanQuanLy.id}" <c:if test="${itemCoQuanQuanLy.id eq coQuanQuanLy.chaId}">selected="selected"</c:if>>${itemCoQuanQuanLy.ten}</option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>

											<div class="form-group-inner">
												<div class="row mgtb">
													<div class="col-lg-3 center">
														<form:label path="capCoQuanQuanLy.id"><spring:message code="vn.sdt.coquanquanly.capcoquanquanly" /><span class="red">*</span>:</form:label>
													</div>
													<div class="col-lg-9">
														<form:select path="capCoQuanQuanLy.id" required="true" class="form-control">
															<form:option value=""><spring:message code="vn.sdt.coquanquanl.select.default"/></form:option>
															<form:option value="3">Cấp Tỉnh/Thành phố </form:option>
															<%--<c:forEach items="${capCoQuanQuanLys}" var="capCQQL" varStatus="loop">
																<option value="${capCQQL.id}" <c:if test="${capCQQL.id eq coQuanQuanLy.capCoQuanQuanLy.id}">selected="selected"</c:if>>${capCQQL.ten}</option>
															</c:forEach>--%>
														</form:select>
													</div>
												</div>
											</div>

											<div class="row form-group">
												<div class="col-md-12 col-sm-12 col-xs-12 text-center">
													<button class="btn btn-info" type="submit"><spring:message code="vn.sdt.tainguyen.luu" /></button>
													<button class="btn btn-info" type="button" onclick="location.href='${homeUrl}'"><spring:message code="vn.sdt.tainguyen.quaylai" /></button>
												</div>

											</div>

										</form:form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
