<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/custom-style.css">
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/datatable/bootstrap-table.js"></script>
<script src="${pageContext.request.contextPath}/js/datatable/bootstrap-editable.js"></script>
<script src="${pageContext.request.contextPath}/js/datatable/bootstrap-table-cookie.js"></script>
<script src="${pageContext.request.contextPath}/js/datatable/bootstrap-table-editable.js"></script>
<script src="${pageContext.request.contextPath}/js/datatable/bootstrap-table-export.js"></script>
<script src="${pageContext.request.contextPath}/js/datatable/bootstrap-table-key-events.js"></script>
<script src="${pageContext.request.contextPath}/js/datatable/bootstrap-table-resizable.js"></script>
<script src="${pageContext.request.contextPath}/js/datatable/colResizable-1.5.source.js"></script>
<script src="${pageContext.request.contextPath}/js/datatable/data-table-active.js"></script>
<liferay-theme:defineObjects />

<portlet:defineObjects />