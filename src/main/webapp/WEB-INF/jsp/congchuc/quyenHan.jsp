<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="../init.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<!-- Data table area Start-->
<div class="admin-dashone-data-table-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline8-list shadow-reset">
                    <div class="sparkline8-hd">
                        <div class="main-sparkline8-hd text-left">
                            <h1 class="tieude">Quyền hạn của cán bộ: ${tenCanBo} - ${tenChucVu}</h1>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="datatable-dashv1-list custom-datatable-overright">

                            <table class="table fixed-table-container" >
                                <thead>
                                    <tr>

                                        <th data-field="stt" class="text-center"><spring:message code="vn.sdt.label.stt"/></th>
                                        <th data-field="ma" class="text-center">Tên vai trò</th>
                                        <th data-field="ten" class="text-center">Tài nguyên sử dụng</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="item" items="${vaiTroDTOS }" varStatus="num">
                                        <tr>
                                            <td class="align-middle text-center">${num.index +1}</td>
                                            <td class="align-middle text-left">${item.ten}</td>
                                            <td class="align-middle text-left">
                                                <c:forEach var="taiNguyen" items="${item.taiNguyens }">
                                                    <p>${taiNguyen.ten}</p>
                                                </c:forEach>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(function() {
        setTimeout(function () {
            initTable();
        }, 1000)
    });
    function indexFormatter(value, row, index, field) {
        return ($('#table').bootstrapTable('getOptions').pageNumber - 1) * $('#table').bootstrapTable('getOptions').pageSize + index + 1;
    }

    function operateFormatter(value, row, index) {
        return [
            '<a class="btn-feature-info edit" href="#" ><i class="fa fa-edit color-0281B8" title="<spring:message code="vn.sdt.edit"/>"></i></a>',
            '<a class="btn-feature-info role" href="#" ><i class="fa fa-book fa-fw color-0281B8" title="Quyền hạn"></i></a>',
            '<a class="btn-feature-danger remove" ><i class="fa fa-trash color-FF0000" title="<spring:message code="vn.sdt.remove"/>"></i></a>'
        ].join('')
    }

    function detailFormatterMa(index, row) {
        return '<a class="detail" href="#">'+row.ma+'</a>'
    }

    function detailFormatterTen(index, row) {
        return '<a class="detail" href="#">'+row.hoVaTen+'</a>'
    }

    function detailFormatterEmail(index, row) {
        if (typeof  row.email != 'undefined' && row.email != null && row.email.length > 0) {
            return '<a class="detail" href="#">'+row.email+'</a>'
        } else {
            return '';
        }
    }

    function detailFormatterCoQuan(index, row) {
        return '<a class="detail" href="#">'+row.coQuanQuanLy.ten+'</a>'
    }

    function detailFormatterChucVu(index, row) {
        return '<a class="detail" href="#">'+row.chucVu.ten+'</a>'
    }

    window.operateEvents = {
        'click .edit': function (e, value, row, index) {
            location.href='${themMoiURL}&congChucId='+value;
        },
        'click .remove': function (e, value, row, index) {
            if (confirm('Bạn có thật sự muốn xóa?')) {
                location.href='${xoaURL}&congChucId='+value;
            }
        },
        'click .detail': function (e, value, row, index) {
            location.href='${chiTietURL}&congChucId='+row.id;
        },

        'click .role': function (e, value, row, index) {
            location.href='${quyenHanURL}&congChucId='+row.id;
        }
    }

    function initTable() {

        $('#table').bootstrapTable('destroy').bootstrapTable({
            columns: [
                [{
                    field: 'state',
                    checkbox: true,
                    align: 'center'
                }, {
                    title: '<spring:message code="vn.sdt.label.stt"/>',
                    field: 'stt',
                    align: 'center',
                    valign: 'middle',
                    formatter: indexFormatter
                }, {
                    title: '<spring:message code="vn.sdt.label.ma"/>',
                    field: 'ma',
                    align: 'center',
                    valign: 'middle',
                    events: window.operateEvents,
                    formatter: detailFormatterMa,
                }, {
                    title: '<spring:message code="vn.sdt.label.hovaten"/>',
                    field: 'hoVaTen',
                    align: 'left',
                    valign: 'middle',
                    events: window.operateEvents,
                    formatter: detailFormatterTen,
                }, {
                    title: '<spring:message code="vn.sdt.label.email"/>',
                    field: 'email',
                    align: 'left',
                    valign: 'middle',
                    events: window.operateEvents,
                    formatter: detailFormatterEmail,
                }, {
                    title: '<spring:message code="vn.sdt.label.coquanquanly"/>',
                    field: 'coQuanQuanLy.ten',
                    align: 'left',
                    valign: 'middle',
                    events: window.operateEvents,
                    formatter: detailFormatterCoQuan,
                }, {
                    title: '<spring:message code="vn.sdt.label.chucvu"/>',
                    field: 'chucVu.ten',
                    align: 'center',
                    valign: 'middle',
                    events: window.operateEvents,
                    formatter: detailFormatterChucVu,
                }, {
                    field: 'id',
                    title: '<spring:message code="vn.sdt.label.thaotac"/>',
                    align: 'center',
                    clickToSelect: false,
                    events: window.operateEvents,
                    formatter: operateFormatter
                }]
            ]
        });
    }
</script>