<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../init.jsp"%>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<portlet:actionURL var="themMoiURL">
    <portlet:param name="action" value="themMoi" />
</portlet:actionURL>
<portlet:renderURL var="homeUrl">
    <portlet:param name="action" value="" />
</portlet:renderURL>
<liferay-portlet:resourceURL var="getChucVuByCQQL" id="getChucVuByCQQL">

</liferay-portlet:resourceURL>
<portlet:defineObjects />
<c:if test="${empty congChuc.id or congChuc.id == 0}">
    <c:set var="tieuDe">
        <spring:message code="vn.sdt.tieude.themmoi.congchuc"/>
    </c:set>
</c:if>
<c:if test="${congChuc.id > 0}">
    <c:set var="tieuDe">
        <spring:message code="vn.sdt.tieude.capnhat.congchuc"/>
    </c:set>

</c:if>
<liferay-ui:error key="alert-error"
                  message="Yêu cầu của bạn thực hiện không thành công!." />

<div class="basic-form-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline12-list shadow-reset">
                    <div class="sparkline12-hd">
                        <div class="main-sparkline12-hd text-center">
                            <h1 class="tieude">THÊM MỚI NGƯỜI DÙNG</h1>
                        </div>
                    </div>
                    <!-- end title -->

                    <!-- content -->
                    <div class="sparkline12-graph">
                        <div class="basic-login-form-ad">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="all-form-element-inner">
                                        <form:form action="${themMoiURL}" method="post" name="submitForm" class="needs-validation " novalidate="true"
                                            modelAttribute="congChuc">
                                            <div class="form-group-inner" style="display: none;" >
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <select id="egov-form-errors">
                                                            <option value="ma">
                                                                <liferay-ui:error key="congchuc.validate.ma.trung">
                                                                    <spring:message code="vn.sdt.validate.trung.ma" />
                                                                </liferay-ui:error>
                                                            </option>
                                                            <option value="email">
                                                                <liferay-ui:error key="congchuc.validate.email.trung">
                                                                    <spring:message code="validate.email.trung" />
                                                                </liferay-ui:error>
                                                            </option>
                                                        </select>

                                                        <form:hidden path="id" />
                                                        <form:hidden path="taiKhoanNguoiDung.id" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="ma"><spring:message code="vn.sdt.tainguyen.ma" /><span class="red">*</span>:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:input path="ma" id="ma" class="form-control" required="true" maxlength="10"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="hoVaTen"><spring:message code="vn.sdt.tainguyen.ten" /><span class="red">*</span>:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:input path="hoVaTen"  class="form-control" required="true"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="ngaySinh"><spring:message code="vn.sdt.label.ngaysinh" />:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:input  placeholder="dd/MM/yyyy" autocomplete="off" path="ngaySinh" type="text" value="${ngaySinhDate}" class="form-control dgov-calendar" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="gioiTinhId"><spring:message code="vn.sdt.congchuc.gioitinh" />:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:select path="gioiTinhId" class="form-control">
                                                            <form:option value="">--Chọn giới tính--</form:option>
                                                            <form:option value="1"><spring:message code="vn.sdt.congchuc.gioitinh.nam" /></form:option>
                                                            <form:option value="2"><spring:message code="vn.sdt.congchuc.gioitinh.nu" /></form:option>
                                                            <form:option value="3"><spring:message code="vn.sdt.congchuc.gioitinh.khongxacdinh" /></form:option>
                                                        </form:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="email"><spring:message code="vn.sdt.label.email" /><span class="red">*</span>:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <c:if test="${(congChuc.email != null)}">
                                                            <c:set var="readonly" value="readonly" scope="session"/>
                                                        </c:if>
                                                        <c:if test="${(empty congChuc.email or congChuc.email.length() == 0) or validateFail.length() > 0}">
                                                            <c:set var="readonly" value="" scope="session"/>
                                                        </c:if>
                                                        <input path="email" type="email" name="email" id="email" class="form-control valid-email invalid" required="true" ${readonly} value="${email}"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner" style="display: none">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="coQuanQuanLy.id" class="login2 pull-right pull-right-pro"><spring:message code="vn.sdt.label.coquanquanly" /><span class="red">*</span>:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:select path="coQuanQuanLy.id"  class="form-control cqql" required="true" onchange="reDrawSelectBox();">
                                                            <form:option value="${coQuanQuanLys.id }">${coQuanQuanLys.ten }</form:option>
                                                        </form:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="chucVu.id"><spring:message code="vn.sdt.label.chucvu" /><span class="red">*</span>:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:select path="chucVu.id" id="chucVuId" class="form-control chucvu" required="true">

                                                        </form:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="soCMND"><spring:message code="vn.sdt.label.socmnd" />:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:input type="number" onKeyPress="if(this.value.length==9 || this.value.length==12) return false;" path="soCMND" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="ngayCapCMND"><spring:message code="vn.sdt.label.ngaycapcmnd" />:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:input placeholder="dd/MM/yyyy" autocomplete="off" path="ngayCapCMND" type="text" value="${ngayCapDate}" class="form-control dgov-calendar" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="noiCapId"><spring:message code="vn.sdt.label.noicapcmnd" />:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:select path="noiCapId"  class="form-control">
                                                            <form:option value=""><spring:message code="vn.sdt.coquanquanl.select.default" /></form:option>
                                                            <c:forEach items="${noiCaps }" var="itemCQQL">
                                                                <form:option value="${itemCQQL.id }">Công an ${itemCQQL.ten }</form:option>
                                                            </c:forEach>
                                                        </form:select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                                    <button class="btn btn-info" type="submit"><spring:message code="vn.sdt.tainguyen.luu" /></button>
                                                    <button class="btn btn-info" type="button" onclick="location.href='${homeUrl}'"><spring:message code="vn.sdt.tainguyen.quaylai" /></button>
                                                </div>

                                            </div>

                                        </form:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
       reDrawSelectBox();
    });

    var url = '${getChucVuByCQQL}';

    function reDrawSelectBox() {

        var coQuanId =  $(".cqql").val();
        $('#chucVuId').empty();
        $('#chucVuId').append("<option value=''> --Chọn chức vụ-- </option>");

        if (coQuanId == "" || typeof coQuanId === 'undefined' || coQuanId == 0|| coQuanId == 'NaN' || typeof coQuanId == 'NaN'){
            return;
        }
        var chucVuId = '${congChuc.chucVu.id}';
        $.ajax({
            type: 'POST'
            ,url: url+'&coQuanId='+parseInt(coQuanId)
            ,success: function(data) {
                if (typeof  data !== 'undefined' && data != null && data != "") {
                    for (var i = 0; i < data.length; i++) {
                        let chucVu2CoQuan2VaiTro = data[i];
                        var selected = "";
                        if(chucVu2CoQuan2VaiTro.chucVu.id == chucVuId){
                            selected = "selected";
                        }
                        $('#chucVuId').append("<option value='"+chucVu2CoQuan2VaiTro.chucVu.id+"' "+selected+">"+chucVu2CoQuan2VaiTro.chucVu.ten+"</option>");
                    };
                }

            },
        });
    }

</script>
