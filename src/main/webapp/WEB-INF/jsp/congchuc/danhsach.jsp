<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="../init.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .pull-left{
            float: right !important;
        }
    </style>
</head>
<portlet:renderURL var="themMoiURL">
    <portlet:param name="action" value="themMoi" />
    <portlet:param name="coQuanQuanLyId" value="${coQuanQuanLyId}"/>
</portlet:renderURL>
<portlet:actionURL var="xoaURL">
    <portlet:param name="action" value="xoa" />
</portlet:actionURL>
<portlet:renderURL var="chiTietURL">
    <portlet:param name="action" value="chiTiet"></portlet:param>
</portlet:renderURL>
<portlet:renderURL var="quyenHanURL" windowState="pop_up">
    <portlet:param name="action" value="quyenHan"></portlet:param>
</portlet:renderURL>
<liferay-portlet:resourceURL var="search" id="search">
    <portlet:param name="coQuanQuanLyId" value="${coQuanQuanLyId}"/>
</liferay-portlet:resourceURL>
<liferay-ui:success key="form-success"
                    message="Yêu cầu của bạn đã được thực hiện thành công!." />
<!-- Data table area Start-->
<div class="admin-dashone-data-table-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline8-list shadow-reset">
                    <div class="sparkline8-hd">
                        <div class="main-sparkline8-hd text-center">
                            <h1 class="tieude"><%--<spring:message code="vn.sdt.tieude.congchuc"/>
                                <br/>
                                <span style="text-transform:uppercase">${tenCoQuanQuanLy}</span>--%>
                                DANH SÁCH NGƯỜI DÙNG
                            </h1>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                                <div class="view-mail-action">
                                    <button class="btn btn-primary" type="button" title="Thêm mới" onclick="location.href='${themMoiURL}'">
                                      Thêm mới
                                    </button>
                                    <button style="display: none" class="btn btn-default" aria-label="export type" title="Xuất dữ liệu"
                                            type="button">
                                        <i class="glyphicon glyphicon-th-list"></i>
                                        <select style="padding-right: 4px;padding-top: 0px;width: 0px;padding-bottom: 0px;" data-content="\f03a" class="btn dropdown-toggle" aria-label="export type" title="Xuất dữ liệu">
                                            <option  value="-1" disabled selected></option >
                                            <option  value="">Xuất mặc định</option >
                                            <option  value="all">Xuất tất cả</option >
                                            <option  value="selected">Xuất được chọn</option >
                                        </select >
                                    </button>
                                </div>
                            </div>
                            <table id="table" data-url="${search}" class="table-fit"
                                   data-toggle="bootstrap-table"
                                   data-side-pagination="server"
                                   data-pagination="true"
                                   data-search="true"
                                   data-show-columns="true"
                                   data-show-pagination-switch="true"
                                   data-show-refresh="true"
                                   data-key-events="true"
                                   data-show-toggle="true"
                                   data-resizable="true"
                                   data-cookie="true"
                                   data-cookie-id-table="saveId"
                                   data-show-export="true"
                                   data-click-to-select="true"
                                   data-mobile-responsive="true"
                                   data-toolbar="#toolbar">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function openWindow(url) {
        var width = window.innerWidth * 0.66 ;
        // define the height in
        var height = width * window.innerHeight / window.innerWidth ;
        var newWindow = window.open(url,'Search popup','height='+height+',width='+width+',left=90,top=100,resizable=no,location=no,status=no,menubar=no,scrollbars=yes');

        if (window.focus) {
            newWindow.focus();
        }

        return false;
    }

    $(function() {
        setTimeout(function () {
            initTable();
        }, 1000);
    });
    function indexFormatter(value, row, index, field) {
        return ($('#table').bootstrapTable('getOptions').pageNumber - 1) * $('#table').bootstrapTable('getOptions').pageSize + index + 1;
    }

    function operateFormatter(value, row, index) {
        return [
            '<a class="btn-feature-info edit" href="#" ><i class="fa fa-edit color-0281B8" title="Chỉnh sửa"></i></a>',
            '<i class=" role fa fa-book color-0281B8 btn-feature-info" style="line-height: 1.2 !important;cursor: pointer;" title="Quyền hạn"></i>',
            '<a class="btn-feature-danger remove" ><i class="fa fa-trash color-FF0000" title="<spring:message code="vn.sdt.remove"/>"></i></a>'
        ].join('')
    }

    function detailFormatterMa(index, row) {
        return '<a class="detail" href="#">'+row.ma+'</a>'
    }

    function detailFormatterTen(index, row) {
        return '<a class="detail" href="#">'+row.hoVaTen+'</a>'
    }

    function detailFormatterEmail(index, row) {
        if (typeof  row.email != 'undefined' && row.email != null && row.email.length > 0) {
            return '<a class="detail" href="#">'+row.email+'</a>'
        } else {
            return '';
        }
    }

    function detailFormatterCoQuan(index, row) {
        return '<a class="detail" href="#">'+row.coQuanQuanLy.ten+'</a>'
    }

    function detailFormatterChucVu(index, row) {
        return '<a class="detail" href="#">'+row.chucVu.ten+'</a>'
    }

    window.operateEvents = {
        'click .edit': function (e, value, row, index) {
            location.href='${themMoiURL}&congChucId='+value;
        },
        'click .remove': function (e, value, row, index) {
            if (confirm('Bạn có thật sự muốn xóa?')) {
                location.href='${xoaURL}&congChucId='+value;
            }
        },
        'click .detail': function (e, value, row, index) {
            location.href='${chiTietURL}&congChucId='+row.id;
        },

        'click .role': function (e, value, row, index) {
            openWindow('${quyenHanURL}&congChucId='+row.id);
        }
    }

    function initTable() {
        $('#toolbar').find('select').change(function () {
        $('#table').bootstrapTable('destroy').bootstrapTable({
            exportDataType: $(this).val(),
            exportOptions:{
                ignoreColumn: [0,7],

            },
            columns: [
                [{
                    field: 'state',
                    checkbox: true,
                    align: 'center'
                }, {
                    title: '<spring:message code="vn.sdt.label.stt"/>',
                    field: 'stt',
                    align: 'center',
                    valign: 'middle',
                    formatter: indexFormatter
                }, {
                    title: '<spring:message code="vn.sdt.label.ma"/>',
                    field: 'ma',
                    align: 'center',
                    valign: 'middle',
                    events: window.operateEvents,
                    formatter: detailFormatterMa,
                }, {
                    title: '<spring:message code="vn.sdt.label.hovaten"/>',
                    field: 'hoVaTen',
                    align: 'left',
                    valign: 'middle',
                    events: window.operateEvents,
                    formatter: detailFormatterTen,
                }, {
                    title: '<spring:message code="vn.sdt.label.email"/>',
                    field: 'email',
                    align: 'left',
                    valign: 'middle',
                    events: window.operateEvents,
                    formatter: detailFormatterEmail,
                }, {
                    title: '<spring:message code="vn.sdt.label.chucvu"/>',
                    field: 'chucVu.ten',
                    align: 'center',
                    valign: 'middle',
                    events: window.operateEvents,
                    formatter: detailFormatterChucVu,
                }, {
                    field: 'id',
                    title: '<spring:message code="vn.sdt.label.thaotac"/>',
                    align: 'center',
                    clickToSelect: false,
                    events: window.operateEvents,
                    formatter: operateFormatter
                }]
            ]
        })
        }).trigger('change');
    }
</script>