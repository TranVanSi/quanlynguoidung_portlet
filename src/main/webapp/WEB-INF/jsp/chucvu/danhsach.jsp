<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="../init.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<portlet:renderURL var="themMoiURL">
    <portlet:param name="action" value="themMoi" />
</portlet:renderURL>
<portlet:actionURL var="xoaURL">
    <portlet:param name="action" value="xoa" />
</portlet:actionURL>
<portlet:renderURL var="chiTietURL">
    <portlet:param name="action" value="chiTiet"></portlet:param>
</portlet:renderURL>
<portlet:actionURL var="importFileURL">
    <portlet:param name="action" value="importFile" />
</portlet:actionURL>

<liferay-portlet:resourceURL var="loadCongChucByChucVuId" id="loadCongChucByChucVuId">
</liferay-portlet:resourceURL>
<liferay-ui:success key="form-success"
                    message="Yêu cầu của bạn đã được thực hiện thành công !." />
<liferay-ui:error key="form-error"
                  message="${thongBao}" />
<div class="admin-dashone-data-table-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline8-list shadow-reset">
                    <div class="sparkline8-hd">
                        <div class="main-sparkline8-hd text-center">
                            <h1 class="tieude"><spring:message code="vn.sdt.chucvu.tieude"/></h1>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <button style="float: right; margin-bottom: 1%" class="btn btn-primary" type="button" title="Thêm mới" onclick="location.href='${themMoiURL}'">Thêm mới</button>
                            <table id="table" class="table-fit" data-toggle="table"
                                   data-pagination="true"
                                   data-search="true"
                                   data-show-columns="false"
                                   data-show-pagination-switch="false"
                                   data-show-refresh="false"
                                   data-key-events="false"
                                   data-show-toggle="true"
                                   data-cookie="false"
                                   data-cookie-id-table="saveId"
                                   data-show-export="false"
                                   data-click-to-select="false"
                                   data-mobile-responsive="true"
                                   data-toolbar="#toolbar">
                                <thead>
                                <tr>
                                    <th data-field="stt" class="text-center"><spring:message code="vn.sdt.label.stt"/></th>
                                    <th data-field="ma" class="text-center"><spring:message code="vn.sdt.label.ma"/></th>
                                    <th data-field="ten" class="text-center"><spring:message code="vn.sdt.label.ten"/></th>
                                    <th data-field="action" class="text-center"><spring:message code="vn.sdt.label.thaotac"/></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="item" items="${listView }" varStatus="num">
                                    <tr>
                                        <td class="align-middle text-center">${num.index +1}</td>
                                        <td class="align-middle text-center">${item.ma}</td>
                                        <td class="align-middle text-left">${item.ten}</td>
                                        <td class="align-middle text-center">
                                            <a style="cursor: pointer" class="btn-feature-info"  onclick="location.href='${themMoiURL}&chucVuId=${item.id}'"><i class="fa fa-edit color-0281B8" title="<spring:message code="vn.sdt.edit"/>"></i></a>
                                            <a style="cursor: pointer" class="btn-feature-danger" onclick="xoa('${item.id}');"><i class="fa fa-trash color-FF0000" title="<spring:message code="vn.sdt.remove"/>"></i></a>

                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#table').bootstrapTable('destroy').bootstrapTable({
            exportDataType: $(this).val(),
            exportOptions:{
                ignoreColumn: [0,4]
            }
        });
       /* $('#fileImport').change(function() {
            if ($(this).val().length > 0) {
                $('#label-upload').text($(this).val());
            }
        });
        setTimeout(function () {
            $('#toolbar').find('select').change(function () {
                $('#table').bootstrapTable('destroy').bootstrapTable({
                    exportDataType: $(this).val(),
                    exportOptions:{
                        ignoreColumn: [0,4]
                    }
                });
            }).trigger('change');
        }, 1000);*/
    })

    function xoa(chucVuId) {
        if (confirm('Bạn có thật sự muốn xóa??')) {
            $.ajax({
                type: 'GET'
                ,url: '${loadCongChucByChucVuId}'+'&chucVuId='+chucVuId
                ,success: function(data) {
                    if (data == true || data == 'true') {
                        alert('Không được xóa vì chức vụ đang được sử dụng!');

                    } else {
                        window.location.href="${xoaURL}&chucVuId="+chucVuId;
                    }

                },
            });
        }
    };
</script>