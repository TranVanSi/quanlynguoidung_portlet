<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="../init.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/custom-style.css">
    <style>
        .red{
            color: red;
        }
        .center{
            text-align: center;
        }
        .mgtb{
            margin-top: 5px;
            margin-bottom: 5px;
        }
        .mg45{
            margin-left: 45%;
        }
    </style>
</head>
<portlet:actionURL var="themMoiURL">
    <portlet:param name="action" value="themMoi" />
</portlet:actionURL>

<portlet:renderURL var="homeUrl">
    <portlet:param name="action" value="" />
</portlet:renderURL>

<c:if test="${empty item.id or item.id == 0}">
    <c:set var="tieuDe">
        <spring:message code="vn.sdt.chucvu.tieude.themmoi"/>
    </c:set>
</c:if>
<c:if test="${item.id > 0}">
    <c:set var="tieuDe">
        <spring:message code="vn.sdt.chucvu.tieude.capnhat"/>
    </c:set>

</c:if>

<liferay-ui:error key="alert-error"
                  message="Yêu cầu của bạn thực hiện không thành công!." />

<div class="basic-form-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="sparkline12-list shadow-reset">
                    <div class="sparkline12-hd">
                        <div class="main-sparkline12-hd text-center">
                            <h1 class="tieude">${tieuDe}</h1>
                        </div>
                    </div>
                    <div class="sparkline12-graph">
                        <div class="basic-login-form-ad">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="all-form-element-inner">
                                        <form:form action="${themMoiURL}" method="post" name="submitForm" class="needs-validation " novalidate="true"
                                                   modelAttribute="item">
                                            <div class="form-group-inner" style="display: none;" >
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <select id="egov-form-errors">
                                                            <option value="ma">
                                                                <liferay-ui:error key="chucvu.validate.ma.trung">
                                                                    <spring:message code="vn.sdt.validate.trung.ma" />
                                                                </liferay-ui:error>
                                                            </option>
                                                        </select>

                                                        <form:hidden path="id" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="ma"><spring:message code="vn.sdt.label.ma" /><span class="red">*</span>:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:input path="ma" id="ma" class="form-control" required="true" maxlength="10"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group-inner">
                                                <div class="row mgtb">
                                                    <div class="col-lg-3 center">
                                                        <form:label path="ten"><spring:message code="vn.sdt.label.ten" /><span class="red">*</span>:</form:label>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <form:input path="ten"  class="form-control" required="true"/>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group-inner">
                                                <div class="login-btn-inner">
                                                    <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                                                <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><spring:message code="vn.sdt.button.luu" /></button>
                                                                <button class="btn btn-sm btn-primary login-submit-cs" type="button" onclick="location.href='${homeUrl}'"><spring:message code="vn.sdt.button.quaylai" /></button>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </form:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- end content -->

                </div>
            </div>
        </div>
    </div>
</div>


