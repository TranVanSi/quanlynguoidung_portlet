<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="${pageContext.request.contextPath}/js/doimatkhau.js"></script>
</head>

<portlet:actionURL var="doiMatKhauURL">
    <portlet:param name="action" value="doiMatKhau" />
</portlet:actionURL>
<liferay-portlet:resourceURL var="checkOldPasswordURL" id="checkOldPassword" ></liferay-portlet:resourceURL>
<liferay-ui:success key="form-success"
                    message="Yêu cầu của bạn đã được thực hiện thành công !." />

<div class="page-title text-center text-uppercase">
    <h3 class="tieude text-center">Thay đổi mật khẩu</h3>
</div>
<input type="hidden" name="checkOldPasswordHidden" value="${checkOldPasswordURL}"/>
<form action="${doiMatKhauURL}" id="formSubmit" method="post" class="form-group row needs-validation"  novalidate="true">
    <div class="col-md-2 col-sm-12 col-xs-12"></div>
    <div class="col-md-8 col-sm-12 col-xs-12 row">
        <div class="col-md-3 col-sm-12 col-xs-12 form-group">Mật khẩu cũ <span class="red">*</span>:</div>
        <div class="col-md-9 col-sm-12 col-xs-12 form-group">
            <input type="password" name="matKhauCu" class="form-control" required="required"/>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12 form-group">Mật khẩu mới <span class="red">*</span>:</div>
        <div class="col-md-9 col-sm-12 col-xs-12 form-group">
            <input type="password" name="matKhauMoi" class="form-control" required="required" minlength="4"/>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12 form-group">Nhập lại mật khẩu <span class="red">*</span>:</div>
        <div class="col-md-9 col-sm-12 col-xs-12 form-group">
            <input type="password" name="nhapLaiMatKhau" class="form-control" required="required"/>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 text-center ">
            <button type="button" id="submitButtonDoiMatKhau" class="btn btn-primary ">Đổi mật khẩu</button>
            <button type="submit" id="submit" class="btn btn-primary hidden">Đổi mật khẩu</button>
        </div>
    </div>
    <div class="col-md-2 col-sm-12 col-xs-12"></div>
</form>

