<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .red{
            color: red;
        }
        .center{
            text-align: center;
        }
        .mgtb{
            margin-top: 5px;
            margin-bottom: 5px;
        }
    </style>
</head>
<script src="${pageContext.request.contextPath}/js/themmoivaitro.js"></script>
<portlet:actionURL var="themMoiURL">
    <portlet:param name="action" value="themMoi" />
</portlet:actionURL>
<portlet:renderURL var="homeUrl">
    <portlet:param name="action" value="" />
</portlet:renderURL>
<liferay-portlet:resourceURL var="validateFormURL" id="validateForm" ></liferay-portlet:resourceURL>
<portlet:defineObjects />
<c:if test="${empty vaiTro.id or vaiTro.id == 0}">
    <c:set var="tieuDe">
        <spring:message code="vn.sdt.tieude.themmoi.vaitro"/>
    </c:set>
</c:if>
<c:if test="${vaiTro.id > 0}">
    <c:set var="tieuDe">
        <spring:message code="vn.sdt.tieude.capnhat.vaitro"/>
    </c:set>

</c:if>
<liferay-ui:error key="alert-error"
                  message="Yêu cầu của bạn thực hiện không thành công!." />

<div class="basic-form-area mg-b-15">
    <div class="container-fluid">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sparkline12-list shadow-reset">
                        <div class="sparkline12-hd">
                            <div class="main-sparkline12-hd text-center">
                                <h1 class="tieude">${tieuDe}</h1>
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="all-form-element-inner">
                                            <form:form action="${themMoiURL}" method="post" name="submitForm" id="formThemMoi" class="needs-validation " novalidate="true"
                                                modelAttribute="vaiTro">
                                                <div class="form-group-inner" style="display: none;" >
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <select id="egov-form-errors">
                                                                <option value="ma">
                                                                    <liferay-ui:error key="vaitro.validate.ma.trung">
                                                                        <spring:message code="vn.sdt.validate.trung.ma" />
                                                                    </liferay-ui:error>
                                                                </option>
                                                                <option value="ten">
                                                                    <liferay-ui:error key="vaitro.validate.ten.trung">
                                                                        <spring:message code="vn.sdt.validate.trung.ten" />
                                                                    </liferay-ui:error>
                                                                </option>
                                                            </select>

                                                            <form:hidden path="id" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group-inner">
                                                    <div class="row mgtb">
                                                        <div class="col-lg-3 center">
                                                            <form:label path="ma"><spring:message code="vn.sdt.tainguyen.ma" /><span class="red">*</span>:</form:label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <form:input path="ma" id="ma" class="form-control" required="true" maxlength="10"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group-inner">
                                                    <div class="row mgtb">
                                                        <div class="col-lg-3 center">
                                                            <form:label path="ten"><spring:message code="vn.sdt.tainguyen.ten" /><span class="red">*</span>:</form:label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <form:input path="ten" id="ten" class="form-control" required="true"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group-inner">
                                                    <div class="row mgtb">
                                                        <div class="col-lg-3 center">
                                                            <form:label path="trangThai"><spring:message code="vn.sdt.tainguyen.trangthai" />:</form:label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <form:select path="trangThai"  class="form-control">
                                                                <form:option value="1"><spring:message code="vn.sdt.tainguyen.trangthai.hoatdong" /></form:option>
                                                                <form:option value="0"><spring:message code="vn.sdt.tainguyen.trangthai.khoa" /></form:option>
                                                            </form:select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group-inner">
                                                    <div class="row mgtb">
                                                        <div class="col-lg-3 center">
                                                            <form:label path="moTa"><spring:message code="vn.sdt.tainguyen.mota" />:</form:label>
                                                        </div>
                                                        <div class="col-lg-9">
                                                            <form:textarea path="moTa"  class="form-control" type="text"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row form-group">

                                                    <div class="col-md-5 col-sm-5 col-sx-5" style="text-align: left">
                                                        <label>Tài nguyên hệ thống</label><br>
                                                        <select name="taiNguyenHeThong" id="taiNguyenHeThong"  multiple="multiple" size="10"  class="form-control">
                                                            <c:forEach items="${danhSachTaiNguyenHeThong }" var="taiNguyenHeThong">
                                                                <option value="${taiNguyenHeThong.id }">${taiNguyenHeThong.ten }</option>
                                                            </c:forEach>
                                                        </select>
                                                        <input type="text" value="" onkeyup="searchOptions(this.value);" class="form-control"/>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 col-sx-2" style="text-align: center;margin-top: 5% !important">
                                                        <button type="button" class="btn btn-primary" id="right_to_left" style="margin-bottom: .3em !important;">>></button><br>
                                                        <button type="button" class="btn btn-primary" id="left_to_right"><<</button>
                                                    </div>
                                                    <div class="col-md-5 col-sm-5 col-sx-5" style="text-align: left">
                                                        <label>Tài nguyên được chọn<span class="red">*</span></label><br>
                                                        <select name="taiNguyenHeThongDuocChon" id="taiNguyenHeThongDuocChon" multiple="multiple"  size="10" class="form-control" required="true">
                                                            <c:forEach items="${danhSachTaiNguyenHeThongDuocChon }" var="taiNguyenHeThongDuocChon" >
                                                                <option value="${taiNguyenHeThongDuocChon.id }">${taiNguyenHeThongDuocChon.ten }</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                                        <button class="btn btn-info" validate="${validateFormURL}" type="button" id="submitButton"><spring:message code="vn.sdt.tainguyen.luu" /></button>
                                                        <button class="btn btn-info" type="button" onclick="location.href='${homeUrl}'"><spring:message code="vn.sdt.tainguyen.quaylai" /></button>
                                                    </div>

                                                </div>

                                            </form:form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/js/themmoivaitro.js"></script>