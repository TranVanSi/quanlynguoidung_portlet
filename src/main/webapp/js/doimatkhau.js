$(document).ready(function(){
    $('#submitButtonDoiMatKhau').click(function(){
        var url = $('[name="checkOldPasswordHidden"]').val();
        var matKhauCu = $('[name="matKhauCu"]').val();
        var matKhauMoi = $('[name="matKhauMoi"]').val();
        var nhapLaiMatKhau = $('[name="nhapLaiMatKhau"]').val();

        if (matKhauCu.trim().length == 0 || matKhauMoi.trim().length == 0 || nhapLaiMatKhau.trim().length == 0) {
            $('#submit').click();
        }

        if (matKhauMoi != nhapLaiMatKhau) {
            $('[name="nhapLaiMatKhau"]').addClass('select2-invalid').attr('title', 'Mật khẩu không trùng khớp!');
            return;
        } else {
            $('[name="nhapLaiMatKhau"]').removeClass('select2-invalid').attr('title', '');
        }

        $.ajax({
            url: url
            ,async: false
            ,data : {
                email : themeDisplay.getUserEmailAddress,
                passwordOld : matKhauCu
            }
            ,success: function(data) {
                if (data || data == 'true') {
                    $('#submit').click();
                }  else {
                    $('[name="matKhauCu"]').addClass('select2-invalid').attr('title', 'Mật khẩu cũ không đúng!');
                }
            }, error: function (err) {
            }
        });
    });
});