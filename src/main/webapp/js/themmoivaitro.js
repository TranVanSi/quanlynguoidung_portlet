$('#submitButton').click(function(){
    $('#formThemMoi').addClass("was-validated");
    $('#taiNguyenHeThongDuocChon option').prop('selected', true);
    var url = $(this).attr('validate');
    
    if (validateForm(url)) {
        $('#formThemMoi').submit();
    } else {
        return ;
    }
});

function validateForm(validateURL) {
    var ma = $("[name='ma']").val();
    var ten = $("[name='ten']").val();
    var vaiTroId = $("[name='id']").val();
    var taiNguyenDuocChon = $('#taiNguyenHeThongDuocChon').val();
    var isValid = false;
    
    $.ajax({
        url: validateURL
        ,data : {
            ma: ma,
            ten: ten,
            vaiTroId: vaiTroId
        }
        ,async: false,
        dataType: "json"
        ,success: function(json) {

            if (json["ma"] == true) {
               $("[name='ma']").removeAttr('title');
                $("[name='ma']").removeClass("form-invalid");
                
            } else {
                $("[name='ma']").attr('title', '+ Mã này đã tồn tại!');
                $("[name='ma']").addClass("form-invalid");
            }

            if (json["ten"] == true) {
                $("[name='ten']").removeAttr('title');
                $("[name='ten']").removeClass("form-invalid");
            } else {
                $("[name='ten']").attr('title', '+ Tên này đã tồn tại!');
                $("[name='ten']").addClass("form-invalid");
                
            }

            if (ma.length == 0) {
                $("[name='ma']").attr('title', '+ Nhập thông tin trường này!');
            }

            if (ten.length == 0) {
                $("[name='ten']").attr('title', '+ Nhập thông tin trường này!');
            }

            if (taiNguyenDuocChon == null || (taiNguyenDuocChon != null && taiNguyenDuocChon.length == 0)) {
                $("[name='taiNguyenHeThongDuocChon']").attr('title', '+ Nhập thông tin trường này!');
            }
            
            if (json["ma"] == true && json["ten"] == true && taiNguyenDuocChon.length > 0) {
                isValid = true;
            }
        }
    });

    return isValid;
}

$('#right_to_left').click(function(){
    var left = $("#taiNguyenHeThong")[0];
    var right = $("#taiNguyenHeThongDuocChon")[0];
    selectCategory(left, right);
});

$('#left_to_right').click(function(){
    var left = $("#taiNguyenHeThongDuocChon")[0];
    var right = $("#taiNguyenHeThong")[0];
    selectCategory(left, right);
});
function selectCategory(left, right)
{
    var options = left.children;


    var tmp = [];

    // Lấy tất cả những options selected từ left
    for (var i = 0; i < options.length; i++){
        if (options[i].selected){
            tmp.push(options[i]);
        }
    }
    // Đưa option selected qua right
    for (var i = 0; i < tmp.length; i++){
        right.appendChild(tmp[i]);
    }

}

function searchOptions($keyWord){

    if($keyWord.length>0) {
        $('#taiNguyenHeThong option').each(function(){
            $keyWord = $keyWord.toLowerCase();
            if(($(this).text().toLowerCase().indexOf($keyWord)) != -1) {
                $(this).css('display','');
            }else{
                $(this).css('display','none');
            }
        });
    }else{
        $('#taiNguyenHeThong option').each(function(){
            $(this).css('display','');
        });
    }
    $('#taiNguyenHeThong option').each(function(){
        var taiNguyenId = $(this).val();
        if($("#taiNguyenHeThongDuocChon option[value='"+taiNguyenId+"']").length > 0){
            $(this).css('display','none');
        }
    });
}