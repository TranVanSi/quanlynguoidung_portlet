$(document).ready(function () {
    loadDataTable();
})

function loadDataTable() {
    var loadCongChucURL = $('[name="loadCongChucDataTableURL"]').val();
    var themMoiURL = $('[name="themMoiURL"]').val();
    
    $('table#congChuc').DataTable({
        ajax: loadCongChucURL,
        serverSide: true,
        columns: [
            {
                data: 'ma'
            },
            {
                data: 'hoVaTen'
            },
            {
                data: 'taiKhoanNguoiDung.email'
            },
            {
                data: 'chucVu.ten'
            },
            {
                data: 'id',
                render: function (data) { return  '<a class="btn btn-raised btn-info btn-xs" href="#" onclick="location.href=\''+themMoiURL+'&congChucId='+data+'\'"><i class="fa fa-edit" aria-hidden="true" title="Chỉnh sửa"></i></a>'; }
            }
        ]
    });
}