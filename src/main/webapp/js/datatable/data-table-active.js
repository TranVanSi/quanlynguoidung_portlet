(function ($) {
 "use strict";

		var $table = $('#table');
				$('#toolbar').find('li.datatable-export-li').click(function () {
					console.log('$(\'li.datatable-export-li.selected\').attr(\'data-value\'): '+$('li.datatable-export-li.selected').attr('data-value'));
					$table.bootstrapTable('destroy').bootstrapTable({
						exportDataType: $('li.datatable-export-li.selected').attr('data-value')
					});
				});
 
})(jQuery); 