$('#submitButton').click(function(){
    
    $('#vaiTroHeThongDuocChon option').prop('selected', true);
    $('#button').click();
});

$('#right_to_left').click(function(){
    var left = $("#vaiTroHeThong")[0];
    var right = $("#vaiTroHeThongDuocChon")[0];
    selectCategory(left, right);
    addCoQuanAjax();
});

$('#left_to_right').click(function(){
    var left = $("#vaiTroHeThongDuocChon")[0];
    var right = $("#vaiTroHeThong")[0];
    selectCategory(left, right);
    addCoQuanAjax();
});
function selectCategory(left, right)
{
    var options = left.children;


    var tmp = [];

    // Lấy tất cả những options selected từ left
    for (var i = 0; i < options.length; i++){
        if (options[i].selected){
            tmp.push(options[i]);
        }
    }
    // Đưa option selected qua right
    for (var i = 0; i < tmp.length; i++){
        right.appendChild(tmp[i]);
    }

}

function searchOptions($keyWord){

    if($keyWord.length>0) {
        $('#vaiTroHeThong option').each(function(){
            $keyWord = $keyWord.toLowerCase();
            if(($(this).text().toLowerCase().indexOf($keyWord)) != -1) {
                $(this).css('display','');
            }else{
                $(this).css('display','none');
            }
        });
    }else{
        $('#vaiTroHeThong option').each(function(){
            $(this).css('display','');
        });
    }
    $('#vaiTroHeThong option').each(function(){
        var vaiTroId = $(this).val();
        if($("#vaiTroHeThongDuocChon option[value='"+vaiTroId+"']").length > 0){
            $(this).css('display','none');
        }
    });
}

function dongCuaSoPoup() {
    window.parent.opener.reloadTable();
    window.close();
}

function addCoQuanAjax () {
    $('#vaiTroHeThongDuocChon option').prop('selected', true);
    var vaiTroHeThongDuocChon = $('[name="vaiTroHeThongDuocChon"]').val();
    var coQuanId = $('[name="coQuanId"]').val();
    var url = $('#addCoQuan').val();
    var chucVuId = $('[name="chucVuId"]').val();

    if (chucVuId > 0 && vaiTroHeThongDuocChon.length > 0) {
        $.ajax({
            url: url
            ,data : {
                vaiTroHeThongDuocChon : vaiTroHeThongDuocChon+"",
                coQuanId : coQuanId,
                chucVuId : chucVuId
            }
            ,success: function(data) {
                $('#vaiTroHeThong option').prop('selected', false);
            }, error: function (err) {
            }
        });
    } else {
        alert('Phải có ít nhất 1 chức vụ và 1 vai trò được chọn');
        var left = $("#vaiTroHeThongDuocChon")[0];
        var right = $("#vaiTroHeThong")[0];
        selectCategory(right, left);
    }
}

$(document).ready(function()
{
    $(window).bind("beforeunload", function() {
        dongCuaSoPoup();
    });
});