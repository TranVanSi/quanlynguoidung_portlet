$(document).ready(function () {
    $('#frmTimKiem').submit();
})
function pagination (total, pageIndex, size, dataURL, themMoiURL, xoaURL) {
    var keyword = $('[name="keyword"]').val();;
    $('#dgov-pagination').pagination({
        total: total,
        current: pageIndex + 1,
        length: size,
        size: size,
        prev: 'Trước',
        next: 'Sau',
        ajax: function(options, refresh, $target){
            console.log();
            $.ajax({
                url: dataURL,
                data:{
                    pageIndex: options.current - 1,
                    size: options.length,
                    keyword: keyword
                },
                cache: false,
                contentType: "application/json; charset=utf-8"

            }).done(function(res){

                reDrawTable(res, options.current, options.length, themMoiURL, xoaURL);
                refresh({
                    total: res.totalElements == 0 ? -1 : res.totalElements,
                    length: res.length
                });
            }).fail(function(error){
                console.log('LOI PHAN TRANG: '+error);
            });
        }
    });
}


function reDrawTable (result, pageIndex, size, themMoiURL, xoaURL) {
    $('tbody').empty();
    var xhtml = "";

    var stt = ((pageIndex - 1) * size);
    for (var i=0; i < result.content.length; i++) {

        xhtml = "<tr>";
        var obj = result.content[i];

        xhtml += '<td data-title="STT">'+(stt +=1)+'</td>';
        xhtml += '<td data-title="Mã">'+obj["ma"]+'</td>';
        xhtml += '<td data-title="Tên">'+obj["ten"]+'</td>';

        var loai = "";
        var trangThai = "";
        if (obj["loai"] == 1) {
            loai =  'Vai trò mặc định';
        } else if (obj["loai"] == 2) {
            loai = 'Vai trò của site';
        } else {
            loai = 'Vai trò tổ chức';
        }

        if (obj["trangThai"] == 1) {
            trangThai = ' Hoạt động';
        } else {
            trangThai = ' Khóa';
        }
        xhtml += '<td data-title="Loại">'+loai+'</td>';
        xhtml += '<td data-title="Trạng thái">'+trangThai+'</td>';

        xhtml += '<td data-title="Chức năng">' +
            '<a class="btn btn-raised btn-info btn-xs" href="#" onclick="location.href=\''+themMoiURL+'&taiNguyenId='+obj["id"]+'\'"><i class="fa fa-edit" aria-hidden="true" title="Chỉnh sửa"></i></a>' +
            '<a class="btn btn-raised btn-danger btn-xs" href="'+xoaURL+'&taiNguyenId='+obj["id"]+'" onclick="return confirm(\'Bạn có thật sự muốn xóa?\')"><i class="fa fa-trash-o" aria-hidden="true" title="Xóa"></i></a>' +
            '</td>';
        xhtml+="</tr>";

        $('tbody').append(xhtml);
    }
}