$(document).ready(function () {
    loadDataTable();
})

function loadDataTable() {
    var loadChucVuURL = $('[name="loadCoQuanQuanLyDataTableURL"]').val();
    var ganChucVuURL = $('[name="ganChucVuURL"]').val();
    
    $('table#chucVu').DataTable({
        ajax: loadChucVuURL,
        serverSide: true,
        searching: true,
        columns: [
            {
                data: 'chucVu.ma'
            },
            {
                data: 'chucVu.ten'
            },
            {
                data: 'vaiTros',
                render: function (data, type, row, meta) {
                    var vaiTro = "";
                    for (var k = 0 ; k < data.length ; k++) {
                        vaiTro += data[k].ten + "<hr/>";
                    }
                    
                    return vaiTro;
                }
            },
            {
                data : 'id',
                render: function (data, type, row, meta) {
                    return  '<a class="btn btn-raised btn-info btn-xs" href="javascript:openWindow(\''+ganChucVuURL+'&chucVuId='+data+'\')">' +
                        '<i class="fa fa-edit" aria-hidden="true" title="Chỉnh sửa"></i>' +
                        '</a>';
                }
            }
        ]
    });
}

function reloadTable () {
    $('table#chucVu').DataTable().destroy();
    loadDataTable();
}