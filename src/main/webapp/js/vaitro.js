$(document).ready(function () {
    $('#frmTimKiem').submit();
})
function pagination ( pageIndex, size, dataURL, themMoiURL, xoaURL) {
    var keyword = $('[name="keyword"]').val();;
    $('#dgov-pagination').pagination({
        total: 0,
        current: pageIndex + 1,
        length: size,
        size: size,
        prev: 'Trước',
        next: 'Sau',
        ajax: function(options, refresh, $target){
            
            $.ajax({
                url: dataURL,
                data:{
                    pageIndex: options.current - 1,
                    size: options.length,
                    keyword: keyword
                },
                cache: false,
                contentType: "application/json; charset=utf-8"

            }).done(function(res){

                reDrawTable(res, options.current, options.length, themMoiURL, xoaURL);
                refresh({
                    total: res.totalElements == 0 ? -1 : res.totalElements,
                    length: res.length
                });
            }).fail(function(error){

            });
        }
    });
}


function reDrawTable (result, pageIndex, size, themMoiURL, xoaURL) {
    $('tbody').empty();
    var xhtml = "";

    var stt = ((pageIndex - 1) * size);
    for (var i=0; i < result.content.length; i++) {

        xhtml = "<tr>";
        var obj = result.content[i];

        xhtml += '<td data-title="STT">'+(stt +=1)+'</td>';
        xhtml += '<td data-title="Mã" class="text-left">'+obj["ma"]+'</td>';
        xhtml += '<td data-title="Tên" class="text-left">'+obj["ten"]+'</td>';

        var trangThai = "";

        if (obj["trangThai"] == 1) {
            trangThai = ' Hoạt động';
        } else {
            trangThai = ' Khóa';
        }
        xhtml += '<td data-title="Mô tả" class="text-left">'+(obj["moTa"] == null ? "" : obj["moTa"])+'</td>';

        var tenTaiNguyen = "";

        if (obj["taiNguyens"].length > 0) {
            for (var k = 0; k < obj["taiNguyens"].length ; k++) {
                tenTaiNguyen += (obj["taiNguyens"][k]["ten"] + "<hr>")
            }
        }
        xhtml += '<td data-title="Tên tài nguyên" class="text-left">'+tenTaiNguyen+'</td>';
        xhtml += '<td data-title="Trạng thái" class="text-center">'+trangThai+'</td>';

        xhtml += '<td data-title="Chức năng">' +
            '<a class="btn btn-raised btn-info btn-xs" href="#" onclick="location.href=\''+themMoiURL+'&id='+obj["id"]+'\'"><i class="fa fa-edit" aria-hidden="true" title="Chỉnh sửa"></i></a>' +
            '</td>';
        xhtml+="</tr>";

        $('tbody').append(xhtml);
    }
}